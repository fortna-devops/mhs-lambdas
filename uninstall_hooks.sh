#!/usr/bin/env bash

set -e

GIT_HOOKS_DIR=./.git/hooks/
HOOKS_DIR=../../hooks/*

cd ${GIT_HOOKS_DIR}

for hook in ${HOOKS_DIR}
do
    tmp=${hook##*/}
    unlink ${tmp%.*}
    echo Sucessfully uninstall ${tmp%.*} hook.
done
