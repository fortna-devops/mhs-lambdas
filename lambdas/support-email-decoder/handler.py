#!/usr/bin/env python3

"""
Retrieve raw emails in MIME format sent to
support.mhsinsights.com and sends decoded
email to internal MHS support users.

..author: Hasan Khan <hasan.khan@mhsglobal.com>
"""

import base64
import email
import logging
import os
from typing import Any, List

import boto3
import s3fs

from analytics_sdk_core.config.config import ConfigManager, SUPPORT_GROUP_KEY
from analytics_sdk_core.email_tools.email_tools import EmailSender


logger = logging.getLogger()
logger.setLevel(logging.INFO)


def get_email_file(boto_session: boto3.session, file_name: str) -> email.message.Message:
    """
    Retrieve email file from S3.
    """
    fs = s3fs.S3FileSystem(session=boto_session)
    file_path = f'mhs-insights-support-emails/emails/{file_name}'

    try:
        with fs.open(file_path, 'rb') as f:
            encoded_email = email.message_from_binary_file(f)
    except FileNotFoundError:
        logger.error(f'Cannot open raw email file: {file_path}')

    return encoded_email


def split_email_sections(email_file: email.message.Message) -> List[Any]:
    """
    Create list of sections comprising email Message object.
    """
    payload_list = []

    if email_file.is_multipart():
        for payload in email_file.get_payload():  # type: ignore
            payload_list.append(payload.get_payload())  # type: ignore
    else:
        payload_list.append(email_file.get_payload())

    return payload_list


def construct_email(message_obj: email.message.Message, email_sections: List[Any]) -> tuple:
    """
    Get email subject and body from encoded raw email.
    """
    # index [0][0]: base64 encoded plain text
    # index [0][1]: base64 encoded html
    # index [1]: base64 encoded string with body of email
    decoded_body = base64.b64decode(email_sections[0][0].get_payload()).decode()
    # Remove email signature if exists. Do nothing otherwise.
    formatted_body = decoded_body.replace('\n', '').replace('\r', '').split('[signature_')
    content = formatted_body[0]

    subject = f'{message_obj["Subject"]} [{message_obj["From"]}]'
    body = f'Date sent: {message_obj["Date"]}<br><br>' \
           f'To: {message_obj["To"]}<br><br>' \
           f'Body: {content}<br><br>'

    return subject, body


def handler(event: dict, _context: dict) -> None:
    """
    Entry point of lambda handler function.
    """
    active_env = os.environ['env']
    file_name = event['Records'][0].get('s3').get('object').get('key')

    boto_session = boto3.Session(profile_name=os.getenv('profile'))
    email_sender = EmailSender(session=boto_session)
    config_manager = ConfigManager(env=active_env)
    support_users = config_manager.get_email_group(SUPPORT_GROUP_KEY)

    email_file = get_email_file(boto_session, file_name)
    raw_email = split_email_sections(email_file)
    subject, body = construct_email(email_file, raw_email)

    email_sender.send_emails(body=body, subject=subject, recipient_list=support_users)


if __name__ == '__main__':
    os.environ['env'] = 'develop'

    handler({}, {})
