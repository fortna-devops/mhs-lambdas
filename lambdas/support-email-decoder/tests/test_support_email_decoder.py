"""
Module for testing the Support Email Decoder lambda.

..author: Hasan Khan <hasan.khan@mhsglobal.com>
"""


import email
import pytest

from handler import split_email_sections, construct_email


@pytest.fixture
def test_email_file():
    with open('tests/po4126uk076c6im8pe3sund4o4g7639e7qfgqco1.txt', 'rb') as f:
        email_file = email.message_from_binary_file(f)
    return email_file


def test_email_subject(test_email_file):
    raw_email = split_email_sections(test_email_file)
    subject, body = construct_email(test_email_file, raw_email)

    assert subject == f'{test_email_file["Subject"]} [{test_email_file["From"]}]'


def test_email_body(test_email_file):
    raw_email = split_email_sections(test_email_file)
    subject, body = construct_email(test_email_file, raw_email)

    assert body == 'Date sent: Wed, 29 Jan 2020 15:25:06 +0000<br><br>To: "support@mhsinsights.com" ' \
                   '<support@mhsinsights.com><br><br>Body: This is a test.<br><br>'
