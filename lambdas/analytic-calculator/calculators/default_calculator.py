import logging

from typing import Tuple, List, Dict, Any, Type

from mhs_rdb.models import ColorsEnum

from .base_default_calculator import BaseDefaultCalculator
from .alarm_processors import BaseAlarmProcessor, SpikeAlarmProcessor, PersistentAlarmProcessor, Result


__all__ = ['DefaultCalculator']


logger = logging.getLogger()


class DefaultCalculator(BaseDefaultCalculator):
    """
    Default calculator.
    """

    _threshold_level_names = {
        'low_red': 'low_red',
        'low_yellow': 'low_orange',
        'high_yellow': 'orange',
        'high_red': 'red'
    }

    _valid_value_types = (float, int)

    _alarm_processors: Tuple[Type[BaseAlarmProcessor], ...] = (
        SpikeAlarmProcessor,
        PersistentAlarmProcessor
    )

    def _validate_value_types(self, values: List[Any]) -> bool:
        """
        Validates value types.
        """

        for value in values:
            if not isinstance(value, self._valid_value_types):
                return False

        return True

    def _process_metric_alarms(self, metric: Dict[str, Any], data: Dict[int, Dict[str, List[Any]]]) -> Result:
        """
        Processes all alarms for one metric.
        """

        result = Result()
        low_red_level = metric['threshold_levels'].get(self._threshold_level_names['low_red'])
        low_yellow_level = metric['threshold_levels'].get(self._threshold_level_names['low_yellow'])
        high_yellow_level = metric['threshold_levels'].get(self._threshold_level_names['high_yellow'])
        high_red_level = metric['threshold_levels'].get(self._threshold_level_names['high_red'])

        if all((low_red_level is None, low_yellow_level is None, high_yellow_level is None, high_red_level is None)):
            result.add_metric('no_threshold_level_names', metric)
            return result

        if low_red_level is None:
            low_red_level = float('-inf'), ColorsEnum.RED

        if low_yellow_level is None:
            low_yellow_level = low_red_level

        if high_red_level is None:
            high_red_level = float('inf'), ColorsEnum.RED

        if high_yellow_level is None:
            high_yellow_level = high_red_level

        metric_data = data.get(metric['id'])
        if not metric_data:
            result.add_metric('no_data', metric)
            return result

        values = metric_data['values']
        timestamps = metric_data['timestamps']
        if not self._validate_value_types(values):
            result.add_metric('invalid_value_types', metric)
            return result

        for alarm_processor in self._alarm_processors:
            result.extend(
                alarm_processor(
                    self._data_points_timedelta,
                    metric,
                    low_red_level,
                    low_yellow_level,
                    high_yellow_level,
                    high_red_level,
                    values,
                    timestamps
                ).process()
            )

        return result

    def _process_alarms(self) -> Result:
        """
        Processes all alarms for all data sources.
        """

        data_sources = self._get_data_sources()
        metric_ids = self._get_metric_ids(data_sources)
        data = self._get_data(metric_ids)
        result = Result()
        for data_source in data_sources:
            for metric in data_source['metrics']:
                result.extend(self._process_metric_alarms(metric, data))

        return result

    def _log_result(self, result: Result):
        """
        Logs result.
        """

        metrics = result.get_metrics('no_threshold_level_names')
        if metrics:
            logger.warning(f"Site: '{self._site}'. No {' and '.join(self._threshold_level_names)} "
                           f"threshold level names were found the next metrics: {metrics}.")

        metrics = result.get_metrics('no_data')
        if metrics:
            logger.warning(f"Site: '{self._site}'. No data were found for the last "
                           f"{self._data_proxy_timedelta.seconds / 60} minutes for the next metrics: {metrics}.")

        metrics = result.get_metrics('invalid_value_types')
        if metrics:
            logger.warning(f"Site: '{self._site}'. Invalid value types were found for the next metrics: {metrics}.")

        alarm_results = []
        for alarm_processor in self._alarm_processors:
            name = alarm_processor.__name__
            num_alarms = len(result.get_alarms(name))
            alarm_results.append(f"{name.replace('Processor', 's')}: {num_alarms}")

        logger.info(f"Site: '{self._site}'. Data source ids: {self._data_source_ids}. "
                    f"{', '.join(alarm_results)}.")

    def calculate(self):
        """
        Calculates alarms.
        """

        result = self._process_alarms()
        self._log_result(result)

        alarms = []
        for alarm_processor in self._alarm_processors:
            alarms.extend(result.get_alarms(alarm_processor.__name__))

        self._insert_alarms(alarms)
