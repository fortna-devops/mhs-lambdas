from abc import abstractmethod
from typing import List, Sequence, Dict, Any
from datetime import timedelta

from mhs_rdb import DataSources, Alarms
from mhs_rdb.models import DataSourcesModel, AlarmsModel
from mhs_rdb.utils.data import DataProxy

from .base_calculator import BaseCalculator


__all__ = ['BaseDefaultCalculator']


class BaseDefaultCalculator(BaseCalculator):
    """
    Base default calculator.
    """

    _threshold_type_names = ('iso',)

    _data_proxy_timedelta = timedelta(minutes=70)

    _data_proxy_sample_rate = DataProxy.minute_sample_rate

    _data_proxy_timestamp_order = DataProxy.asc_ordering

    _data_points_timedelta = timedelta(minutes=1)

    @staticmethod
    def _get_metric_ids(data_sources: List[Dict[str, Any]]) -> List[int]:
        """
        Returns list of metric ids for all data sources.
        """

        return [metric['id'] for data_source in data_sources for metric in data_source['metrics']]

    def _get_data_sources_with_metrics_and_thresholds(self) -> List[DataSourcesModel]:
        """
        Returns data sources with metrics and thresholds.
        """

        with self._config_manager.connector.get_session(self._customer, self._site) as session:
            return DataSources(session).get_with_metrics_and_thresholds(
                ids=self._data_source_ids,
                metric_to_analyze=True,
                threshold_type_names=self._threshold_type_names
            )

    def _get_data_sources(self) -> List[Dict[str, Any]]:
        """
        Returns data sources with metrics and threshold levels.

        Example of return value:
        [
            {
                'id': 1,
                'type_name': 'vt',
                'metrics': [
                    {
                        'id': 1,
                        'name': 'rms_velocity_x',
                        'display_name': 'RMS Vibrational Velocity X',
                        'units': 'in/sec',
                        'min_value': 0.0,
                        'max_value': 4.0,
                        'threshold_levels': {
                            'red': (1.1, '<ColorsEnum.RED: 3>'),
                            'orange': (0.5, '<ColorsEnum.YELLOW: 2>')
                        }
                    },
                    {
                        'id': 2,
                        'name': 'rms_velocity_z',
                        'display_name': 'RMS Vibrational Velocity Z',
                        'units': 'in/sec',
                        'min_value': 0.0,
                        'max_value': 4.0,
                        'threshold_levels': {
                            'red': (1.1, '<ColorsEnum.RED: 3>')
                        }
                    },
                    {
                        'id': 3,
                        'name': 'temperature',
                        'display_name': 'Temperature',
                        'units': None,
                        'min_value': None,
                        'max_value': None,
                        'threshold_levels': {}
                    }
                ]
            }
        ]
        """

        data_sources = []
        for data_source in self._get_data_sources_with_metrics_and_thresholds():

            metrics: Dict[int, Dict[str, Any]] = {}
            for metric in data_source.metrics:

                if metric.id not in metrics:
                    metrics[metric.id] = {
                        'id': metric.id,
                        'name': metric.name,
                        'display_name': metric.display_name,
                        'units': metric.units,
                        'min_value': metric.min_value,
                        'max_value': metric.max_value,
                        'threshold_levels': {}
                    }

                for threshold in metric.thresholds:
                    metrics[metric.id]['threshold_levels'][threshold.level_name] = (
                        threshold.value,
                        threshold.level.color
                    )

            data_sources.append({
                'id': data_source.id,
                'type_name': data_source.type.name,
                'metrics': list(metrics.values())
            })

        return data_sources

    def _get_grouped_metric_data(self, metric_ids: List[int]) -> Sequence[Dict[str, Any]]:
        """
        Returns grouped metric data.

        Returns data in ascending order.
        """

        start_datetime = self._datetime - self._data_proxy_timedelta
        with self._config_manager.connector.get_session(self._customer, self._site) as session:
            return DataProxy(session).get_grouped_by_metric_id(
                start_datetime=start_datetime,
                end_datetime=self._datetime,
                metric_ids=metric_ids,
                sample_rate=self._data_proxy_sample_rate,
                timestamp_order=self._data_proxy_timestamp_order
            )

    def _get_data(self, metric_ids: List[int]) -> Dict[int, Dict[str, List[Any]]]:
        """
        Example of return value:
        {
            1: {
                'values': [0.1, 0.2],
                'timestamps': [datetime.datetime(2020, 1, 1, 0, 0, 0), datetime.datetime(2020, 1, 1, 0, 1, 0)]
            },
            2: {
                'values': [-0.1, -0.2],
                'timestamps': [datetime.datetime(2020, 1, 1, 0, 0, 0), datetime.datetime(2020, 1, 1, 0, 1, 0)]
            }
        }
        """

        data: Dict[int, Dict[str, List[Any]]] = {}
        for metric_data in self._get_grouped_metric_data(metric_ids):

            if metric_data['metric_id'] not in data:
                data[metric_data['metric_id']] = {'values': [], 'timestamps': []}

            data[metric_data['metric_id']]['values'].append(metric_data['value'])
            data[metric_data['metric_id']]['timestamps'].append(metric_data['timestamp'])

        return data

    def _insert_alarms(self, alarms: List[AlarmsModel]):
        """
        Insert alarms.
        """

        with self._config_manager.connector.get_session(self._customer, self._site) as session:
            Alarms(session).insert_all(alarms)

    @abstractmethod
    def calculate(self):
        pass
