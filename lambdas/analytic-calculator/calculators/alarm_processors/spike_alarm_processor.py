from .base_alarm_processor import BaseAlarmProcessor


__all__ = ['SpikeAlarmProcessor']


class SpikeAlarmProcessor(BaseAlarmProcessor):
    """
    Spike alarm processor.
    """

    _type_name = 'iso_spike'

    _max_sequence_length = 7
