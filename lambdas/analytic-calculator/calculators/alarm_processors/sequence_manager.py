from typing import List, Optional, Union
from datetime import datetime

from mhs_rdb.models import ColorsEnum

from .sequence import Sequence


__all__ = ['SequenceManager']


class SequenceManager:
    """
    Sequence manager.
    """

    def __init__(self, min_sequence_length: Optional[int] = None, max_sequence_length: Optional[int] = None):
        self._min_sequence_length = min_sequence_length
        self._max_sequence_length = max_sequence_length
        self._sequence: Optional[Sequence] = None
        self._sequences: List[Sequence] = []

    @property
    def sequences(self) -> List[Sequence]:
        """
        Returns list of sequential exceeded values.
        """

        return self._sequences

    def interrupt_sequence(self):
        """
        Interrupts sequential exceeded values.
        """

        if self._sequence is None:
            return

        if self._min_sequence_length is not None and self._sequence.length < self._min_sequence_length:
            self._sequence = None
            return

        if self._max_sequence_length is not None and self._sequence.length > self._max_sequence_length:
            self._sequence = None
            return

        self._sequences.append(self._sequence)
        self._sequence = None

    def add_value(self, criteria_value: float, color: ColorsEnum, value: Union[float, int], timestamp: datetime):
        """
        Adds value to the sequential exceeded values.
        """

        if self._sequence is None:
            self._sequence = Sequence(criteria_value, color)

        if self._sequence.color == color:
            self._sequence.add_value(value, timestamp)
            return

        self.interrupt_sequence()
        self._sequence = Sequence(criteria_value, color)
        self._sequence.add_value(value, timestamp)
