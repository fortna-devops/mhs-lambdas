from abc import ABC, abstractmethod
from typing import Tuple, List, Dict, Union, Any, Optional
from datetime import timedelta, datetime
from statistics import mean

from mhs_rdb.models import ColorsEnum, AlarmsModel

from .sequence import Sequence
from .sequence_manager import SequenceManager
from .result import Result


__all__ = ['BaseAlarmProcessor']


class BaseAlarmProcessor(ABC):
    """
    Base alarm processor.
    """

    _min_sequence_length: Optional[int] = None

    _max_sequence_length: Optional[int] = None

    def __init__(self,
                 data_points_timedelta: timedelta,
                 metric: Dict[str, Any],
                 low_red_level: Tuple[float, ColorsEnum],
                 low_yellow_level: Tuple[float, ColorsEnum],
                 high_yellow_level: Tuple[float, ColorsEnum],
                 high_red_level: Tuple[float, ColorsEnum],
                 values: List[Union[float, int]],
                 timestamps: List[datetime]):
        self._data_points_timedelta = data_points_timedelta
        self._metric = metric
        self._low_red_value, self._low_red_color = low_red_level
        self._low_yellow_value, self._low_yellow_color = low_yellow_level
        self._high_yellow_value, self._high_yellow_color = high_yellow_level
        self._high_red_value, self._high_red_color = high_red_level
        self._values = values
        self._timestamps = timestamps

    def _get_sequences(self) -> List[Sequence]:
        """
        Returns list of sequences.

        This method analyzes data points (self._values and self._timestamps lists)
        and creates sequences of exceeded values if such sequences were found.

        Sequence is the entity with sequential values which exceeds some threshold value
        for some period of time.

        New sequence will be created if any threshold value is exceeded.

        If the timedelta between two data points is bigger than self._data_points_timedelta
        then the current sequence will be interrupted.

        If metric has min value or max value and current data point value is not in this range
        then the current sequence will be interrupted.

        If the value continue to exceed the same threshold value
        then the current sequence will be extended with this value.

        If the value start exceeding other threshold value
        then the current sequence will be interrupted and the new one will be created.

        If the value stop exceeding any threshold value
        then the current sequence will be interrupted.

        The sequences can be used to create alarms.
        """

        sequence_manager = SequenceManager(self._min_sequence_length, self._max_sequence_length)
        for i, value in enumerate(self._values):
            timestamp = self._timestamps[i]

            if i > 0 and timestamp - self._data_points_timedelta > self._timestamps[i - 1]:
                sequence_manager.interrupt_sequence()

            if self._metric['min_value'] is not None and value < self._metric['min_value']:
                sequence_manager.interrupt_sequence()
                continue

            if self._metric['max_value'] is not None and value > self._metric['max_value']:
                sequence_manager.interrupt_sequence()
                continue

            if value < self._low_red_value:
                sequence_manager.add_value(self._low_red_value, self._low_red_color, value, timestamp)
                continue

            if value < self._low_yellow_value:
                sequence_manager.add_value(self._low_yellow_value, self._low_yellow_color, value, timestamp)
                continue

            if value > self._high_red_value:
                sequence_manager.add_value(self._high_red_value, self._high_red_color, value, timestamp)
                continue

            if value > self._high_yellow_value:
                sequence_manager.add_value(self._high_yellow_value, self._high_yellow_color, value, timestamp)
                continue

            sequence_manager.interrupt_sequence()

        sequence_manager.interrupt_sequence()

        return sequence_manager.sequences

    def process(self) -> Result:
        """
        Processes alarms.
        """

        sequences = self._get_sequences()
        result = Result()
        for sequence in sequences:

            alarm = AlarmsModel(
                metric_id=self._metric['id'],
                started_at=sequence.started_at,
                ended_at=sequence.ended_at,
                type_name=self._type_name,
                color=sequence.color,
                trigger_value=mean(sequence.values),
                trigger_criteria=sequence.criteria_value
            )

            result.add_alarm(self.__class__.__name__, alarm)

        return result

    @property
    @abstractmethod
    def _type_name(self) -> str:
        pass
