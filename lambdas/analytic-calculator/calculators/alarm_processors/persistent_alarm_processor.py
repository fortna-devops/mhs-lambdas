from .base_alarm_processor import BaseAlarmProcessor


__all__ = ['PersistentAlarmProcessor']


class PersistentAlarmProcessor(BaseAlarmProcessor):
    """
    Persistent alarm processor.
    """

    _type_name = 'iso_persistent'

    _min_sequence_length = 7
