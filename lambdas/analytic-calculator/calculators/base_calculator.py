from abc import ABC, abstractmethod
from typing import Dict, Any
from datetime import datetime

from mhs_rdb import ConfigManager


__all__ = ['BaseCalculator']


class BaseCalculator(ABC):
    """
    Base calculator.
    """

    def __init__(self, config_manager: ConfigManager, event: Dict[str, Any]):
        self._config_manager = config_manager
        self._customer = event['customer']
        self._site = event['site']
        self._data_source_ids = event['data_source_ids']
        self._datetime = datetime.utcfromtimestamp(event['timestamp'])

    @abstractmethod
    def calculate(self):
        pass
