from datetime import datetime

from calculators import DefaultCalculator
from calculators.alarm_processors.sequence import Sequence


def generate_metric(id,
                    name='rms_velocity_x',
                    display_name='RMS Vibrational Velocity X',
                    units=None,
                    min_value=None,
                    max_value=None,
                    threshold_levels=None):

    if not threshold_levels:
        threshold_levels = {}

    threshold_levels = {
        DefaultCalculator._threshold_level_names[name]: value for name, value in threshold_levels.items()
    }

    return {
        'id': id,
        'name': name,
        'display_name': display_name,
        'units': units,
        'min_value': min_value,
        'max_value': max_value,
        'threshold_levels': threshold_levels
    }


def generate_metric_data(id, values, minutes):

    return {
        id: {
            'values': values,
            'timestamps': [datetime(2020, 1, 1, 0, minute) for minute in minutes]
        }
    }


def generate_alarm_processor_init_args(metric,
                                       low_red_level,
                                       low_yellow_level,
                                       high_yellow_level,
                                       high_red_level,
                                       values,
                                       minutes):

    return [
        DefaultCalculator._data_points_timedelta,
        metric,
        low_red_level,
        low_yellow_level,
        high_yellow_level,
        high_red_level,
        values,
        [datetime(2020, 1, 1, 0, minute) for minute in minutes]
    ]


def generate_sequence(criteria_value, color, values, started_at_minute, ended_at_minute):

    sequence = Sequence(criteria_value, color)
    sequence._values = values

    if started_at_minute is not None:
        sequence._started_at = datetime(2020, 1, 1, 0, started_at_minute)

    if ended_at_minute is not None:
        sequence._ended_at = datetime(2020, 1, 1, 0, ended_at_minute)

    return sequence
