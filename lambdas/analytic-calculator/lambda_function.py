import os
import time
import logging

from typing import Tuple, List, Dict, Any, Type

import boto3

from mhs_rdb import ConfigManager, DataSources
from utils import LambdaInvoker
from calculators import BaseCalculator, DefaultCalculator


BOTO_SESSION = boto3.Session(profile_name=os.getenv('profile'))

CONFIG_MANAGER = ConfigManager(
    env=os.environ['env'],
    boto_session=BOTO_SESSION,
    debug=os.getenv('debug') == 'TRUE'
)

DATA_SOURCE_TYPE_NAMES: Tuple[str, ...] = ()

SITE_CALCULATORS: Dict[str, Type[BaseCalculator]] = {}


logger = logging.getLogger()

if logger.handlers:
    logger.setLevel(logging.INFO)
else:
    logging.basicConfig(level=logging.INFO)


class LambdaHandler:
    """
    Lambda handler.
    """

    def __init__(self,
                 boto_session: boto3.Session,
                 config_manager: ConfigManager,
                 data_source_type_names: Tuple[str, ...],
                 site_calculators: Dict[str, Type[BaseCalculator]]):
        self._boto_session = boto_session
        self._config_manager = config_manager
        self._data_source_type_names = data_source_type_names
        self._site_calculators = site_calculators

    def _get_data_source_ids(self, customer: str, site: str) -> List[int]:
        with self._config_manager.connector.get_session(customer, site) as session:
            return DataSources(session).get_ids(self._data_source_type_names)

    def _invoke(self):
        """
        Invokes lambda asynchronous for each slice of data sources for each site.
        """

        max_num_data_sources = int(os.environ['max_num_data_sources'])
        invoke_delay = float(os.environ['invoke_delay'])
        lambda_invoker = LambdaInvoker(self._boto_session)
        customer_sites = self._config_manager.customer_sites
        timestamp = time.time()
        for customer, sites in customer_sites.items():
            for site in sites:

                data_source_ids = self._get_data_source_ids(customer, site)
                for i in range(0, len(data_source_ids), max_num_data_sources):

                    ids = data_source_ids[i:i + max_num_data_sources]

                    logger.info(f"Site: '{site}'. Invoke lambda '{lambda_invoker.function_name}' "
                                f"for data sources with ids: {ids}.")

                    payload = {
                        'action': 'calculate',
                        'customer': customer,
                        'site': site,
                        'data_source_ids': ids,
                        'timestamp': timestamp
                    }
                    lambda_invoker.invoke(payload)
                    time.sleep(invoke_delay)

    def handle(self, event: Dict[str, Any]):
        """
        Handles event.
        """

        if event.get('action', 'invoke') == 'invoke':
            self._invoke()
            return

        logger.info(f"Start calculations. Site: '{event['site']}'. "
                    f"Data source ids: {event['data_source_ids']}.")

        site_calculator = self._site_calculators.get(event['site'], DefaultCalculator)
        site_calculator(self._config_manager, event).calculate()


def lambda_handler(event: Dict[str, Any], _context: Dict[str, Any]):
    LambdaHandler(BOTO_SESSION, CONFIG_MANAGER, DATA_SOURCE_TYPE_NAMES, SITE_CALCULATORS).handle(event)
