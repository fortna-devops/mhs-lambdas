import os
import json

from typing import Dict, Any

import boto3


__all__ = ['LambdaInvoker']


class LambdaInvoker:
    """
    Lambda invoker.
    """

    def __init__(self,
                 boto_session: boto3.Session,
                 function_name: str = os.environ['AWS_LAMBDA_FUNCTION_NAME'],
                 env: str = os.environ['env']):
        self._lambda_client = boto_session.client('lambda')
        self._function_name = function_name
        self._env = env

    @property
    def function_name(self) -> str:
        """
        Returns function name.
        """

        return self._function_name

    def invoke(self, payload: Dict[str, Any]):
        """
        Invokes lambda asynchronously.
        """

        self._lambda_client.invoke(
            InvocationType='Event',
            FunctionName=self._function_name,
            Qualifier=self._env,
            Payload=json.dumps(payload)
        )
