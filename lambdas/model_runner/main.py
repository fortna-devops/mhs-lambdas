import json
import os
import logging

import boto3
from mhs_rdb import ConfigManager


logger = logging.getLogger()
logging.basicConfig(level=logging.INFO)


logger.info('start lambda execution')
print('start lambda execution')
# get current env
ENV = os.getenv('env')
assert ENV, '"env" variable not specified'

logger.info(f'got env var {ENV}')
print(f'got env var {ENV}')

# get sagemaker endpoint name
ENDPOINT_NAME = os.getenv('endpoint_name')
assert ENDPOINT_NAME, '"endpoint_name" variable not specified'

logger.info(f'got endpoint name {ENDPOINT_NAME}')
print(f'got endpoint name {ENDPOINT_NAME}')

IGNORED_SITES = json.loads(os.getenv('ignored_sites', '[]'))

logger.info(f'got ignored sites {IGNORED_SITES}')
print(f'got ignored sites {IGNORED_SITES}')



def handler(event, _context):
    """
    Handler.
    """

    logger.info(f'Entered lambda handler')
    print(f'Entered lambda handler')


    profile = event.get('profile')

    logger.info(f'got profile {profile}')
    print(f'got profile {profile}')



    session = boto3.Session(profile_name=profile)

    logger.info(f'created boto session {session}')
    print(f'created boto session {session}')


    cm = ConfigManager(env=ENV, boto_session=session)
    runtime = session.client('sagemaker-runtime')


    logger.info(f'created sagemaker runtime client {runtime}')
    print(f'created sagemaker runtime client {runtime}')


    for customer_name, sites in cm.customer_sites.items():
        logger.info(f'iterating customer_name: {customer_name}, sites: {sites}')
        print(f'iterating customer_name: {customer_name}, sites: {sites}')
        for site_name in sites:
            if site_name in IGNORED_SITES:
                logger.info(f'Skipping "{site_name}" because it specified in "ignored_sites"')
                print(f'Skipping "{site_name}" because it specified in "ignored_sites"')
                continue

            logger.info(f'Invoking "{ENDPOINT_NAME}" model for site "{site_name}"')
            print(f'Invoking "{ENDPOINT_NAME}" model for site "{site_name}"')
            response = runtime.invoke_endpoint(
                EndpointName=ENDPOINT_NAME,
                ContentType='application/json',
                Body=json.dumps({
                    'customer_name': customer_name,
                    'site_name': site_name
                })
            )
            logger.info(f'Endpoint response: {response}')
            print(f'Endpoint response: {response}')


if __name__ == '__main__':
    handler({}, {})
