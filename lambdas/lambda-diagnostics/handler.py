#!/usr/bin/env python3
"""
creates and sends a more detailed error message when an AWS Lambda
fails. Queries the CloudWatch logs to find the source of the failure
..author: Caitlin Barron <caitlin.barron@mhsglobal.com>
"""
import boto3
from botocore.exceptions import ClientError
import logging
import json
import time
import calendar
from datetime import datetime
from jinja2 import Template
import os
import re
from typing import Union, Optional, Tuple, List, IO, Any
from analytics_sdk_core.data_init import data_pull, data_write_lite
from analytics_sdk_core.email_tools.email_tools import EmailSender
from analytics_sdk_core.config.config import ConfigManager, DEV_GROUP_KEY

logger = logging.getLogger()
logger.setLevel(logging.INFO)


# function to dynamically build the query string
def build_query_string(fields: List[str], direction: str, filter_args: List[str]) -> str:
    query_string = f'fields {", ".join(fields)} | sort @timestamp {direction} | filter'
    return f'{query_string} {" or ".join(filter_args)}'


def insight_query(client: boto3.client, log_group: str, range_start: int, range_end: int, query_str: str, query_limit: int) -> List[dict]:
    """
    function to run a cloudwatch insights query
    and return the response once finished
    """
    # queue the query
    query_response = client.start_query(
        logGroupName=log_group,
        startTime=range_start,
        endTime=range_end,
        queryString=query_str,
        limit=query_limit)

    # get ID of query
    id = query_response.get('queryId')

    # wait until status == completed
    query_status = 'Queued'
    while query_status == 'Queued' or query_status == 'Running':
        query = client.get_query_results(queryId=id)
        query_status = query.get('status')
        if query_status == 'Failed' or query_status == 'Cancelled':
            raise Exception(
                f"CloudWatch Insights query with the string '{query_str}' failed or was cancelled"
            )
        time.sleep(2)

    results = query.get('results')
    return results


# use a version number to find the corresponding alias
def get_lambda_alias(version_num:str, lambda_name:str) -> str:
    # get a list of all aliases for the given lambda
    lambda_client = boto3.client('lambda')
    alias_str = "Alias:"
    original_len = len(alias_str)
    response = lambda_client.list_aliases(FunctionName=lambda_name)
    aliases = response["Aliases"]
    # if there are no aliases at all for the lambda
    if not aliases:
        alias_str += " This lambda has no aliases"
    else:
        for alias in aliases:
            # if the version number applied to this alias
            if alias["FunctionVersion"] == version_num:
                # if more than one alias for a version
                if len(alias_str) > original_len:
                    alias_str += ","
                # add alias name to string
                alias_str += f" {alias['Name']} "

    # if none of the aliases match the version number
    if len(alias_str) <= original_len:
        alias_str += " This version has no alias"
    return alias_str


def str_to_epoch(time_str:str) -> int:
    """
    convert event timestamp to epoch time and make it a range for the query
    2019-04-05T12:30:47.502-04:00 <-- ISO_8601 time format
    """
    offset = time_str[-5]
    if offset == "+":
        split_str = time_str.split('+')
        short_str = split_str[0]
    else:
        split_str = time_str.split('-')
        split_str = split_str[:-1]
        short_str = "-".join(short_str)

    form = '%Y-%m-%dT%H:%M:%S.%f'
    time_struct = time.strptime(short_str, form)
    epoch = int(calendar.timegm(time_struct) * 1000)
    return epoch


def lambda_handler(event, context):
    # get event details from alarm message
    print(f"event: {event}")
    message_str = event.get('Records')[0].get('Sns').get('Message')
    message = json.loads(message_str)
    time_change = message.get('StateChangeTime') #string in UTC time
    print(f"diagnostics triggered by report at: {time_change}")

    trigger = message.get('Trigger')
    namespace = trigger.get('Namespace')
    lambda_name = trigger.get('Dimensions')[0].get('value')
    log_group = f"/{namespace.lower()}/{lambda_name}"

    es = EmailSender()
    #get email list from config manager
    active_env = os.environ['env']
    cm = ConfigManager(active_env)
    emails = cm.get_email_group(DEV_GROUP_KEY)

    # set up for CloudWatch Insights query
    client = boto3.client('logs')

    fields = ["@timestamp", "@message", "@logStream"]

    #use "/(?i)word/" for non case-sensitive searches
    filter_args = [
        "@message like /(?i)err/",          #filter for error
        "@message like /(?i)war/",          #filter for warning
        "@message like /(?i)trace/",        #filter for traceback
        "@message like /(?i)unable/",       #filter for unable
        #filter for exception and remove queries of the error table
        "@message like /(?i)except/ | filter @message not like /(?i)from error/"]
    query_str = build_query_string(fields, 'desc', filter_args)

    epoch = str_to_epoch(time_change)
    range_start = epoch - 300000   #event - 5 min
    range_end = epoch + 300000     #event + 5 min

    results = []
    logstreams=[]
    # add query to queue, response holds ID
    while not results:
        results = insight_query(
            client, log_group, range_start, range_end, query_str, 100)
        print(f"query results: {results}")
        if not results:
            time.sleep(30)

    # get results of query
    for result in results:
        error_time = result[0].get('value')
        error_msg = result[1].get('value')
        error_logstream = result[2].get('value')
        error_details = {
            'timestamp': error_time,
            'message': error_msg,
            'logstream': error_logstream
        }

        #if an email has already been sent for this logstream
        if error_logstream in logstreams:
            continue

        logstreams.append(error_logstream)

        #get version number and alias
        version_num = re.split("\[|\]", error_logstream)[1]
        alias = get_lambda_alias(version_num, lambda_name)

        print(f'an error or warning found reported at {error_time} in {error_logstream}')

        #get the whole log for this stream
        log_start = epoch - 172800000   #event - 2 days
        log_end = epoch + 30000         #event + 30 sec
        log_query_str = build_query_string(["@timestamp", "@message"],
                            'asc', [f'@logStream like "{error_logstream}"'])
        logs_results = insight_query(client, log_group, log_start, log_end, log_query_str, 10000)
        all_logs = []
        for result in logs_results:
            log = {}

            # get log timestamp
            log['timestamp'] = f"{result[0].get('value')} UTC"   #string in UTC time

            # get log message
            log['message'] = result[1].get('value')
            all_logs.append(log)

        # get template for email
        with open('email_template.html', 'r') as f:
            template = Template(f.read())

        # fill out template with message
        email_message = template.render(
            failed_lambda=log_group, version_alias=alias, error=error_details, all_logs=all_logs)

        #send emails
        es.send_emails(email_message, f"AWS {lambda_name} Failure", emails)
        print(f"email(s) sent for {error_logstream}")
    print("done!")

"""
with open('event.json','r') as f:
    event = json.load(f)
    lambda_handler(event, {})
"""