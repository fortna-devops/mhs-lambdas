#Lambda Diagnostics
----------------------------
#### Background
Finds and returns a more detailed error report when a lambda fails. This lambda is triggered when a lambda failure is detected and reported by the "lambda_failure" SNS topic. The event details are then used to query the Cloudwatch Insights Logs and find the corresponding error. A detailed email will then be sent with a full error report and information about what failed and when.


----------------------------
#### Methodology
An event is given passed into the lambda handler that includes many details about what lambda failed, when it failed, and what signaled the failure.

Example of an Event:

```python
event = {"Records": [{"EventSource": "aws:sns", "EventVersion": "1.0", "EventSubscriptionArn": "arn:aws:sns:us-east-1:286214959794:lambda_failure:296a80dc-3247-43bf-90d4-da0fb05e36d2", "Sns": {"Type": "Notification", "MessageId": "7cd3bc8f-6d99-5e87-aa89-e9d1481d53a8", "TopicArn": "arn:aws:sns:us-east-1:286214959794:lambda_failure", "Subject": "ALARM: \"Demo Lambda Failed\" in US East (N. Virginia)", "Message": "{\"AlarmName\":\"Demo Lambda Failed\",\"AlarmDescription\":null,\"AWSAccountId\":\"286214959794\",\"NewStateValue\":\"ALARM\",\"NewStateReason\":\"Threshold Crossed: 1 out of the last 24 datapoints [1.0 (21/05/19 16:06:00)] was greater than the threshold (0.0) (minimum 1 datapoint for OK -> ALARM transition).\",\"StateChangeTime\":\"2019-05-21T16:11:48.730+0000\",\"Region\":\"US East (N. Virginia)\",\"OldStateValue\":\"INSUFFICIENT_DATA\",\"Trigger\":{\"MetricName\":\"Errors\",\"Namespace\":\"AWS/Lambda\",\"StatisticType\":\"Statistic\",\"Statistic\":\"AVERAGE\",\"Unit\":null,\"Dimensions\":[{\"value\":\"demo-lambda\",\"name\":\"FunctionName\"}],\"Period\":300,\"EvaluationPeriods\":24,\"ComparisonOperator\":\"GreaterThanThreshold\",\"Threshold\":0.0,\"TreatMissingData\":\"- TreatMissingData: missing\",\"EvaluateLowSampleCountPercentile\":\"\"}}", "Timestamp": "2019-05-21T16:11:48.825Z", "SignatureVersion": "1", "Signature": "or8CYPlGYhVdgpVe/vJbkILK3S6NgSy2jW11m5Jcxrk7CO+L278M94vNwrf3hRTdm/epRSWLDWVZawQOVvdB6nEr9qlr5dgABxM1Z6U92vjn697HmiZySaKxA6GGLZ4w2L4TkFwvKonIYG1vDM2nOwefMyxa4JdscLKbwOiO/1Tw8K+GdCxQ8R0vWG9TzeAuC93ie+OavifVg1yQeHxPcVQwov/snL820legwDGF4OizkolLSxlfoG4qpuACkuHJyjDPQqknoczGAetLh5333iYi6KcnnV0MC9MnxLHVtgzRQyK2+idCOhUJLwldkFhkmBLUjDNebs85o3z0X6MBSA==", "SigningCertUrl": "https://sns.us-east-1.amazonaws.com/SimpleNotificationService-6aad65c2f9911b05cd53efda11f913f9.pem", "UnsubscribeUrl": "https://sns.us-east-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-east-1:286214959794:lambda_failure:296a80dc-3247-43bf-90d4-da0fb05e36d2", "MessageAttributes": {}}}]}
```


----------------------------
#### Configurable Parameters:
None.

----------------------------
#### Lambda Triggers:
Invoked by events in the "lambda_failure" topic
