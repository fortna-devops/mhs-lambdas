#!/usr/bin/env python3

# from moto import mock_lambda, mock_cloudwatch, mock_logs
from handler import insight_query, build_query_string, get_lambda_alias, str_to_epoch
# import boto3
# import zipfile
# import time
# import pytest
# import pdb
import re


def test_query_string_two_filter_fields():
    fields = ["@timestamp", "@message"]
    filter_args = [
        "@message like /(?i)err/",
        "@message like /(?i)war/"]
    response = build_query_string(fields, 'desc', filter_args)
    expected = "fields @timestamp, @message | sort @timestamp desc | filter @message like /(?i)err/ or @message like /(?i)war/"
    assert response == expected


def test_query_string_one_filter_fields():
    fields = ["@timestamp"]
    filter_args = ["@message like /(?i)err/"]
    response = build_query_string(fields, 'asc', filter_args)
    expected = "fields @timestamp | sort @timestamp asc | filter @message like /(?i)err/"
    assert response == expected


def test_time_conversion():
    utc = "2019-06-14T10:03:42.014+0000"
    response = str_to_epoch(utc)
    expected = 1560506622000
    assert response == expected


# def mock_lambda_event():
#     code = '''
#         def lambda_handler(event, context):
#             print(event)
#             return event
#         '''
#     zip_output = io.BytesIO()
#     zip_file = zipfile.ZipFile(zip_output, 'w', zipfile.ZIP_DEFLATED)
#     zip_file.writestr('lambda_function.py', code)
#     zip_file.close()
#     zip_output.seek(0)
#     return zip_output.read()


# def create_mock_lambda(lambda_client, lambda_name, return_event):
#     return lambda_client.create_function(
#         FunctionName=lambda_name,
#         Runtime='python2.7',
#         Role='arn:iam:test-iam-role:us-west-2:123456789012:test-iam-role',
#         Handler='lambda_function.lambda_handler',
#         Code={
#             'ZipFile': return_event,
#         },
#         Publish=True,
#         Timeout=3,
#         MemorySize=128)


# @mock_lambda
# def test_get_lambda_alias():
#     lambda_client = boto3.client('lambda')

#     create_mock_lambda(lambda_client, "test_lambda", mock_lambda_event)
#     lambda_client.create_alias(
#         FunctionName="test_lambda",
#         Name="Development",
#         FunctionVersion="1")

#     lambda_client.invoke(FunctionName="test_lambda")

#     result_alias = get_lambda_alias("1", "test_lambda")
#     assert result_alias == "Development"


# @mock_lambda
# def test_get_lambda_alias_no_alias_for_version():
#     lambda_client = boto3.client('lambda')

#     create_mock_lambda(lambda_client, "test_lambda", mock_lambda_event)
#     lambda_client.create_alias(
#         FunctionName="test_lambda",
#         Name="Development",
#         FunctionVersion="1")

#     lambda_client.update_function_configuration(FunctionName="test_lambda", Timeout=30)
#     lambda_client.publish_version(FunctionName="test_lambda")

#     lambda_client.invoke(FunctionName="test_lambda")

#     result_alias = get_lambda_alias("2", "test_lambda")
#     assert result_alias == "Alias: This version has no alias"


# @mock_lambda
# def test_get_lambda_alias_no_alias_at_all():
#     lambda_client = boto3.client('lambda')

#     create_mock_lambda(lambda_client, "test_lambda", mock_lambda_event)

#     lambda_client.invoke(FunctionName="test_lambda")

#     result_alias = get_lambda_alias("1", "test_lambda")
#     assert result_alias == "Alias: This lambda has no aliases"



# @mock_logs
# def test_insight_query():
#     #pdb.set_trace()
#     log_client = boto3.client('logs', 'us-east-1')

#     log_group = "/aws/lambda/fake_lambda"
#     log_stream = "test_stream"
#     query_str = "fields @timestamp, @message | sort @timestamp desc"

#     log_client.create_log_group(logGroupName=log_group)
#     log_client.create_log_stream(logGroupName=log_group, logStreamName=log_stream)

#     log_client.put_log_events(
#             logGroupName=log_group,
#             logStreamName=log_stream,
#             logEvents=[
#                 {"timestamp":12345, "message":"oranges"},
#                 {"timestamp":12445, "message":"apples"}])

#     res = log_client.get_log_events(
#         logGroupName=log_group,
#         logStreamName=log_stream
#     )
#     print(res['events'])

#     time.sleep(2)
#     epoch_time = int(time.time()) * 1000
#     range_start = epoch_time-3600000
#     range_end = epoch_time+3600000
#     print(range_start)
#     print(range_end)

#     results = insight_query(log_client, log_group, range_start, range_end, query_str, 10)

#     expected_results = [
#                             [{"field":"timestamp", "value":12445},
#                                 {"field":"message", "value":"apples"}],
#                             [{"field":"timestamp", "value":12345},
#                                 {"field":"message", "value":"oranges"}]
#                         ]

#     assert results == expected_results

