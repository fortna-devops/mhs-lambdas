import json
import logging
from datetime import datetime, timedelta
from typing import Type, Dict, Any, Tuple, List, Optional

from mhs_rdb.utils.data import DataPoints, DataProxy
from mhs_web_utils.authorizer import AuthorizationError
from mhs_web_utils.validation import Validator, ListField, AnyField
from mhs_web_utils.response import Response, JSONResponse

from mhs_rdb.models import MetricsModel

from .base_handler import BaseHandler
from utils import Context, StructureMapping


__all__ = ['DefaultHandler']


logger = logging.getLogger()


class DataValidator(Validator):
    data = ListField(AnyField())


class DefaultHandler(BaseHandler):

    validator_cls: Type[Validator] = DataValidator
    context_cls: Type[Context] = Context

    request_timestamp_key: str = 'timestamp'
    request_timestamp_format: str = '%Y-%m-%d %H:%M:%S.%f'

    request_structure_key: Tuple[str, str, str] = ('asset_name', 'component_type', 'data_source_type')
    request_metric_groups_key: str = 'metric_groups'

    # Cloud data structure attributes in the next order: asset, component, data_source, metric
    cloud_structure_key: Tuple[str, str, str, str] = ('name', 'type.name', 'type.name', 'name')

    def handle(self) -> Response:
        data = self.validator_cls(self._data).validate()

        cognito_client_id = self._token.data['client_id']
        assets = self._get_assets(cognito_client_id)
        if not assets:
            raise AuthorizationError(f'No gateway found for site {self._site} '
                                     f'with cognito_client_id {cognito_client_id}')

        self._update_gateway_heartbeat(cognito_client_id)

        context = self._get_context()
        context.cloud_structure_mapping = StructureMapping(assets, self.cloud_structure_key)
        context.data_points = DataPoints(DataProxy.raw_data_kind)
        context.errors = []

        for data_group in data['data']:
            self._process_data_group(data_group, context)

        self._insert_data(context)

        return self._get_response(context)

    def _get_context(self) -> Context:
        return self.context_cls()

    def _insert_data(self, context: Context):
        with self._connector.get_session(self._customer, self._site) as session:
            DataProxy(session).insert(context.data_points)

    def _get_response(self, context: Context) -> Response:
        return JSONResponse({
            'num_inserted_rows': len(context.data_points),
            'errors': context.errors
        })

    def _get_flags(self, name: str, value: Any, context: Context) -> int:
        return 0

    def _get_data_source_key(self, data_group: Dict[str, Any], context: Context) -> Optional[Tuple[Any, ...]]:
        data_source_key: List[Any] = []
        for key_part in self.request_structure_key:
            structure_part = data_group.get(key_part)
            if not structure_part:
                msg = f'No "{key_part}" key found in data group'
                logger.warning(msg)
                context.errors.append(msg)
                return None
            data_source_key.append(structure_part)

        return tuple(data_source_key)

    def _get_metric_groups(self, data_group: Dict[str, Any], context: Context) -> Optional[List[Dict[str, Any]]]:
        metric_groups = data_group.get(self.request_metric_groups_key)
        if not metric_groups:
            msg = f'No "{self.request_metric_groups_key}" key found in data group'
            logger.warning(msg)
            context.errors.append(msg)
            return None

        return metric_groups

    def _get_timestamp(self, metric_group: Dict[str, Any], context: Context) -> Optional[datetime]:
        timestamp = metric_group.pop(self.request_timestamp_key, None)
        if not timestamp:
            msg = f'No "{self.request_timestamp_key}" key found in metrics group'
            logger.warning(msg)
            context.errors.append(msg)
            return None

        try:
            timestamp = datetime.strptime(timestamp, self.request_timestamp_format)
        except ValueError:
            msg = f'"{timestamp}" does not match "{self.request_timestamp_format}" format'
            logger.warning(msg)
            context.errors.append(msg)
            return None

        return timestamp

    def _get_metric(self, name: str, context: Context) -> Optional[MetricsModel]:
        metric_key = *context.data_source_key, name
        metric = context.cloud_structure_mapping.get_element(*metric_key)
        if not metric:
            msg = f'No metric found for key ({", ".join(metric_key)})'
            logger.warning(msg)
            context.errors.append(msg)

        return metric

    def _process_data_group(self, data_group: Dict[str, Any], context: Context):
        data_source_key = self._get_data_source_key(data_group, context)
        if not data_source_key:
            return

        context.data_source_key = data_source_key

        metric_groups = self._get_metric_groups(data_group, context)
        if not metric_groups:
            return

        for data_element in metric_groups:
            self._process_metric_group(data_element, context)

    def _process_metric_group(self, metric_group: Dict[str, Any], context: Context):
        timestamp = self._get_timestamp(metric_group, context)
        if not timestamp:
            return

        context.timestamp = timestamp
        context.metric_group = metric_group

        for name, value in metric_group.items():
            self._process_metric(name, value, context)

    def _process_metric(self, name: str, value: Any, context: Context):
        metric = self._get_metric(name, context)
        if not metric:
            return

        flags = self._get_flags(name, value, context)
        context.data_points.add_data(metric=metric, timestamp=context.timestamp, value=value, flags=flags)


TEST_ENV = {
    'app_client_id': '4cdoq1iq6ssdl8ojkjtj9akovh',
    'app_client_secret': 'emccn7hopjmp7tcnrvle6tqm5d0dkbhrf49aih8b0nkie5112at',
    'host': 'https://dev-gate-pool.auth.us-east-1.amazoncognito.com/oauth2/token',
}

TEST_EVENT_TIMESTAMP = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')
TEST_EVENT_TIMEDELTA_TIMESTAMP = (datetime.utcnow() - timedelta(hours=2)).strftime('%Y-%m-%d %H:%M:%S.%f')


TEST_EVENT = {
    'httpMethod': 'POST',
    'path': 'data',
    'body': json.dumps({
        'site': 'dhl-brescia',
        'data': [
            {
                'asset_name': 'Z01MC100',
                'component_type': 'motor',
                'data_source_type': 'vfd',
                'metric_groups': [
                    {
                        'speed': 1.1,
                        'current': 2.1,
                        'voltage': 3.1,
                        'mechanical_power': 4.1,
                        'cos_phi': 0.1,
                        'timestamp': TEST_EVENT_TIMESTAMP
                    },
                    {
                        'speed': 1.1,
                        'current': 2.1,
                        'voltage': 0,
                        'mechanical_power': 4.1,
                        'cos_phi': 0.1,
                        'timestamp': TEST_EVENT_TIMESTAMP
                    },
                    {
                        'speed': 1.1,
                        'current': 2.1,
                        'voltage': 3.1,
                        'mechanical_power': 4.1,
                        'cos_phi': 0.1,
                        'timestamp': TEST_EVENT_TIMEDELTA_TIMESTAMP
                    }
                ]
            },
            {
                'asset_name': 'sorter 4',
                'component_type': 'motor',
                'data_source_type': 'vfd',
                'metric_groups': [
                    {
                        'speed': 1.1,
                        'current': 2.1,
                        'voltage': 3.1,
                        'mechanical_power': 4.1,
                        'cos_phi': 0.1,
                        'timestamp': TEST_EVENT_TIMESTAMP
                    }
                ]
            }
        ]
    })
}
