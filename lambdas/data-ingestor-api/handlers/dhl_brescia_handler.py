import json
import logging

from typing import Tuple, List, Dict, Any, Optional
from datetime import datetime, timedelta

from sqlalchemy.orm import contains_eager

from mhs_web_utils.authorizer import AuthorizationError
from mhs_web_utils.response import Response, JSONResponse
from mhs_web_utils.validation import Validator, StrField, FloatField, BaseFiled, DateTimeField, ListField,\
    NestedDataField, FieldValidationError

from mhs_rdb import Warnings, Errors, Alarms, AssetStatusTransitions, Gateways
from mhs_rdb.models import GatewaysModel, DataStatusesEnum, ColorsEnum, AlarmsModel, \
    AssetStatusTransitionsModel, AssetStatusesEnum, DataSourcesModel, ComponentsModel
from mhs_rdb.utils.data import DataPoints, DataProxy

from .base_handler import BaseHandler


__all__ = ['DHLBresciaHandler']


logger = logging.getLogger()


TIMESTAMP_FORMAT = '%Y-%m-%d %H:%M:%S.%f'


class IntField(BaseFiled):
    """
    Int field.
    """

    def _clean(self, value: str) -> float:
        try:
            return int(float(value))
        except ValueError:
            raise FieldValidationError(f'"{value}" is not convertible to int')


class MetricsValidator(Validator):
    """
    Metrics validator.
    """

    speed = FloatField(required=False)
    current = FloatField(required=False)
    voltage = FloatField(required=False)
    mechanical_power = FloatField(required=False)
    cos_phi = FloatField(required=False)
    timestamp = DateTimeField(value_format=TIMESTAMP_FORMAT)


class TemperaturesValidator(Validator):
    """
    Temperatures validator.
    """

    heat_sink_temperature = FloatField(required=False)
    timestamp = DateTimeField(value_format=TIMESTAMP_FORMAT)


class LifetimeValidator(Validator):
    """
    Lifetime validator.
    """

    operating_time = FloatField(required=False)
    running_time = FloatField(required=False)
    timestamp = DateTimeField(value_format=TIMESTAMP_FORMAT)


class StatuesValidator(Validator):
    """
    Statuses validator.
    """

    status_word = IntField(required=False)
    fault = StrField(required=False)
    warning = StrField(required=False)
    timestamp = DateTimeField(value_format=TIMESTAMP_FORMAT)


class DataElementValidator(Validator):
    """
    Data element validator.
    """

    asset_name = StrField()
    component_type = StrField()
    data_source_type = StrField()
    metrics = ListField(NestedDataField(MetricsValidator), required=False)
    temperatures = ListField(NestedDataField(TemperaturesValidator), required=False)
    lifetimes = ListField(NestedDataField(LifetimeValidator), required=False)
    statuses = ListField(NestedDataField(StatuesValidator), required=False)

    def validate(self) -> Dict[str, Any]:
        """
        Returns validated args. Sets 'asset_name', 'component_type' and 'data_source_type' to lowercase.
        """

        args = super().validate()

        for key in 'asset_name', 'component_type', 'data_source_type':
            args[key] = args[key].lower()

        return args


class DataValidator(Validator):
    """
    Data validator.
    """

    data = ListField(NestedDataField(DataElementValidator))


class DHLBresciaHandler(BaseHandler):
    """
    DHL Brescia handler.
    """

    _group_names = (
        'metrics',
        'temperatures',
        'lifetimes',
        'statuses'
    )

    _min_voltage = 0

    _warning_alarm_color = ColorsEnum.YELLOW

    _warning_alarm_type_name = 'warning'

    _error_alarm_color = ColorsEnum.RED

    _error_alarm_type_name = 'error'

    def _get_gateway(self, cognito_client_id: str) -> Optional[GatewaysModel]:
        """
        Returns gateway.
        """

        with self._connector.get_session(self._customer, self._site) as session:
            return Gateways(session).query\
                .options(
                    contains_eager(
                        Gateways.model.data_sources,
                        DataSourcesModel.component,
                        ComponentsModel.asset
                    ),
                    contains_eager(
                        Gateways.model.data_sources,
                        DataSourcesModel.metrics
                    ),
                    contains_eager(
                        Gateways.model.data_sources,
                        DataSourcesModel.component,
                        ComponentsModel.type
                    ),
                    contains_eager(
                        Gateways.model.data_sources,
                        DataSourcesModel.type
                    )
                )\
                .filter(Gateways.model.cognito_client_id == cognito_client_id)\
                .join(Gateways.model.data_sources)\
                .join(DataSourcesModel.component)\
                .join(ComponentsModel.asset)\
                .join(DataSourcesModel.metrics)\
                .join(ComponentsModel.type)\
                .join(DataSourcesModel.type)\
                .one_or_none()

    def _get_entities(self,
                      asset_attr_name: str = 'name',
                      component_attr_name: str = 'type.name',
                      data_source_attr_name: str = 'type.name',
                      metric_attr_name: str = 'name') -> Dict[Tuple[str, ...], Any]:
        """
        Returns entities for asset attr value, component attr value, data source attr value and metric attr value.
        Converts all values to lowercase to keep it case insensitive.

        Example of return values:
        {
            ('sorter 1',): <AssetsModel object>,
            ('sorter 1', 'motor 1'): <ComponentsModel object>,
            ('sorter 1', 'motor 1', 'qm42vt2'): <DataSourcesModel object>,
            ('sorter 1', 'motor', 'qm42vt2', 'temperature'): <MetricsModel object>
        }
        """

        cognito_client_id = self._token.data['client_id']
        gateway = self._get_gateway(cognito_client_id)

        if not gateway:
            error = f'No gateway found for site {self._site} with cognito_client_id {cognito_client_id}'
            raise AuthorizationError(error)

        entities: Dict[Tuple[str, ...], Any] = {}
        for data_source in gateway.data_sources:

            asset_attr_value = getattr(data_source.component.asset, asset_attr_name).lower()
            asset_key = (asset_attr_value,)
            if asset_key not in entities:
                entities[asset_key] = data_source.component.asset

            if '.' in component_attr_name:
                outer_attr_name, inner_attr_name = component_attr_name.split('.')
                component_attr_value = getattr(getattr(data_source.component, outer_attr_name), inner_attr_name).lower()
            else:
                component_attr_value = getattr(data_source.component, component_attr_name).lower()
            component_key = (asset_attr_value, component_attr_value)
            if component_key not in entities:
                entities[component_key] = data_source.component

            if '.' in data_source_attr_name:
                outer_attr_name, inner_attr_name = data_source_attr_name.split('.')
                data_source_attr_value = getattr(getattr(data_source, outer_attr_name), inner_attr_name).lower()
            else:
                data_source_attr_value = getattr(data_source, data_source_attr_name).lower()
            data_source_key = (asset_attr_value, component_attr_value, data_source_attr_value)
            if data_source_key not in entities:
                entities[data_source_key] = data_source

            for metric in data_source.metrics:
                metric_attr_value = getattr(metric, metric_attr_name).lower()
                metric_key = (asset_attr_value, component_attr_value, data_source_attr_value, metric_attr_value)
                if metric_key not in entities:
                    entities[metric_key] = metric

        return entities

    def _get_entity(self,
                    entities: Dict[Tuple[str, ...], Any],
                    asset_attr_value: str,
                    component_attr_value: Optional[str] = None,
                    data_source_attr_value: Optional[str] = None,
                    metric_attr_value: Optional[str] = None) -> Optional[Any]:
        """
        Returns entity from entities.
        """

        if component_attr_value is not None and data_source_attr_value is not None and metric_attr_value is not None:

            metric = entities.get((asset_attr_value, component_attr_value, data_source_attr_value, metric_attr_value))
            if metric is None:
                logger.warning(f"'{metric_attr_value}' metric not found for '{data_source_attr_value}' data source for "
                               f"'{component_attr_value}' component for '{asset_attr_value}' asset for "
                               f"'{self._site}' site.")

            return metric

        if component_attr_value is not None and data_source_attr_value is not None:

            data_source = entities.get((asset_attr_value, component_attr_value, data_source_attr_value))
            if data_source is None:

                logger.warning(f"'{data_source_attr_value}' data source not found for "
                               f"'{component_attr_value}' component for "
                               f"'{asset_attr_value}' asset for '{self._site}' site.")

            return data_source

        if component_attr_value is not None:

            component = entities.get((asset_attr_value, component_attr_value))
            if component is None:

                logger.warning(f"'{component_attr_value}' component not found for '{asset_attr_value}' asset for "
                               f"'{self._site}' site.")

            return component

        asset = entities.get((asset_attr_value,))
        if asset is None:
            logger.warning(f"'{asset_attr_value}' asset not found for '{self._site}' site.")

        return asset

    def _handle_metrics_group_element(self,
                                      entities: Dict[Tuple[str, ...], Any],
                                      asset_status_transitions: List[AssetStatusTransitionsModel],
                                      asset_name: str,
                                      attrs: Dict[str, Any],
                                      timestamp: datetime) -> int:
        """
        Handles metrics group element. Adds assets statuses.

        Returns flags based on voltage value.
        For now we assume that asset was off when voltage == 0.
        """

        voltage = attrs.get('voltage')
        if voltage is None or voltage > self._min_voltage:
            flags = 0
        else:
            flags = DataStatusesEnum.asset_off.value

        asset = self._get_entity(entities, asset_name)
        if asset is not None:
            asset_status_transition = AssetStatusTransitionsModel(
                asset_id=asset.id,
                status=AssetStatusesEnum.ON if not flags else AssetStatusesEnum.OFF,
                timestamp=timestamp

            )
            asset_status_transitions.append(asset_status_transition)

        return flags

    def _get_alarm_descriptions(self, description_service: Any) -> Dict[str, str]:
        """
        Returns alarm descriptions.
        """

        def result_handler(rows: List[Any]) -> Dict[str, str]:
            """
            Handles result.
            """

            result: Dict[str, str] = {}
            for row in rows:
                result[row.name] = row.description

            return result

        return self._cache_service_call(description_service.__name__, description_service, 'get_all', result_handler)

    def _handle_statuses_group_element(self,
                                       entities: Dict[Tuple[str, ...], Any],
                                       alarms: List[AlarmsModel],
                                       asset_name: str,
                                       component_type: str,
                                       data_source_type: str,
                                       attrs: Dict[str, Any],
                                       timestamp: datetime):
        """
        Handles statuses group element. Adds alarms.
        """

        for attr_name, attr_value in attrs.items():

            if attr_name not in ('warning', 'fault'):
                continue

            metric = self._get_entity(entities, asset_name, component_type, data_source_type, attr_name)
            if metric is None:
                continue

            alarm_kwargs = {
                'metric_id': metric.id,
                'started_at': timestamp,
                'ended_at': timestamp,
                'type_name': self._warning_alarm_type_name,
                'color': self._warning_alarm_color,
                'trigger_value': 0,
                'trigger_criteria': 0
            }
            description_service = Warnings
            description_code = attr_value

            if attr_name == 'fault':
                alarm_kwargs['type_name'] = self._error_alarm_type_name
                alarm_kwargs['color'] = self._error_alarm_color
                description_service = Errors

            descriptions = self._get_alarm_descriptions(description_service)
            description = descriptions.get(description_code)
            if description is None:
                logger.warning(f"No description found for code '{description_code}' for '{attr_name}' metric "
                               f"for '{self._site}' site.")
                continue

            alarm_kwargs['static_description'] = f"Code {description_code}: {description}"
            alarm = AlarmsModel(**alarm_kwargs)
            alarms.append(alarm)

    def _handle_group_elements(self,
                               entities: Dict[Tuple[str, ...], Any],
                               asset_status_transitions: List[AssetStatusTransitionsModel],
                               data_points: DataPoints,
                               alarms: List[AlarmsModel],
                               asset_name: str,
                               component_type: str,
                               data_source_type: str,
                               group_name: str,
                               group_elements: List[Dict[str, Any]]):
        """
        Handles group elements. Adds data points.
        """

        for group_element in group_elements:

            attrs = {
                attr_name: attr_value for attr_name, attr_value in group_element.items() if attr_value is not None
            }

            group_element_keys = group_element.keys() - {'timestamp'}
            if len(group_element_keys - attrs.keys()) == len(group_element_keys):
                logger.warning(f"{group_element_keys} attributes not found for '{asset_name}' asset "
                               f"for '{component_type}' component for '{data_source_type}' data source "
                               f"for '{group_name}' group.")
                continue

            timestamp = attrs.pop('timestamp')
            flags = None
            if group_name == 'metrics':
                flags = self._handle_metrics_group_element(
                    entities,
                    asset_status_transitions,
                    asset_name,
                    attrs,
                    timestamp
                )

            if group_name == 'temperatures':
                attrs['temperature'] = attrs['heat_sink_temperature']
                del attrs['heat_sink_temperature']

            if group_name == 'statuses':
                self._handle_statuses_group_element(
                    entities,
                    alarms,
                    asset_name,
                    component_type,
                    data_source_type,
                    attrs,
                    timestamp
                )

            for attr_name, attr_value in attrs.items():

                metric = self._get_entity(entities, asset_name, component_type, data_source_type, attr_name)
                if metric is None:
                    continue

                data_points.add_data(metric=metric, timestamp=timestamp, value=attr_value, flags=flags)

    def handle(self) -> Response:
        """
        Handles data.
        """

        # update heartbeat timestamp
        self._update_gateway_heartbeat(
            cognito_client_id=self._token.data['client_id'])

        self._data = DataValidator(self._data).validate()
        entities = self._get_entities(
            asset_attr_name='name',
            component_attr_name='type.name',
            data_source_attr_name='type.name',
            metric_attr_name='name'
        )

        asset_status_transitions: List[AssetStatusTransitionsModel] = []
        data_points = DataPoints(DataProxy.raw_data_kind)
        alarms: List[AlarmsModel] = []
        for data_element in self._data['data']:

            asset_name = data_element['asset_name']
            component_type = data_element['component_type']
            data_source_type = data_element['data_source_type']
            for group_name in self._group_names:

                group_elements = data_element.get(group_name)
                if not group_elements:
                    continue

                self._handle_group_elements(
                    entities,
                    asset_status_transitions,
                    data_points,
                    alarms,
                    asset_name,
                    component_type,
                    data_source_type,
                    group_name,
                    group_elements
                )

        if asset_status_transitions:
            with self._connector.get_session(self._customer, self._site) as session:
                AssetStatusTransitions(session).insert_all(asset_status_transitions)

        if alarms:
            with self._connector.get_session(self._customer, self._site) as session:
                Alarms(session).insert_all(alarms)

        with self._connector.get_session(self._customer, self._site) as session:
            DataProxy(session).insert(data_points)

        return JSONResponse({'num_rows': len(data_points)})


TEST_ENV = {
    'app_client_id': '4cdoq1iq6ssdl8ojkjtj9akovh',
    'app_client_secret': 'emccn7hopjmp7tcnrvle6tqm5d0dkbhrf49aih8b0nkie5112at',
    'host': 'https://dev-gate-pool.auth.us-east-1.amazoncognito.com/oauth2/token',
}

TEST_EVENT_TIMESTAMP = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')
TEST_EVENT_TIMEDELTA_TIMESTAMP = (datetime.utcnow() - timedelta(hours=2)).strftime('%Y-%m-%d %H:%M:%S.%f')
TEST_EVENT = {
    'httpMethod': 'POST',
    'path': 'data',
    'body': json.dumps({
        'site': 'dhl-brescia',
        'data': [
            {
                'asset_name': 'Z01MC100',
                'component_type': 'vfd',
                'data_source_type': 'vfd',
                'metrics': [
                    {
                        'speed': '1.1',
                        'current': '2.1',
                        'voltage': '3.1',
                        'mechanical_power': '4.1',
                        'cos_phi': '0.1',
                        'timestamp': TEST_EVENT_TIMESTAMP
                    },
                    {
                        'speed': '1.1',
                        'current': '2.1',
                        'voltage': '0',
                        'mechanical_power': '4.1',
                        'cos_phi': '0.1',
                        'timestamp': TEST_EVENT_TIMESTAMP
                    },
                    {
                        'speed': '1.1',
                        'current': '2.1',
                        'voltage': '3.1',
                        'mechanical_power': '4.1',
                        'cos_phi': '0.1',
                        'timestamp': TEST_EVENT_TIMEDELTA_TIMESTAMP
                    }
                ],
                'temperatures': [
                    {
                        'heat_sink_temperature': '5.1',
                        'timestamp': TEST_EVENT_TIMESTAMP
                    }
                ],
                'lifetimes': [
                    {
                        'operating_time': '6.1',
                        'running_time': '7.1',
                        'timestamp': TEST_EVENT_TIMESTAMP
                    }
                ],
                'statuses': [
                    {
                        'status_word': '8',
                        'timestamp': TEST_EVENT_TIMESTAMP
                    },
                    {
                        'warning': '1.0',
                        'timestamp': TEST_EVENT_TIMESTAMP
                    },
                    {
                        'fault': '10.0',
                        'timestamp': TEST_EVENT_TIMEDELTA_TIMESTAMP
                    }
                ]
            },
            {
                'asset_name': 'sorter 4',
                'component_type': 'motor',
                'data_source_type': 'vfd',
                'metrics': [
                    {
                        'speed': '1.1',
                        'current': '2.1',
                        'voltage': '3.1',
                        'mechanical_power': '4.1',
                        'cos_phi': '0.1',
                        'timestamp': TEST_EVENT_TIMESTAMP
                    }
                ]
            }
        ]
    })
}
