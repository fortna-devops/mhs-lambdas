import logging

from abc import ABC, abstractmethod
from datetime import datetime
from typing import Dict, Any, Optional, Callable, List

from mhs_rdb import Connector, Assets, Gateways
from mhs_rdb.models import GatewaysModel, ComponentsModel, DataSourcesModel, AssetsModel

from sqlalchemy.orm import contains_eager

from mhs_web_utils.response import Response
from mhs_web_utils.authorizer.cognito.token import Token


__all__ = ['BaseHandler']


logger = logging.getLogger()


class BaseHandler(ABC):
    """
    Base handler.
    """

    def __init__(self,
                 connector: Connector,
                 token: Token,
                 headers: Dict[str, str],
                 customer: str,
                 site: str,
                 data: Dict[str, Any]):
        self._connector = connector
        self._token = token
        self._headers = headers
        self._customer = customer
        self._site = site
        self._data = data
        self._cache: Dict[str, Any] = {}

    def _get_assets(self, cognito_client_id: str) -> List[AssetsModel]:
        """
        Get assets and related objects for the specified `cognito_client_id`
        """
        with self._connector.get_session(self._customer, self._site) as session:
            return Assets(session).query\
                .options(
                    contains_eager(
                        AssetsModel.components,
                        ComponentsModel.data_sources,
                        DataSourcesModel.metrics,
                    ),
                    contains_eager(
                        AssetsModel.components,
                        ComponentsModel.type,
                    ),
                    contains_eager(
                        AssetsModel.components,
                        ComponentsModel.data_sources,
                        DataSourcesModel.type,
                    )
                )\
                .join(AssetsModel.components)\
                .join(ComponentsModel.data_sources)\
                .join(DataSourcesModel.metrics)\
                .join(ComponentsModel.type)\
                .join(DataSourcesModel.type)\
                .join(DataSourcesModel.gateways)\
                .filter(GatewaysModel.cognito_client_id == cognito_client_id)\
                .all()

    def _update_gateway_heartbeat(self, cognito_client_id: str, timestamp: datetime = datetime.utcnow()):
        with self._connector.get_session(self._customer, self._site) as session:
            Gateways(session).update(
                Gateways.model.cognito_client_id == cognito_client_id,
                values={'heartbeat_timestamp': timestamp}
            )

    def _cache_service_call(self,
                            key: str,
                            service: Any,
                            method_name: str,
                            result_handler: Optional[Callable[[Any], Any]] = None,
                            *args,
                            **kwargs) -> Any:
        """
        Caches service call result.
        """

        result = self._cache.get(key)
        if result is not None:
            return result
        with self._connector.get_session(self._customer, self._site) as session:
            result = getattr(service(session), method_name)(*args, **kwargs)
        if result_handler:
            result = result_handler(result)
        self._cache[key] = result
        return result

    @abstractmethod
    def handle(self) -> Response:
        pass
