from datetime import datetime, timedelta
import json

from .dhl_brescia_handler import DHLBresciaHandler


__all__ = ['DHLBrnoHandler']


class DHLBrnoHandler(DHLBresciaHandler):
    pass


TEST_ENV = {
    'app_client_id': 'cakaln6p9pmdmnod3rk6c9u1a',
    'app_client_secret': '1uibl1jddtr0h9fr16kg05dkd0q7q6cdg02n0jk0tpld82h8miq3',
    'host': 'https://dev-gate-pool.auth.us-east-1.amazoncognito.com/oauth2/token',
}

TEST_EVENT_TIMESTAMP = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')
TEST_EVENT_TIMEDELTA_TIMESTAMP = (datetime.utcnow() - timedelta(hours=2)).strftime('%Y-%m-%d %H:%M:%S.%f')
TEST_EVENT = {
    'httpMethod': 'POST',
    'path': 'data',
    'body': json.dumps({
        'site': 'dhl-brno',
        'data': [
            {
                'asset_name': 'sorter 4',
                'component_type': 'motor',
                'data_source_type': 'vfd',
                'metrics': [
                    {
                        'speed': '1.1',
                        'current': '2.1',
                        'voltage': '3.1',
                        'mechanical_power': '4.1',
                        'cos_phi': '0.1',
                        'timestamp': TEST_EVENT_TIMESTAMP
                    },
                    {
                        'speed': '1.1',
                        'current': '2.1',
                        'voltage': '0',
                        'mechanical_power': '4.1',
                        'cos_phi': '0.1',
                        'timestamp': TEST_EVENT_TIMESTAMP
                    },
                    {
                        'speed': '1.1',
                        'current': '2.1',
                        'voltage': '3.1',
                        'mechanical_power': '4.1',
                        'cos_phi': '0.1',
                        'timestamp': TEST_EVENT_TIMEDELTA_TIMESTAMP
                    }
                ],
                'temperatures': [
                    {
                        'heat_sink_temperature': '5.1',
                        'timestamp': TEST_EVENT_TIMESTAMP
                    }
                ],
                'lifetimes': [
                    {
                        'operating_time': '6.1',
                        'running_time': '7.1',
                        'timestamp': TEST_EVENT_TIMESTAMP
                    }
                ],
                'statuses': [
                    {
                        'status_word': '8',
                        'timestamp': TEST_EVENT_TIMESTAMP
                    },
                    {
                        'warning': '1',
                        'timestamp': TEST_EVENT_TIMESTAMP
                    },
                    {
                        'fault': '10',
                        'timestamp': TEST_EVENT_TIMESTAMP
                    }
                ]
            },
            {
                'asset_name': 'sorter 4',
                'component_type': 'motor',
                'data_source_type': 'vfd',
                'metrics': [
                    {
                        'speed': '1.1',
                        'current': '2.1',
                        'voltage': '3.1',
                        'mechanical_power': '4.1',
                        'cos_phi': '0.1',
                        'timestamp': TEST_EVENT_TIMESTAMP
                    }
                ]
            }
        ]
    })
}
