import os
import json
import logging

from typing import Dict, Type

from mhs_rdb import Connector

from mhs_web_utils.request import Request
from mhs_web_utils.response import Response
from mhs_web_utils.handler import Handler
from mhs_web_utils.resolver import Resolver
from mhs_web_utils.route import Route
from mhs_web_utils.authorizer.cognito import CognitoAuthorizer
from mhs_web_utils.validation import SiteValidator, AnyField
from mhs_web_utils.handler.mixins import DBMixin, MethodMixin

import handlers


logger = logging.getLogger()

if logger.handlers:
    logger.setLevel(logging.INFO)
else:
    logging.basicConfig(level=logging.INFO)


POST_HANDLERS: Dict[str, Type[handlers.BaseHandler]] = {
    'dhl_brescia': handlers.DHLBresciaHandler,
    'dhl_brno': handlers.DHLBrnoHandler
}


class BodyValidator(SiteValidator):
    """
    Body validator.
    """

    data = AnyField()


class DataIngestorHandler(DBMixin, MethodMixin, Handler):
    """
    Data ingestor handler.

    All requests from all sites will be handled by this handler.
    The handlers.DefaultHandler will be used if site name is not found in the POST_HANDLERS.
    """

    allowed_methods = ('POST',)
    authorizer_cls = CognitoAuthorizer

    def post_handler(self, request: Request) -> Response:
        """
        POST request handler.
        """

        body = BodyValidator(json.loads(request.body)).validate()
        customer = body['customer']
        site = body['site']
        logger.info(f'===Received request from {site} site==\nRequest body: {body}')

        data = {'data': body['data']}

        handler = POST_HANDLERS.get(site, handlers.DefaultHandler)(
            connector=self._connector,
            token=self.authorizer.token,
            headers=request.headers,
            customer=customer,
            site=site,
            data=data
        )

        return handler.handle()


CONNECTOR = Connector()
RESOLVER = Resolver(
    Route(lambda path: path.endswith('data'), DataIngestorHandler(CONNECTOR)),
    debug=os.getenv('API_DEBUG') == 'True'
)


def lambda_handler(event, context):
    return RESOLVER.process(event, context)
