from functools import reduce
from typing import Any, Hashable, Dict, List, Tuple, Optional

from mhs_rdb.models import AssetsModel


__all__ = ['Context', 'StructureNode', 'StructureMapping']


class Context(dict):
    """
    Utility class to store current processing context
    """

    def __setattr__(self, key: str, value: Any):
        self[key] = value

    def __getattr__(self, item) -> Any:
        return self[item]


class StructureNode:
    """
    Represents one node in cloud structure.
    `self._obj` contains an object itself such as asset, component, data_source, metric
    `self._children` contains dict with node key and child `StructureNode` node as a value
    """

    def __init__(self, obj: Any):
        self._obj: Any = obj
        self._children: Dict[Hashable, 'StructureNode'] = {}

    def add_child(self, key: Hashable, node: 'StructureNode'):
        self._children[key] = node

    def get_child(self, key: Hashable) -> Optional['StructureNode']:
        return self._children.get(key)

    @property
    def obj(self) -> Any:
        return self._obj


class StructureMapping:

    def __init__(self, assets: List[AssetsModel], cloud_structure_key: Tuple[str, str, str, str]):
        self._root_node = StructureNode('root')

        asset_key, component_key, data_source_key, metric_key = cloud_structure_key
        for asset in assets:
            asset_node = StructureNode(asset)
            self._root_node.add_child(self._get_key(asset, asset_key), asset_node)

            for component in asset.components:
                component_node = StructureNode(component)
                asset_node.add_child(self._get_key(component, component_key), component_node)

                for data_source in component.data_sources:
                    data_source_node = StructureNode(data_source)
                    component_node.add_child(self._get_key(data_source, data_source_key), data_source_node)

                    for metric in data_source.metrics:
                        data_source_node.add_child(self._get_key(metric, metric_key), StructureNode(metric))

    def _get_key(self, obj: Any, key: str) -> Hashable:
        return reduce(lambda o, k: getattr(o, k), key.split('.'), obj)

    def get_element(self, *key: Hashable) -> Optional[Any]:
        node = self._root_node
        for key_part in key:
            _node = node.get_child(key_part)
            if not _node:
                return None
            node = _node

        return node.obj
