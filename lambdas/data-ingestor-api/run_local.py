"""
Local testing
"""

import argparse
import configparser
import importlib
import logging
import os
import json
import base64
import requests

from pprint import pprint

from boto3.session import Session as BotoSession


ENV_NAME = 'develop_new'
DEFAULT_HOST = 'mhspredict-dev-new.c2z9g3wxqyud.us-east-1.rds.amazonaws.com'
DEFAULT_USERNAME = 'developer'


def get_auth_token(client_id: str, client_secret: str, auth_pool_url: str, scope: str = 'test-resource-service/scope') -> str:
    """
    Makes HTTP request to Cognito auth pool and returns auth token for further usage.
    """

    encode_data = f'{client_id}:{client_secret}'.encode('utf-8')
    encode_authorization = base64.b64encode(encode_data)
    authorization = encode_authorization.decode('utf-8')

    headers = {
        'Authorization': f'Basic {authorization}'
    }

    data = {
        'grant_type': 'client_credentials',
        'scope': scope
    }

    response = requests.post(url=auth_pool_url, headers=headers, data=data)
    assert response.status_code, 200
    return json.loads(response.content)['access_token']


def main():
    """
    local runner
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('handler_module_name', help='filename from "handlers" dir')
    parser.add_argument('-p', '--profile', help='AWS profile name')
    parser.add_argument('--user_name', help=f'RDB username (default is "{DEFAULT_USERNAME}")',
                        default=DEFAULT_USERNAME)
    parser.add_argument('--host', help=f'RDB host (default is "{DEFAULT_HOST}")',
                        default=DEFAULT_HOST)
    parser.add_argument('-d', '--debug', help='show debug info',
                        action='store_true', default=False)
    parser.add_argument('-sp', '--simple_print', help='simple print output',
                        action='store_true', default=False)
    parser.add_argument('-b', '--body_print', help='prints only body',
                        action='store_true', default=False)

    args = parser.parse_args()

    log_level = logging.DEBUG if args.debug else logging.INFO
    logging.basicConfig(level=log_level)

    handler_module = importlib.import_module(f'handlers.{args.handler_module_name}')
    env = getattr(handler_module, 'TEST_ENV')
    event = getattr(handler_module, 'TEST_EVENT')

    auth_token = get_auth_token(env['app_client_id'], env['app_client_secret'], env['host'])
    if not event.get('headers'):
        event['headers'] = {}
    event['headers']['Authorization'] = f'Bearer {auth_token}'

    boto_session = BotoSession(profile_name=args.profile)

    import lambda_function
    lambda_function.CONNECTOR._boto_session = boto_session
    lambda_function.CONNECTOR._debug = args.debug
    lambda_function.CONNECTOR._host = args.host
    lambda_function.CONNECTOR._username = args.user_name

    result = lambda_function.lambda_handler(event, {})
    if args.body_print:
        result = result['body']
    if args.simple_print:
        print(result)
    else:
        pprint(result)


if __name__ == '__main__':

    class RawConfigParser(configparser.ConfigParser):  # pylint disable=R0901
        """inherit config parser"""
        # https://stackoverflow.com/questions/19359556/configparser-reads-capital-keys-and-make-them-lower-case"""
        def optionxform(self, optionstr):
            """override optionxform to be case sensitive"""
            return optionstr

    config = RawConfigParser()
    config.read('.config.ini')
    env_vars = dict(config.items('env'))
    env_vars.update(config.items(f'env:{ENV_NAME}'))
    for key, value in env_vars.items():
        os.environ[key] = value

    main()
