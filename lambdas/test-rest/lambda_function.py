def lambda_handler(event, _context):
    """
    Test REST lambda. Prints event and returns 200 response.
    """

    print(event)

    return {
        'statusCode': 200,
        'headers': {'Content-Type': 'text/plain'},
        'body': 'test REST response'
    }
