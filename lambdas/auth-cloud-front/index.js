const querystring = require('querystring');
const https = require('https');
const jwt = require('jsonwebtoken');
const jwkToPem = require('jwk-to-pem');

const LOGIN_ENDPOINT = '/login/index.html';

const ENVIRONMENTS = {
  production: {
    jwk_path: './production_jwks.json',
    auth_domain: 'mhsinsights.com',
    cognito_pool_id: 'us-east-1_eEombidgG',
    cognito_iss: 'https://cognito-idp.us-east-1.amazonaws.com/',
  },
  preprod: {
    jwk_path: './production_jwks.json',
    auth_domain: 'preprod.mhsinsights.com',
    cognito_pool_id: 'us-east-1_eEombidgG',
    cognito_iss: 'https://cognito-idp.us-east-1.amazonaws.com/',
  },
  develop: {
    jwk_path: './develop_jwks.json',
    auth_domain: 'dev.mhsinsights.com',
    cognito_pool_id: 'us-east-1_TEkwyJ3Ix',
    cognito_iss: 'https://cognito-idp.us-east-1.amazonaws.com/',
  },
  rnd: {
    jwk_path: './develop_jwks.json',
    auth_domain: 'rnd.mhsinsights.com',
    cognito_pool_id: 'us-east-1_TEkwyJ3Ix',
    cognito_iss: 'https://cognito-idp.us-east-1.amazonaws.com/',
  },
};

const ENVIRONMENT_HOSTS = {
  'rnd.mhsinsights.com': 'rnd',
  'dev.mhsinsights.com': 'develop',
  'preprod.mhsinsights.com': 'preprod',
};

const DEFAULT_ENVIRONMENT = 'production';

const getEnv = (request) => {
  if (!('host' in request.headers)) {
    return DEFAULT_ENVIRONMENT;
  }

  const host = request.headers.host[0].value.toLowerCase();
  if (host in ENVIRONMENT_HOSTS) {
    return ENVIRONMENT_HOSTS[host];
  }

  return DEFAULT_ENVIRONMENT;
};

const requireEnvJwk = env => require(ENVIRONMENTS[env].jwk_path);

const getEnvAuthDomain = env => ENVIRONMENTS[env].auth_domain;

const getEnvCognitoPoolId = env => ENVIRONMENTS[env].cognito_pool_id;

const getEnvCognitoISS = env => ENVIRONMENTS[env].cognito_iss + getEnvCognitoPoolId(env);

const getEnvSites = env => ENVIRONMENTS[env].sites;

const getEnvCustomers = env => ENVIRONMENTS[env].customers;

const getCustomerByGroup = group => group.split('-', 1)[0];

const getRedirectURL = (request, env) => 'https://' + getEnvAuthDomain(env) + LOGIN_ENDPOINT;

const getCookies = (request) => {
  const cookies = {};
  if (request.headers && request.headers.cookie && request.headers.cookie[0].key.toLowerCase() === 'cookie') {
    request.headers.cookie[0].value.split(';').forEach((cookie) => {
      const parts = cookie.split('=');
      const key = parts.shift().trim();
      const value = decodeURI(parts.join('='));
      if (key !== '') {
        cookies[key] = value;
      }
    });
  }
  return cookies;
};

const getRedirectResponse = (url, code) => {
  if (code) {
    url += '?code=' + encodeURIComponent(code);
  }
  const headers = {
    location: [{ key: 'Location', value: url }],
    'cache-control': [{ key: 'Cache-Control', value: 'no-cache, no-store, must-revalidate' }],
    expires: [{ key: 'Expires', value: '0' }],
  };
  return {
    status: '302',
    statusDescription: 'Found',
    headers: headers,
  };
};

const tokenHandler = (request, env, jwk, redirectURL, cookies, callback) => {
  const token = cookies.token;
  const decodedToken = jwt.decode(token, { complete: true });

  if (!decodedToken) {
    callback(null, getRedirectResponse(redirectURL, 'invalid-token'));
    return;
  }

  if (decodedToken.payload.iss !== getEnvCognitoISS(env)) {
    callback(null, getRedirectResponse(redirectURL, 'invalid-token-iss'));
    return;
  }

  if (decodedToken.payload.token_use.toLowerCase() !== 'id') {
    callback(null, getRedirectResponse(redirectURL, 'invalid-token-id'));
    return;
  }

  const keys = jwk.keys;
  const pems = {};
  for (let i = 0; i < keys.length; i += 1) {
    pems[keys[i].kid] = jwkToPem(keys[i]);
  }

  const kid = decodedToken.header.kid;
  const pem = pems[kid];

  if (!pem) {
    callback(null, getRedirectResponse(redirectURL, 'invalid-token-pem'));
    return;
  }

  jwt.verify(token, pem, { issuer: getEnvCognitoISS(env) }, (error) => {
    if (error) {
      callback(null, getRedirectResponse(redirectURL, 'token-verification-failed'));
      return;
    }

    callback(null, request);
  });
};

exports.handler = (event, context, callback) => {
  const request = event.Records[0].cf.request;
  const env = getEnv(request);
  const jwk = requireEnvJwk(env);
  const redirectURL = getRedirectURL(request, env);

  // check if token is in cookies.
  const cookies = getCookies(request);
  if ('token' in cookies) {
    tokenHandler(request, env, jwk, redirectURL, cookies, callback);
    return;
  }

  callback(null, getRedirectResponse(redirectURL, 'token-not-found'));
};
