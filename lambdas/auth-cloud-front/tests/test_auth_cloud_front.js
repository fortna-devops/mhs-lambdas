const expect = require('chai').expect;
const assert = require('chai').assert;
const mockDate = require('mockdate');
const authCloudFront = require('../index');

const testEvent = {
  Records: [
    {
      cf: {
        request: {
          method: 'GET',
          uri: '/',
          headers: {
            host: [
              {
                key: 'Host',
                value: 'mhsinsights.com',
              }
            ],
          },
        },
      },
    },
  ],
};

const testToken = 'eyJraWQiOiJvbVRSMzRcL3hjdHQ0Y3NXQmNVZytQVlV5Z25sdlZGWG9ZYTg2RkowWlpjWT0iLCJhbGciOiJSUzI1NiJ9.eyJhdF9oYXNoIjoiU3RQUEV1M29zWUFicEZ5YkVleEZDZyIsInN1YiI6IjBmNzJkMWU3LTEwYzktNGUyNi05NzZkLTAwNWNmNDcxMmUyMiIsImF1ZCI6IjVkdXV2NDQxOGJqYTVlbjlkZDM5a2RrNm1zIiwiY29nbml0bzpncm91cHMiOlsiRmVkRXgiLCJESEwiLCJBRE1JTiJdLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwidG9rZW5fdXNlIjoiaWQiLCJhdXRoX3RpbWUiOjE1NDAzMTMwNTYsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC51cy1lYXN0LTEuYW1hem9uYXdzLmNvbVwvdXMtZWFzdC0xX2VFb21iaWRnRyIsImNvZ25pdG86dXNlcm5hbWUiOiJua2hhYmFyb3YiLCJleHAiOjE1NDAzMTY2NTYsImlhdCI6MTU0MDMxMzA1NiwiZW1haWwiOiJOaWtvbGF5LktoYWJhcm92QGRhdGFhcnQuY29tIn0.B4eFCRkO_8g9I8gopFYui6CvqxrJtV6lQGI0cq87Kp5soA3b6xoG-dBBFFnZGAWkgZ5UrAjBEJLr6k8omv1b33Y75DySwM11WSeaPOQHzvAUmOVD0zXtbxsTKNWgejGupFCdOyrhagdjIMF680FZk2OFa1E_x2YslBvescqfVMIW51VMf2xI4vSzKtO2IZECLtUlTNgN0YBS45K7TAZfWi17wRz41DjbcHwILR5fV5LVmGfzAiP9ac1vQU2ZEK7ncsNUDCyRnFEWzToNNq2EUAtQ5CUXKTJ6hXKAgWV8r2thlzjt3zZqLnDBVxZIaA2SnUHQ1PJY9c75Fb8lTX9tEw';

const cloneTestEvent = () => JSON.parse(JSON.stringify(testEvent));

const cloneTestEventWithToken = () => {
  let testEvent = cloneTestEvent();
  testEvent.Records[0].cf.request.headers.cookie = [{key: 'cookie', value: 'token=' + testToken}];
  return testEvent;
};

describe('test auth cloud front', () => {
  before(() => {
    // any date, just to initialise hook
    mockDate.set('01/01/1970 00:00 GMT+0000');
  });

  after(() => {
    mockDate.reset();
  });

  it('test redirect to login with invalid token', () => {
    let testEvent = cloneTestEventWithToken();
    testEvent.Records[0].cf.request.headers.cookie[0].value = 'invalid-token';
    authCloudFront.handler(testEvent, null, (context, result) => {
      expect(result.status).equal('302');
    });
  });

  it('test redirect to login page without token', () => {
    let testEvent = cloneTestEvent();
    authCloudFront.handler(testEvent, null, (context, result) => {
      expect(result.status).equal('302');
    });
  });

  it('test success token validation', () => {
    let testEvent = cloneTestEventWithToken();
    authCloudFront.handler(testEvent, null, (context, result) => {
      expect(testEvent.Records[0].cf.request).equal(result);
    });
  });

});
