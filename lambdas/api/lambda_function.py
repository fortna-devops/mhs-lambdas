"""
Module that acts as the entry point
for the majority of front end API endpoints
"""
import logging
import os

from mhs_rdb import Connector
from mhs_web_utils.resolver import Resolver
from mhs_web_utils.route import Route


import handlers


logger = logging.getLogger()

if logger.handlers:
    logger.setLevel(logging.INFO)
else:
    logging.basicConfig(level=logging.INFO)


CONNECTOR = Connector()
API_DEBUG = os.getenv('API_DEBUG') == 'True'


RESOLVER = Resolver(
    Route(lambda path: path.endswith('customers'), handlers.CustomersHandler(CONNECTOR)),
    Route(lambda path: path.endswith('customer-events'), handlers.CustomerEventsHandler(CONNECTOR)),
    Route(lambda path: path.endswith('left-menu'), handlers.LeftMenuHandler(CONNECTOR)),
    Route(lambda path: path.endswith('component-details'), handlers.ComponentDetailsHandler(CONNECTOR)),
    Route(lambda path: path.endswith('asset-details'), handlers.AssetDetailsHandler(CONNECTOR)),
    Route(lambda path: path.endswith('kpi-details'), handlers.KpiDetailsHandler(CONNECTOR)),
    Route(lambda path: path.endswith('report-filters'), handlers.ReportFiltersHandler(CONNECTOR)),
    Route(lambda path: path.endswith('metrics'), handlers.MetricsHandler(CONNECTOR)),
    Route(lambda path: path.endswith('data'), handlers.DataHandler(CONNECTOR)),
    Route(lambda path: path.endswith('alarms'), handlers.AlarmsHandler(CONNECTOR)),
    Route(lambda path: path.endswith('alarm-counts'), handlers.AlarmCountsHandler(CONNECTOR)),
    Route(lambda path: path.endswith('alert-unsubscribe'), handlers.AlertUnsubscribeHandler(CONNECTOR)),
    Route(lambda path: path.endswith('user-pref'), handlers.UserPreferencesHandler()),
    Route(lambda path: path.endswith('send-email'), handlers.SendEmailHandler()),
    Route(lambda path: path.endswith('external-media'), handlers.ExternalMediaHandler()),
    debug=API_DEBUG
)


def lambda_handler(event, context):
    """
    Entry point for AWS API Gateway
    calls a different function depending
    on the path param in the event dictionary
    """
    return RESOLVER.process(event, context)
