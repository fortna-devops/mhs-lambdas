"""
Local testing
"""

import argparse
import configparser
import importlib
import logging
import os

from pprint import pprint

from boto3.session import Session as BotoSession


ENV_NAME = 'develop_new'
DEFAULT_HOST = 'mhspredict-dev-new.c2z9g3wxqyud.us-east-1.rds.amazonaws.com'
DEFAULT_USERNAME = 'developer'
AUTH_TOKEN = 'eyJraWQiOiJ1UTJob2FxOHphRGlpdmg5QXBHNkpnVEtna2hHcXg4d0NXcVZkZ2tDMlNzPSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiI0ZGU0ZjAzYi1mNGVkLTQxNmMtOWRlOS0yMzZkYWIyNWY2MGEiLCJhdWQiOiI3cWdwYWQzNzFiZ2pyY2FvMGgyNzJrcDAzZSIsImNvZ25pdG86Z3JvdXBzIjpbIm1oc2Vtb3Rpb24tZmFjaWxpdHktNCIsIm1oc3ZhbnJpZXQtZmFjaWxpdHktMSIsImFtYXpvbnRtcC1zZGY0IiwibWhzYXRyb25peC1mYWNpbGl0eS0xIiwiYWRtaW4iLCJBRE1JTiIsIm1ocy1kYXRhYXJ0IiwibWhzLXRtcCIsIm1ocy1jb21tZXJjZS1jcm9zc2luZyIsIm1oc2Vtb3Rpb24tZmFjaWxpdHktMiIsIm1oc3RtcC1kYXRhYXJ0IiwibWhzb2NtLWZhY2lsaXR5LTEiLCJtaHMtZW11bGF0ZWQiLCJtaHMtdGVzdGxvb3AtYXRsYW50YSIsIm1oc2Vtb3Rpb24tZmFjaWxpdHktMSIsImRobC1icmVzY2lhIiwibWhzZW1vdGlvbi1mYWNpbGl0eS0zIl0sImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJldmVudF9pZCI6Ijc1OTc5OGU0LTc0NjktNDU5ZS05YjJlLTFhOGM1N2EwM2UxNSIsInRva2VuX3VzZSI6ImlkIiwiYXV0aF90aW1lIjoxNTk2NTM0MTQ1LCJpc3MiOiJodHRwczpcL1wvY29nbml0by1pZHAudXMtZWFzdC0xLmFtYXpvbmF3cy5jb21cL3VzLWVhc3QtMV9URWt3eUozSXgiLCJjb2duaXRvOnVzZXJuYW1lIjoiaXBhbnRlbGV5ZXYiLCJleHAiOjE1OTY1Mzc3NDUsImlhdCI6MTU5NjUzNDE0NSwiZW1haWwiOiJpZ29yLnBhbnRlbGV5ZXZAZGF0YWFydC5jb20ifQ.atwA2jfwhl46-NACP-JBymn9tJ3yIGUCP4MkyJ648GgtHQ8XVfjKenWu9PfsWT_5-8KTojUS_84UBVNhCEEeF8mlHuEspSQ4QpC6Dqls0T0XLGqYjqfX7zkL0yaoxlgzBbbHh6-OiRaE0DDuGb6y_-eQd2ri1afF-fm2bWg2_61lfyR7_kpW7ZzmoCvAA_ZQYvHxngSIZllxd5E8KjaC0JKQa9DllvOSpRMpxzmnemQgYCBObqAjXzkUIpzU07tkqLkKXOf5PDQg-kRz9D4JJsuZlqURAMlCWwlV8xxKlrd-XseY7CyP0w1xNbJ4oyt0NJf-24EEJ5ZsBSNFMYUpfw'


def main():
    """
    local runner
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('handler_module_name', help='filename from "handlers" dir')
    parser.add_argument('-p', '--profile', help='AWS profile name')
    parser.add_argument('--user_name', help=f'RDB username (default is "{DEFAULT_USERNAME}")',
                        default=DEFAULT_USERNAME)
    parser.add_argument('--host', help=f'RDB host (default is "{DEFAULT_HOST}")',
                        default=DEFAULT_HOST)
    parser.add_argument('-d', '--debug', help='show debug info',
                        action='store_true', default=False)
    parser.add_argument('-sp', '--simple_print', help='simple print output',
                        action='store_true', default=False)
    parser.add_argument('-b', '--body_print', help='prints only body',
                        action='store_true', default=False)

    args = parser.parse_args()

    log_level = logging.DEBUG if args.debug else logging.INFO
    logging.basicConfig(level=log_level)

    handler_module = importlib.import_module(f'handlers.{args.handler_module_name}')
    event = getattr(handler_module, 'TEST_EVENT')
    if not event.get('headers'):
        event['headers'] = {}
    event['headers']['Authorization'] = f'Bearer {AUTH_TOKEN}'

    boto_session = BotoSession(profile_name=args.profile)

    import lambda_function
    lambda_function.CONNECTOR._boto_session = boto_session
    lambda_function.CONNECTOR._debug = args.debug
    lambda_function.CONNECTOR._host = args.host
    lambda_function.CONNECTOR._username = args.user_name

    result = lambda_function.lambda_handler(event, {})
    if args.body_print:
        result = result['body']
    if args.simple_print:
        print(result)
    else:
        pprint(result)


if __name__ == '__main__':

    class RawConfigParser(configparser.ConfigParser):  # pylint disable=R0901
        """inherit config parser"""
        # https://stackoverflow.com/questions/19359556/configparser-reads-capital-keys-and-make-them-lower-case"""
        def optionxform(self, optionstr):
            """override optionxform to be case sensitive"""
            return optionstr

    config = RawConfigParser()
    config.read('.config.ini')
    env_vars = dict(config.items('env'))
    env_vars.update(config.items(f'env:{ENV_NAME}'))
    for key, value in env_vars.items():
        os.environ[key] = value

    main()
