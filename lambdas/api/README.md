# Facility Overview Endpoint
----------------------------
#### Background

This lambda exists to send data about a particular facility. It is used as an endpoint to respond to UI requests.

----------------------------
#### Methodology

The lambda receives parameters within the `event` object with the following format:

```
{
  'client':'mhspredict-site-dhl-miami',
  'start_date':'2018-11-30',
  'end_date':'2018-12-04'
}
```

Queries the `location` table to get status of conveyors and aggregates data by area specificationm, i.e. "Pick Mods", "Inbound", etc.

Queries the `alarms` table to get a list of alarms that have existed over the dates in between the specified start and end dates. Will only send the most recent alarms, as there will likely be repeats from day to day.

----------------------------
#### Return Format

```
{
	'area_percentages':
		{
		    'Gapper':{'critical': 0.0, 'moderate': 39.29, 'normal': 60.71}, 
		    'Recirc':{'critical': 0.0, 'moderate': 0.0, 'normal': 100.0},
		    'Sorter': {'critical': 0.0, 'moderate': 0.0, 'normal': 100.0}
		},
	'total_percentage': {'critical': 0.0, 'moderate': 22.92, 'normal': 77.08},
	'alarm_log': ['SR2-10 Bearing (sensor #42) has high frequency vibrations z over upper limit (6.0), Actual = 65.53'], 
	'conveyors_with_errors': ['SR2-10']
}

```


----------------------------
#### Configurable Parameters:

None.

----------------------------

#### Lambda Triggers:

The lambda is triggerred to run whenever the API endpoint is hit on the UI.
