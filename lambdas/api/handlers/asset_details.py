import logging
from collections import defaultdict
from datetime import datetime, timedelta
from typing import Dict, Any, List, Tuple

from mhs_rdb import Assets, AssetStatusTransitions
from mhs_rdb.models import AssetStatusTransitionsModel, AssetStatusesEnum, ComponentsModel, MetricsModel
from mhs_rdb.serializers import ModelDictSerializer

from mhs_web_utils.request import Request
from mhs_web_utils.response import Response, JSONResponse
from mhs_web_utils.handler import Handler
from mhs_web_utils.handler.mixins import DBMixin, MethodMixin
from mhs_web_utils.validation import SiteValidator, ListField, IntField, TimedeltaField


from .mixins import MetricsGrouper


__all__ = ['AssetDetailsHandler']


logger = logging.getLogger(__file__.split('.')[0].upper())


class ArgsValidator(SiteValidator):
    asset_ids = ListField(IntField())
    lookup_days = TimedeltaField(units='days', default=timedelta(days=7))


class StatsCalculator:

    def __init__(self):
        self._total: timedelta = timedelta()
        self._values: Dict[AssetStatusesEnum, timedelta] = defaultdict(timedelta)

    def add(self, key: AssetStatusesEnum, value: timedelta):
        self._values[key] += value
        self._total += value

    @property
    def total(self):
        return self._total

    def to_dict(self) -> Dict[str, float]:
        on_hours = round(self._values[AssetStatusesEnum.ON].total_seconds() / 3600, 2)
        off_hours = round(self._values[AssetStatusesEnum.OFF].total_seconds() / 3600, 2)

        return {
            'on_hours': on_hours,
            'off_hours': off_hours,
        }


class AssetStatusTransitionsSerializer(ModelDictSerializer):

    def __init__(self, start_datetime: datetime, end_datetime: datetime):
        super().__init__()

        self._start_datetime = start_datetime
        self._end_datetime = end_datetime

    def serialize(self, obj: List[AssetStatusTransitionsModel]) -> Tuple[List[Dict[str, Any]], Dict[str, float]]:

        serialized_transitions = []
        stats_calculator = StatsCalculator()

        obj.reverse()
        last_i = len(obj) - 1
        for i, transition in enumerate(obj):

            if i == 0:
                serialized_transition = {
                    'status': transition.status.invert(),
                    'start_date_time': self._start_datetime,
                    'end_date_time': transition.timestamp
                }
                serialized_transitions.append(serialized_transition)
                stats_calculator.add(transition.status.invert(), transition.timestamp - self._start_datetime)

            if i < last_i:
                serialized_transition = {
                    'status': transition.status,
                    'start_date_time': transition.timestamp,
                    'end_date_time': obj[i+1].timestamp
                }
                serialized_transitions.append(serialized_transition)
                stats_calculator.add(transition.status, obj[i+1].timestamp - transition.timestamp)
                continue

            serialized_transition = {
                'status': transition.status,
                'start_date_time': transition.timestamp,
                'end_date_time': self._end_datetime
            }
            serialized_transitions.append(serialized_transition)
            stats_calculator.add(transition.status, self._end_datetime - transition.timestamp)

        return serialized_transitions, stats_calculator.to_dict()


class AssetsSerializer(MetricsGrouper, ModelDictSerializer):
    included_attrs = ('id', 'name', 'components')
    key_map = {
        'components': 'metrics'
    }

    def __init__(self,
                 start_datetime: datetime,
                 end_datetime: datetime,
                 asset_status_transitions: Dict[int, List[AssetStatusTransitionsModel]]):
        super().__init__()

        self._asset_status_transitions = asset_status_transitions
        self._status_transition_serializer = AssetStatusTransitionsSerializer(start_datetime, end_datetime)

    def _serialize_model(self, obj: Any) -> Dict[str, Any]:
        res = super()._serialize_model(obj=obj)
        status_transitions = self._asset_status_transitions[obj.id]
        res['transitions'], res['runtime_stats'] = self._status_transition_serializer.serialize(status_transitions)
        return res

    def handle_components(self, components: List[ComponentsModel]) -> List[Dict[str, Any]]:
        metrics: List[MetricsModel] = []
        for component in components:
            for data_source in component.data_sources:
                metrics.extend(data_source.metrics)

        return self.group_metrics(metrics=metrics)


class AssetDetailsHandler(DBMixin, MethodMixin, Handler):
    allowed_methods = ('GET',)
    args_validator_cls = ArgsValidator

    def get_handler(self, request: Request) -> Response:
        kwargs = self.args_validator_cls(request.query).validate()
        end_datetime = datetime.utcnow()
        start_datetime = end_datetime - kwargs['lookup_days']
        with self.connector.get_session(kwargs.pop('customer'), kwargs.pop('site')) as session:
            assets = Assets(session).get_details(
                asset_ids=kwargs['asset_ids'],
            )
            asset_status_transitions = AssetStatusTransitions(session).group_by_asset_id(
                asset_ids=kwargs['asset_ids'],
                start_datetime=start_datetime,
                end_datetime=end_datetime
            )

        asset_details = AssetsSerializer(start_datetime, end_datetime, asset_status_transitions).serialize(assets)

        return JSONResponse({'asset_details': asset_details})


TEST_EVENT = {
    'httpMethod': 'GET',
    'path': 'asset-details',
    'queryStringParameters': {
        'site': 'mhs_emulated',
        'asset_ids': '1,2',
    }
}
