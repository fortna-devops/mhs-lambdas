import itertools
import logging
from decimal import Decimal
from datetime import datetime, timedelta
from typing import Dict, Any, List, Sequence, Tuple, Iterable

from mhs_rdb.utils.data import DataProxy
from mhs_rdb.serializers import BaseSerializer

from mhs_web_utils.request import Request
from mhs_web_utils.response import Response, JSONResponse
from mhs_web_utils.handler import Handler
from mhs_web_utils.handler.mixins import DBMixin, MethodMixin
from mhs_web_utils.validation import SiteValidator, ListField, StrField, IntField, DateTimeField


__all__ = ['DataHandler']


logger = logging.getLogger(__file__.split('.')[0].upper())


class ArgsValidator(SiteValidator):
    metric_ids = ListField(IntField())
    start_datetime = DateTimeField()
    end_datetime = DateTimeField()
    sample_rate = StrField(choices=DataProxy.sample_rate_options, default=DataProxy.hour_sample_rate)
    group_by = StrField(choices=('kind', 'component_id'), default='kind')


class DataSerializer(BaseSerializer):
    """
    Serialize "data" rows to a suitable format for graphs
    """

    min_decimal_places: int = 2
    max_decimal_places: int = 5

    def __init__(self, group_by: str):
        self._group_by = group_by

    def _get_group_key(self, item: Dict[str, Any]):
        return item[self._group_by], item['metric_id']

    def _get_decimal_places(self, a: Decimal, b: Decimal) -> int:
        """
        Compare two decimal values and return number of decimal places to display based on their difference
        """
        diff = (a.copy_abs() - b.copy_abs()).copy_abs()
        places = diff.adjusted() * -1
        _, digits, _ = diff.as_tuple()

        if digits and digits[0] > 5:
            places -= 1

        return min(self.max_decimal_places, max(self.min_decimal_places, places))

    def _process_metric_data(self, metric_data: Iterable[Dict[str, Any]], series: int
                             ) -> Tuple[List[Dict[str, Any]], Dict[str, Any]]:
        result = []
        min_value = Decimal('NaN')
        max_value = Decimal('NaN')

        for point in metric_data:
            result.append({'x': point['timestamp'], 'y': point['value'], 'series': series})
            value = Decimal.from_float(point['value'])
            min_value = min_value.min(value)
            max_value = max_value.max(value)

        meta = {
            'min_value': min_value,
            'max_value': max_value,
            'decimal_places': self._get_decimal_places(min_value, max_value)
        }

        return result, meta

    def serialize(self, obj: Sequence[Dict[str, Any]]) -> List[Dict[str, Any]]:
        """
        Serialize data for graphs
        """
        result: List[Dict[str, Any]] = []
        seen_groups: List[str] = []
        for (group_key, metric_id), group in itertools.groupby(obj, self._get_group_key):
            if group_key not in seen_groups:
                seen_groups.append(group_key)

            data, meta = self._process_metric_data(group, seen_groups.index(group_key))
            result.append({
                'metric_id': metric_id,
                'group_key': group_key,
                'meta': meta,
                'data': data
            })

        return result


class DataHandler(DBMixin, MethodMixin, Handler):
    """
    Get data from data tables and serializes rows to a suitable format for graphs like this:
    {
      "data": [
        {
          "metric_id": 1842,
          "group_key": "raw",
          "meta": {
            "min_value": 0.0675441533900224,
            "max_value": 0.0699914894998074,
            "decimal_places": 3
          },
          "data": [
            {
              "x": "2021-02-16 16:00:00.000",
              "y": 0.0675441533900224,
              "series": 0
            },
            {
              "x": "2021-02-16 15:00:00.000",
              "y": 0.0698638807982206,
              "series": 0
            },
            {
              "x": "2021-02-16 14:00:00.000",
              "y": 0.0699914894998074,
              "series": 0
            }
          ]
        },
        {
          "metric_id": 1843,
          "group_key": "raw",
          "meta": {
            "min_value": 72.4382329305013,
            "max_value": 72.6077670869373,
            "decimal_places": 2
          },
          "data": [
            {
              "x": "2021-02-16 16:00:00.000",
              "y": 72.5525735708383,
              "series": 0
            },
            {
              "x": "2021-02-16 15:00:00.000",
              "y": 72.4382329305013,
              "series": 0
            },
            {
              "x": "2021-02-16 14:00:00.000",
              "y": 72.6077670869373,
              "series": 0
            }
          ]
        },
        {
          "metric_id": 1844,
          "group_key": "raw",
          "meta": {
            "min_value": 0.393982547521591,
            "max_value": 0.395728553716953,
            "decimal_places": 3
          },
          "data": [
            {
              "x": "2021-02-16 16:00:00.000",
              "y": 0.395728553716953,
              "series": 0
            },
            {
              "x": "2021-02-16 15:00:00.000",
              "y": 0.394215466827154,
              "series": 0
            },
            {
              "x": "2021-02-16 14:00:00.000",
              "y": 0.393982547521591,
              "series": 0
            }
          ]
        },
        {
          "metric_id": 1842,
          "group_key": "calculated",
          "meta": {
            "min_value": 0.0703557249158621,
            "max_value": 0.0703557249158621,
            "decimal_places": 2
          },
          "data": [
            {
              "x": "2021-02-16 14:00:00.000",
              "y": 0.0703557249158621,
              "series": 1
            }
          ]
        },
        {
          "metric_id": 1843,
          "group_key": "calculated",
          "meta": {
            "min_value": 72.5011990865072,
            "max_value": 72.5011990865072,
            "decimal_places": 2
          },
          "data": [
            {
              "x": "2021-02-16 14:00:00.000",
              "y": 72.5011990865072,
              "series": 1
            }
          ]
        },
        {
          "metric_id": 1844,
          "group_key": "calculated",
          "meta": {
            "min_value": 0.393681170543035,
            "max_value": 0.393681170543035,
            "decimal_places": 2
          },
          "data": [
            {
              "x": "2021-02-16 14:00:00.000",
              "y": 0.393681170543035,
              "series": 1
            }
          ]
        }
      ]
    }
    """

    allowed_methods = ('GET',)
    args_validator_cls = ArgsValidator

    def get_handler(self, request: Request) -> Response:
        kwargs = self.args_validator_cls(request.query).validate()
        group_by = kwargs.pop('group_by')
        kwargs['include_kind'] = group_by == 'kind'
        if group_by == 'component_id':
            kwargs['group_level'] = DataProxy.component_group_level

        serializer = DataSerializer(group_by)

        with self.connector.get_session(kwargs.pop('customer'), kwargs.pop('site')) as session:
            data = DataProxy(session).get_grouped_with_kind(**kwargs)
        return JSONResponse({'data': serializer.serialize(data)})


TIMESTAMP = datetime.utcnow()
PERIOD = timedelta(hours=3)
TEST_EVENT = {
    'httpMethod': 'GET',
    'path': 'data',
    'queryStringParameters': {
        'site': 'mhs_emulated',
        'metric_ids': '1842,1843,1844',
        # 'group_by': 'component_id',
        # 'sample_rate': 'hour',
        'start_datetime': (TIMESTAMP - PERIOD).strftime('%Y-%m-%dT%H:%M:%S.%fZ'),
        'end_datetime': TIMESTAMP.strftime('%Y-%m-%dT%H:%M:%S.%fZ'),
    }
}
