import os
import json
import base64
import logging
from pathlib import PurePosixPath, PurePath
from typing import Optional

from boto3.session import Session as BotoSession
import s3fs

from mhs_web_utils.request import Request
from mhs_web_utils.response import Response, JSONResponse, NotFoundResponse
from mhs_web_utils.handler import Handler
from mhs_web_utils.handler.mixins import MethodMixin
from mhs_web_utils.validation import SiteValidator, Validator, StrField, PathField, Base64EncodedField


__all__ = ['ExternalMediaHandler']


logger = logging.getLogger(__file__.split('.')[0].upper())


class PostArgsValidator(SiteValidator):
    include_customer = False

    path = PathField(PurePosixPath)


class GetArgsValidator(PostArgsValidator):
    file_name = StrField(required=False)


class DeleteArgsValidator(PostArgsValidator):
    file_name = StrField()


class BodyValidator(Validator):
    file_name = StrField()
    file_content = Base64EncodedField()


class ExternalMediaHandler(MethodMixin, Handler):
    """
    Interface to store, retrieve and delete files on external storage (S3)
    """

    allowed_methods = ('GET', 'POST', 'DELETE')

    boto_session = BotoSession(profile_name=os.getenv('profile'))
    fs = s3fs.S3FileSystem(session=boto_session)
    bucket_prefix = 'mhspredict-site-'
    storage_prefix = 'external_media'

    def get_s3_key(self, site: str, path: PurePath, file_name: Optional[str] = None) -> str:
        """
        Build S3 key based on arguments
        """
        site = site.replace('_', '-')
        path = PurePosixPath(f'{self.bucket_prefix}{site}') / self.storage_prefix / path
        if file_name:
            path = path / file_name
        return str(path)

    def get_file(self, s3_key: str) -> Response:
        """
        Get file content from S3
        """
        try:
            with self.fs.open(s3_key, 'rb') as f:
                file_content = f.read()
        except FileNotFoundError:
            return NotFoundResponse()

        return JSONResponse({
            'file_content': str(base64.b64encode(file_content), 'utf-8')
        })

    def list_files(self, s3_key: str) -> Response:
        """
        List available files in specified key on S3
        """
        files = []
        try:
            file_details = self.fs.ls(s3_key, detail=True)
            for file_detail in file_details:
                if file_detail['StorageClass'] != 'STANDARD':
                    continue
                files.append({'file_name': file_detail['Key'].split('/')[-1]})
        except FileNotFoundError:
            pass

        return JSONResponse(list(reversed(files)))

    def get_handler(self, request: Request) -> Response:
        """
        List files or get a specific file from S3
        """
        kwargs = GetArgsValidator(request.query).validate()
        file_name = kwargs['file_name']

        s3_key = self.get_s3_key(**kwargs)
        if file_name:
            return self.get_file(s3_key)

        return self.list_files(s3_key)

    def post_handler(self, request: Request) -> Response:
        """
        Put file to S3
        """
        kwargs = PostArgsValidator(request.query).validate()
        body = BodyValidator(json.loads(request.body)).validate()

        s3_key = self.get_s3_key(file_name=body['file_name'], **kwargs)
        with self.fs.open(s3_key, 'wb') as f:
            f.write(body['file_content'])

        return JSONResponse({'status': True})

    def delete_handler(self, request: Request) -> Response:
        """
        Delete file from S3
        """
        kwargs = DeleteArgsValidator(request.query).validate()
        s3_key = self.get_s3_key(**kwargs)

        try:
            self.fs.rm(s3_key)
        except FileNotFoundError:
            return NotFoundResponse()

        return JSONResponse({'status': True})


TEST_EVENT = {
    'httpMethod': 'DELETE',
    'path': 'external-media',
    'queryStringParameters': {
        'site': 'mhs-emulated',
        'path': 'SS1-4/test-equipment',
        'file_name': 'test_file.txt',
    },
    'body': json.dumps({
        'file_name': 'test_file.txt',
        'file_content': base64.b64encode(b'test file content').decode()
    })
}
