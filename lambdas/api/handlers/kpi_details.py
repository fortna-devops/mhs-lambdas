import logging
from datetime import datetime
from typing import Dict, Any, List

from mhs_rdb import Kpis
from mhs_rdb.utils.kpi_color_calculators import KpiColors, DefaultKpiColorCalculator
from mhs_rdb.models import KpisModel
from mhs_rdb.serializers import ModelDictSerializer

from mhs_web_utils.request import Request
from mhs_web_utils.response import Response, JSONResponse
from mhs_web_utils.handler import Handler
from mhs_web_utils.handler.mixins import DBMixin, MethodMixin
from mhs_web_utils.validation import SiteValidator, ListField, IntField


from .mixins import AlarmsConfigMixin


__all__ = ['KpiDetailsHandler']


logger = logging.getLogger(__file__.split('.')[0].upper())


class ArgsValidator(SiteValidator):
    asset_ids = ListField(IntField())


class KpiSerializer(ModelDictSerializer):

    included_attrs = ('id', 'name')

    def __init__(self, kpi_colors: KpiColors):
        super().__init__()
        self._kpi_colors = kpi_colors

    def _serialize_model(self, obj: Any) -> Dict[str, Any]:

        serialized_kpi = super()._serialize_model(obj=obj)
        serialized_kpi['color'] = self._kpi_colors.get_color(serialized_kpi['id'])

        return serialized_kpi


class KpiGroupsSerializer(ModelDictSerializer):

    def __init__(self, kpi_colors: KpiColors):
        super().__init__()
        self._kpi_colors = kpi_colors

    def serialize(self, obj: List[KpisModel]) -> List[Dict[str, Any]]:

        serialized_kpi_assets: Dict[int, Dict[str, Any]] = {}
        for kpi in obj:

            asset_id = kpi.asset_id
            if asset_id not in serialized_kpi_assets:
                serialized_kpi_assets[asset_id] = {
                    'asset_id': asset_id,
                    'groups': {}
                }

            group_name = kpi.kpi_group.display_name
            if group_name not in serialized_kpi_assets[asset_id]['groups']:
                serialized_kpi_assets[asset_id]['groups'][group_name] = {
                    'name': group_name,
                    'kpis': []
                }

            serialized_kpi = KpiSerializer(self._kpi_colors).serialize(kpi)
            serialized_kpi_assets[asset_id]['groups'][group_name]['kpis'].append(serialized_kpi)

        for serialized_kpi_asset in serialized_kpi_assets.values():
            serialized_kpi_asset['groups'] = list(serialized_kpi_asset['groups'].values())

        return list(serialized_kpi_assets.values())


class KpiDetailsHandler(DBMixin, MethodMixin, AlarmsConfigMixin, Handler):

    allowed_methods = ('GET',)
    args_validator_cls = ArgsValidator

    def get_handler(self, request: Request) -> Response:
        kwargs = self.args_validator_cls(request.query).validate()

        customer, site = kwargs['customer'], kwargs['site']
        asset_ids = kwargs['asset_ids']
        alarm_end = datetime.utcnow()
        alarm_start = alarm_end - self._alarm_data_timedelta

        with self.connector.get_session(customer, site) as session:
            kpi_colors = DefaultKpiColorCalculator(session).calculate(
                alarm_start=alarm_start,
                alarm_end=alarm_end,
                asset_ids=asset_ids
            )

        with self.connector.get_session(customer, site) as session:
            kpis = Kpis(session).get_with_groups(asset_ids=asset_ids)

        return JSONResponse({'kpis': KpiGroupsSerializer(kpi_colors).serialize(kpis)})


TEST_EVENT = {
    'httpMethod': 'GET',
    'path': 'kpi-details',
    'queryStringParameters': {
        'site': 'mhs_emulated',
        'asset_ids': '4,5',
    }
}
