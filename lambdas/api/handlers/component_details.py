import logging
from collections import defaultdict
from datetime import datetime
from typing import Dict, Any, List

from mhs_rdb import Components
from mhs_rdb.models import DataSourcesModel, ThresholdsModel, ComponentsModel, ComponentTypesModel, MetricsModel
from mhs_rdb.serializers import ModelDictSerializer
from mhs_rdb.utils.health_calculators import DefaultHealthCalculator

from mhs_web_utils.request import Request
from mhs_web_utils.response import Response, JSONResponse
from mhs_web_utils.handler import Handler
from mhs_web_utils.handler.mixins import DBMixin, MethodMixin
from mhs_web_utils.validation import SiteValidator, ListField, IntField


from .mixins import AlarmsConfigMixin


__all__ = ['ComponentDetailsHandler']


logger = logging.getLogger(__file__.split('.')[0].upper())


class ArgsValidator(SiteValidator):
    component_ids = ListField(IntField())


class ThresholdsSerializer(ModelDictSerializer):
    included_attrs = ('id', 'level_name', 'value')

    def _serialize_model(self, obj: ThresholdsModel) -> Dict[str, Any]:
        res = super()._serialize_model(obj=obj)
        res['color'] = obj.level.color
        return res


class MetricsSerializer(ModelDictSerializer):
    included_attrs = ('id', 'name', 'units', 'thresholds')

    thresholds_serializer = ThresholdsSerializer()

    def __init__(self, data_source: DataSourcesModel, grouped_metrics: Dict[str, List[int]]):
        super().__init__()
        self._data_source = data_source
        self._grouped_metrics = grouped_metrics

    def _serialize_model(self, obj: MetricsModel) -> Dict[str, Any]:
        result = super()._serialize_model(obj)
        result['display_name'] = self._serialize_display_name(obj)
        return result

    def _serialize_display_name(self, metric: MetricsModel) -> str:
        if len(self._grouped_metrics[metric.display_name]) > 1 and self._data_source.location:
            return f'{self._data_source.location}: {metric.display_name}'
        return metric.display_name

    def handle_thresholds(self, thresholds: List[ThresholdsModel]) -> List[Dict[str, Any]]:
        return self.thresholds_serializer.serialize(thresholds)


class DataSourceSerializer(ModelDictSerializer):
    included_attrs = ('id',)

    def __init__(self):
        super().__init__()
        self._grouped_metrics: Dict[str, List[int]] = defaultdict(list)

    def _serialize_list(self, objs: List[DataSourcesModel]) -> List[Dict[str, Any]]:
        result = []

        for data_source in objs:
            for metric in data_source.metrics:
                self._grouped_metrics[metric.display_name].append(metric.id)

        for data_source in objs:
            result.append(self._serialize(data_source))

        return result

    def _serialize_model(self, obj: DataSourcesModel) -> Dict[str, Any]:
        result = super()._serialize_model(obj)
        result['metrics'] = MetricsSerializer(obj, self._grouped_metrics).serialize(obj.metrics)
        return result


class ComponentTypeSerializer(ModelDictSerializer):
    included_attrs = ('id', 'name')


class ComponentsSerializer(ModelDictSerializer):
    def __init__(self, components_health_map: Dict[int, float]):
        super().__init__()

        self._components_health_map = components_health_map

    component_type_serializer = ComponentTypeSerializer()
    included_attrs = ('id', 'name', 'type', 'data_sources')

    def _serialize_model(self, obj: ComponentsModel) -> Dict[str, Any]:
        res = super()._serialize_model(obj=obj)
        res['health'] = round(self._components_health_map.get(obj.id, 100.))
        return res

    def handle_type(self, component_type: ComponentTypesModel) -> Dict[str, Any]:
        return self.component_type_serializer.serialize(component_type)

    def handle_data_sources(self, data_sources: List[DataSourcesModel]) -> List[Dict[str, Any]]:
        return DataSourceSerializer().serialize(data_sources)


class ComponentDetailsHandler(DBMixin, MethodMixin, AlarmsConfigMixin, Handler):
    allowed_methods = ('GET',)
    args_validator_cls = ArgsValidator

    def get_handler(self, request: Request) -> Response:
        """
        Get detailed information about component: metrics, thresholds, health
        {
          "component_details": [
            {
              "data_sources": [
                {
                  "metrics": [
                    {
                      "thresholds": [],
                      "name": "temperature",
                      "display_name": "temperature",
                      "units": "°F",
                      "id": 20
                    },
                    {
                      "thresholds": [],
                      "name": "rms_velocity_x",
                      "display_name": "rms_velocity_x",
                      "units": "in/sec",
                      "id": 21
                    },
                    {
                      "thresholds": [],
                      "name": "rms_velocity_z",
                      "display_name": "rms_velocity_z",
                      "units": "in/sec",
                      "id": 19
                    }
                  ],
                  "id": 4
                }
              ],
              "type": {
                "id": 1,
                "name": "bearing",
              },
              "name": "Bearing",
              "id": 2,
              "health": 100
            },
            {
              "data_sources": [
                {
                  "metrics": [
                    {
                      "thresholds": [],
                      "name": "rms_velocity_z",
                      "display_name": "rms_velocity_z",
                      "units": "in/sec",
                      "id": 2
                    },
                    {
                      "thresholds": [],
                      "name": "rms_velocity_x",
                      "display_name": "rms_velocity_x",
                      "units": "in/sec",
                      "id": 4
                    },
                    {
                      "thresholds": [],
                      "name": "temperature",
                      "display_name": "temperature",
                      "units": "°F",
                      "id": 3
                    }
                  ],
                  "id": 3
                }
              ],
              "type": {
                "id": 2,
                "name": "gearbox",
              },
              "name": "Gearbox",
              "id": 1,
              "health": 92
            },
            {
              "data_sources": [
                {
                  "metrics": [
                    {
                      "thresholds": [],
                      "name": "temperature",
                      "display_name": "temperature",
                      "units": "°F",
                      "id": 37
                    },
                    {
                      "thresholds": [],
                      "name": "rms_velocity_x",
                      "display_name": "rms_velocity_x",
                      "units": "in/sec",
                      "id": 38
                    },
                    {
                      "thresholds": [],
                      "name": "rms_velocity_z",
                      "display_name": "rms_velocity_z",
                      "units": "in/sec",
                      "id": 36
                    }
                  ],
                  "id": 5
                }
              ],
              "type": {
                "id": 3,
                "name": "motor",
              },
              "name": "Motor",
              "id": 3,
              "health": 100
            },
            {
              "data_sources": [
                {
                  "metrics": [
                    {
                      "thresholds": [],
                      "name": "rms_velocity_x",
                      "display_name": "rms_velocity_x",
                      "units": "in/sec",
                      "id": 55
                    },
                    {
                      "thresholds": [],
                      "name": "temperature",
                      "display_name": "temperature",
                      "units": "°F",
                      "id": 54
                    },
                    {
                      "thresholds": [],
                      "name": "rms_velocity_z",
                      "display_name": "rms_velocity_z",
                      "units": "in/sec",
                      "id": 53
                    }
                  ],
                  "id": 6
                }
              ],
              "type": {
                "id": 3,
                "name": "motor",
              },
              "name": "Motor",
              "id": 4,
              "health": 100
            }
          ]
        }
        """
        kwargs = self.args_validator_cls(request.query).validate()
        end_datetime = datetime.utcnow()
        start_datetime = end_datetime - self._alarm_data_timedelta
        with self.connector.get_session(kwargs.pop('customer'), kwargs.pop('site')) as session:
            components = Components(session).get_details(**kwargs)
            components_health_map = DefaultHealthCalculator(session).calculate(
                alarm_start=start_datetime,
                alarm_end=end_datetime,
                component_ids=kwargs['component_ids']
            )
        component_details = ComponentsSerializer(components_health_map).serialize(components)
        return JSONResponse({'component_details': component_details})


TEST_EVENT = {
    'httpMethod': 'GET',
    'path': 'component-details',
    'queryStringParameters': {
        'site': 'mhs_emulated',
        'component_ids': '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15',
    }
}
