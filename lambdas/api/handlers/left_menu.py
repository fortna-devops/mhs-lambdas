"""
Left menu handler.
"""

from abc import ABC, abstractmethod
from typing import Tuple, List, Dict, Any, Type
from datetime import datetime

from mhs_web_utils.authorizer import CognitoCustomerAuthorizer
from mhs_web_utils.request import Request
from mhs_web_utils.response import Response, JSONResponse
from mhs_web_utils.handler import Handler
from mhs_web_utils.handler.mixins import DBMixin, MethodMixin
from mhs_web_utils.validation import CustomerValidator

from mhs_rdb import CustomerSites, FacilityAreas
from mhs_rdb.models import CustomerSitesModel, FacilityAreasModel, AssetsModel, ComponentsModel
from mhs_rdb.models import ColorsEnum
from mhs_rdb.utils.color_calculators import DefaultColorCalculator as ColorCalculator
from mhs_rdb.utils.color_calculators import Colors

from .mixins import AlarmsConfigMixin


__all__ = ['LeftMenuHandler']


def serialize_component(component: ComponentsModel, component_colors: Colors) -> Dict[str, Any]:
    """
    Returns serialized component.
    """

    serialized_component = {
        'id': component.id,
        'name': component.name,
        'color': component_colors.get_color(component.id),
        'type': {
            'id': component.type.id,
            'name': component.type.name
        }
    }

    return serialized_component


def serialize_asset(asset: AssetsModel, component_colors: Colors) -> Dict[str, Any]:
    """
    Returns serialized asset.
    """

    serialized_asset = {
        'id': asset.id,
        'name': asset.name,
        'color': ColorsEnum.GREEN,
        'type': {
            'id': asset.type.id,
            'name': asset.type.name
        },
        'components': []
    }

    for component in asset.components:

        serialized_component = serialize_component(component, component_colors)
        serialized_asset['components'].append(serialized_component)

        if serialized_component['color'].value > serialized_asset['color'].value:
            serialized_asset['color'] = serialized_component['color']

    return serialized_asset


def serialize_facility_area(facility_area: FacilityAreasModel, component_colors: Colors) -> Dict[str, Any]:
    """
    Returns serialized facility area.
    """

    serialized_facility_area = {
        'id': facility_area.id,
        'name': facility_area.name,
        'color': ColorsEnum.GREEN,
        'assets': []
    }

    for asset in facility_area.assets:

        serialized_asset = serialize_asset(asset, component_colors)
        serialized_facility_area['assets'].append(serialized_asset)

        if serialized_asset['color'].value > serialized_facility_area['color'].value:
            serialized_facility_area['color'] = serialized_asset['color']

    return serialized_facility_area


def serialize_facility(customer_site: CustomerSitesModel,
                       facility_areas: List[FacilityAreasModel],
                       component_colors: Colors) -> Dict[str, Any]:
    """
    Returns serialized facility.
    """

    serialized_facility = {
        'site': customer_site.name,
        'name': customer_site.facility_name,
        'color': ColorsEnum.GREEN,
        'facility_areas': []
    }

    for facility_area in facility_areas:

        serialized_facility_area = serialize_facility_area(facility_area, component_colors)
        serialized_facility['facility_areas'].append(serialized_facility_area)

        if serialized_facility_area['color'].value > serialized_facility['color'].value:
            serialized_facility['color'] = serialized_facility_area['color']

    return serialized_facility


def serialize_site(sites: List[Tuple[CustomerSitesModel,
                                     List[FacilityAreasModel],
                                     Colors]]) -> Dict[str, Any]:
    """
    If there is the region column we assume that we have menu with 'countries' key.
    Otherwise we have menu with 'facilities' key.
    """

    serialized_facilities = [serialize_facility(*site) for site in sites]

    if not serialized_facilities or sites[0][0].region is None:
        return {'facilities': serialized_facilities}

    serialized_countries: Dict[str, Any] = {}
    for num_customer_site, (customer_site, *_) in enumerate(sites):

        serialized_country = serialized_countries.get(customer_site.nation)
        if not serialized_country:

            serialized_country = {
                'name': customer_site.nation,
                'color': ColorsEnum.GREEN,
                'regions': {}
            }
            serialized_countries[customer_site.nation] = serialized_country

        serialized_region = serialized_country['regions'].get(customer_site.region)
        if not serialized_region:

            serialized_region = {
                'name': customer_site.region,
                'color': ColorsEnum.GREEN,
                'facilities': []
            }
            serialized_country['regions'][customer_site.region] = serialized_region

        serialized_facility = serialized_facilities[num_customer_site]
        serialized_country['regions'][customer_site.region]['facilities'].append(serialized_facility)

        if serialized_facility['color'].value > serialized_region['color'].value:
            serialized_region['color'] = serialized_facility['color']

        if serialized_facility['color'].value > serialized_country['color'].value:
            serialized_country['color'] = serialized_facility['color']

    for serialized_country in serialized_countries.values():
        serialized_country['regions'] = list(serialized_country['regions'].values())

    return {'countries': list(serialized_countries.values())}


class BaseOverviewSetter(ABC):
    """
    Base overview setter.
    """

    @staticmethod
    def _divide(n: float, m: float) -> float:
        try:
            return round(n / m, 2)
        except ZeroDivisionError:
            return 0

    @abstractmethod
    def set_overview(self, serialized_sites: Dict[str, Any]):
        return


class FacilityAreaOverviewSetter(BaseOverviewSetter):
    """
    Facility area overview setter.
    """

    _colors = (
        ColorsEnum.GREEN,
        ColorsEnum.YELLOW,
        ColorsEnum.RED
    )

    @staticmethod
    def _get_serialized_facility_areas(serialized_sites: Dict[str, Any]) -> List[Dict[str, Any]]:
        """
        Returns serialized facility areas.
        """

        serialized_facility_areas: List[Dict[str, Any]] = []
        if 'facilities' in serialized_sites:
            for serialized_facility in serialized_sites['facilities']:
                serialized_facility_areas.extend(serialized_facility['facility_areas'])
            return serialized_facility_areas

        for serialized_county in serialized_sites['countries']:
            for serialized_region in serialized_county['regions']:
                for serialized_facility in serialized_region['facilities']:
                    serialized_facility_areas.extend(serialized_facility['facility_areas'])
        return serialized_facility_areas

    def set_overview(self, serialized_sites: Dict[str, Any]):
        """
        Sets overview to facility area.
        """

        for serialized_facility_area in self._get_serialized_facility_areas(serialized_sites):

            serialized_facility_area['overview'] = {}

            serialized_facility_area['overview']['num_components'] = 0
            for color in self._colors:
                serialized_facility_area['overview'][f'num_{color.name.lower()}_components'] = 0

            for serialized_asset in serialized_facility_area['assets']:
                for serialized_component in serialized_asset['components']:
                    color_name = serialized_component['color'].name.lower()
                    serialized_facility_area['overview'][f'num_{color_name}_components'] += 1

            for color in self._colors:
                num_components = serialized_facility_area['overview'][f'num_{color.name.lower()}_components']
                serialized_facility_area['overview'][f'num_components'] += num_components


class FacilityOverviewSetter(BaseOverviewSetter):
    """
    Facility overview setter.
    """

    @staticmethod
    def _get_serialized_facilities(serialized_sites: Dict[str, Any]) -> List[Dict[str, Any]]:
        """
        Returns serialized facilities.
        """

        if 'facilities' in serialized_sites:
            return serialized_sites['facilities']

        serialized_facilities: List[Dict[str, Any]] = []
        for serialized_county in serialized_sites['countries']:
            for serialized_region in serialized_county['regions']:
                serialized_facilities.extend(serialized_region['facilities'])
        return serialized_facilities

    def _get_serialized_overview_facility_area(self, serialized_facility_area: Dict[str, Any]) -> Dict[str, Any]:
        """
        Returns serialized overview facility area.
        """

        yellow_components = self._divide(
            serialized_facility_area['overview']['num_yellow_components'] * 100.0,
            serialized_facility_area['overview']['num_components']
        )
        red_components = self._divide(
            serialized_facility_area['overview']['num_red_components'] * 100.0,
            serialized_facility_area['overview']['num_components']
        )
        green_components = 100 - yellow_components - red_components

        return {
            'name': serialized_facility_area['name'],
            'percentages': {
                'green_components': green_components,
                'yellow_components': yellow_components,
                'red_components': red_components
            }
        }

    def set_overview(self, serialized_sites: Dict[str, Any]):
        """
        Sets overview to facilities.
        """

        for serialized_facility in self._get_serialized_facilities(serialized_sites):

            serialized_facility['overview'] = {
                'facility_area_percentages': [],
                'yellow_assets': [],
                'red_assets': [],
                'percentages': {}
            }

            num_yellow_components = 0
            num_red_components = 0
            num_components = 0

            for serialized_facility_area in serialized_facility['facility_areas']:

                num_yellow_components += serialized_facility_area['overview']['num_yellow_components']
                num_red_components += serialized_facility_area['overview']['num_red_components']
                num_components += serialized_facility_area['overview']['num_components']

                serialized_overview_facility_area = self._get_serialized_overview_facility_area(
                    serialized_facility_area
                )

                serialized_facility['overview']['facility_area_percentages'].append(serialized_overview_facility_area)

                for serialized_asset in serialized_facility_area['assets']:
                    if serialized_asset['color'] not in (ColorsEnum.YELLOW, ColorsEnum.RED):
                        continue

                    serialized_overview_asset = {
                        'facility_name': serialized_facility['name'],
                        'id': serialized_asset['id'],
                        'name': serialized_asset['name'],
                        'color': serialized_asset['color']
                    }

                    if serialized_asset['color'] == ColorsEnum.YELLOW:
                        serialized_facility['overview']['yellow_assets'].append(serialized_overview_asset)

                    if serialized_asset['color'] == ColorsEnum.RED:
                        serialized_facility['overview']['red_assets'].append(serialized_overview_asset)

            yellow_components = self._divide(num_yellow_components * 100.0, num_components)
            red_components = self._divide(num_red_components * 100.0, num_components)
            green_components = 100 - yellow_components - red_components

            serialized_facility['overview']['percentages'] = {
                'green_components': green_components,
                'yellow_components': yellow_components,
                'red_components': red_components
            }


class RegionOverviewSetter(BaseOverviewSetter):
    """
    Region overview setter.
    """

    @staticmethod
    def _get_serialized_regions(serialized_sites: Dict[str, Any]) -> List[Dict[str, Any]]:
        """
        Returns serialized regions.
        """

        if 'facilities' in serialized_sites:
            return []

        serialized_regions: List[Dict[str, Any]] = []
        for serialized_county in serialized_sites['countries']:
            serialized_regions.extend(serialized_county['regions'])
        return serialized_regions

    def _get_serialized_overview_facility(self, serialized_facility: Dict[str, Any]) -> Dict[str, Any]:
        """
        Returns serialized overview facility.
        """

        num_green_assets = 0
        num_yellow_assets = 0
        num_red_assets = 0

        for serialized_facility_area in serialized_facility['facility_areas']:

            for serialized_asset in serialized_facility_area['assets']:

                if serialized_asset['color'] == ColorsEnum.GREEN:
                    num_green_assets += 1

                if serialized_asset['color'] == ColorsEnum.YELLOW:
                    num_yellow_assets += 1

                if serialized_asset['color'] == ColorsEnum.RED:
                    num_red_assets += 1

        num_assets = num_green_assets + num_yellow_assets + num_red_assets

        yellow_assets = self._divide(num_yellow_assets * 100.0, num_assets)
        red_assets = self._divide(num_red_assets * 100.0, num_assets)
        green_assets = 100 - yellow_assets - red_assets

        return {
            'name': serialized_facility['name'],
            'color': serialized_facility['color'],
            'num_green_assets': num_green_assets,
            'num_yellow_assets': num_yellow_assets,
            'num_red_assets': num_red_assets,
            'percentages': {
                'green_assets': green_assets,
                'yellow_assets': yellow_assets,
                'red_assets': red_assets
            }
        }

    def set_overview(self, serialized_sites: Dict[str, Any]):
        """
        Sets overview to regions.
        """

        for serialized_region in self._get_serialized_regions(serialized_sites):
            serialized_region['overview'] = {
                'facilities': [],
                'yellow_assets': [],
                'red_assets': [],
                'percentages': {}
            }

            num_green_assets = 0
            num_yellow_assets = 0
            num_red_assets = 0

            for serialized_facility in serialized_region['facilities']:
                serialized_overview_facility = self._get_serialized_overview_facility(serialized_facility)

                num_green_assets += serialized_overview_facility['num_green_assets']
                num_yellow_assets += serialized_overview_facility['num_yellow_assets']
                num_red_assets += serialized_overview_facility['num_red_assets']

                serialized_region['overview']['facilities'].append(serialized_overview_facility)
                serialized_region['overview']['yellow_assets'].extend(serialized_facility['overview']['yellow_assets'])
                serialized_region['overview']['red_assets'].extend(serialized_facility['overview']['red_assets'])

            num_assets = num_green_assets + num_yellow_assets + num_red_assets

            yellow_assets = self._divide(num_yellow_assets * 100.0, num_assets)
            red_assets = self._divide(num_red_assets * 100.0, num_assets)
            green_assets = 100 - yellow_assets - red_assets

            serialized_region['overview']['percentages'] = {
                'green_assets': green_assets,
                'yellow_assets': yellow_assets,
                'red_assets': red_assets
            }


class CountryOverviewSetter(BaseOverviewSetter):
    """
    Country overview setter.
    """

    @staticmethod
    def _get_serialized_countries(serialized_sites: Dict[str, Any]) -> List[Dict[str, Any]]:
        """
        Returns serialized countries.
        """

        if 'facilities' in serialized_sites:
            return []

        return serialized_sites['countries']

    def set_overview(self, serialized_sites: Dict[str, Any]):
        """
        Sets overview to country.
        """

        for serialized_county in self._get_serialized_countries(serialized_sites):

            serialized_county['overview'] = {
                'facilities': [],
                'yellow_assets': [],
                'red_assets': []
            }

            for serialized_region in serialized_county['regions']:
                serialized_county['overview']['facilities'].extend(serialized_region['overview']['facilities'])
                serialized_county['overview']['yellow_assets'].extend(serialized_region['overview']['yellow_assets'])
                serialized_county['overview']['red_assets'].extend(serialized_region['overview']['red_assets'])


class LeftMenuHandler(DBMixin, MethodMixin, AlarmsConfigMixin, Handler):
    """
    Handles left menu.
    """

    authorizer_cls = CognitoCustomerAuthorizer

    allowed_methods = ('GET',)

    _customer_schema_name = 'customer'

    _overview_setters: Tuple[Type[BaseOverviewSetter], ...] = (
        FacilityAreaOverviewSetter,
        FacilityOverviewSetter,
        RegionOverviewSetter,
        CountryOverviewSetter
    )

    def _get_customer_sites(self, customer: str, schema_names: List[str]) -> List[CustomerSitesModel]:
        """
        Returns customer sites with assets.
        """

        customer_sites = []
        with self.connector.get_session(customer, self._customer_schema_name) as session:
            for customer_site in CustomerSites(session).get_all():
                if customer_site.schema_name not in schema_names:
                    continue

                customer_sites.append(customer_site)

        return customer_sites

    def _get_facility_areas(self, customer: str, schema_name: str) -> List[FacilityAreasModel]:
        """
        Returns facility areas.
        """

        with self.connector.get_session(customer, schema_name) as session:
            return FacilityAreas(session).get_with_assets_and_components()

    def _get_component_colors(self,
                              customer: str,
                              schema_name: str,
                              alarm_data_start_datetime: datetime,
                              alarm_data_end_datetime: datetime) -> Colors:
        """
        Returns component colors.
        """

        with self.connector.get_session(customer, schema_name) as session:
            return ColorCalculator(session).calculate(alarm_data_start_datetime, alarm_data_end_datetime)

    def get_handler(self, request: Request) -> Response:
        """
        Handles request.

        Response example:
        {
            "countries": [
                {
                    "name": "United States",
                    "color": "YELLOW",
                    "regions": [
                        {
                            "name": "Southern",
                            "color": "YELLOW",
                            "facilities": [
                                {
                                    "site": "mhsatronix-facility-1",
                                    "name": "Facility 1",
                                    "color": "YELLOW",
                                    "facility_areas": [
                                        {
                                            "id": 1,
                                            "name": "Small AFE",
                                            "color": "YELLOW",
                                            "assets": [
                                                {
                                                    "id": 1,
                                                    "name": "6602",
                                                    "color": "YELLOW",
                                                    "type": {
                                                        "id": 1,
                                                        "name": "Sorter",
                                                    },
                                                    "components": [
                                                        {
                                                            "id": 1,
                                                            "name": "Bearing",
                                                            "color": "GREEN",
                                                            "type": {
                                                                "id": 1,
                                                                "name": "BEARING",
                                                            }
                                                        }
                                                    ]
                                                }
                                            ],
                                            "overview": {
                                                "num_components": 1,
                                                "num_green_components": 0,
                                                "num_yellow_components": 1,
                                                "num_red_components": 0
                                            }
                                        }
                                    ],
                                    "overview": {
                                        "facility_area_percentages": [
                                            {
                                                "name": "Small AFE",
                                                "percentages": {
                                                    "green_components": 0,
                                                    "yellow_components": 100,
                                                    "red_components": 0
                                                }
                                            }
                                        ],
                                        "yellow_assets": [
                                            {
                                                "facility_name": "Facility 1",
                                                "id": 1,
                                                "name": "6602",
                                                "color": "YELLOW"
                                            }
                                        ],
                                        "red_assets": [],
                                        "percentages": {
                                            "green_components": 0,
                                            "yellow_components": 100,
                                            "red_components": 0
                                        }
                                    }
                                }
                            ],
                            "overview": {
                                "facilities": [
                                    {
                                        "name": "Facility 1",
                                        "color": "YELLOW",
                                        "num_green_assets": 0,
                                        "num_yellow_assets": 1,
                                        "num_red_assets": 0,
                                        "percentages": {
                                            "green_assets": 0,
                                            "yellow_assets": 100,
                                            "red_assets": 0
                                        }
                                    }
                                ],
                                "yellow_assets": [
                                    {
                                        "facility_name": "Facility 1",
                                        "id": 1,
                                        "name": "6602",
                                        "color": "YELLOW"
                                    }
                                ],
                                "red_assets": [],
                                "percentages": {
                                    "green_assets": 0,
                                    "yellow_assets": 100,
                                    "red_assets": 0
                                }
                            }
                        }
                    ],
                    "overview": {
                        "facilities": [
                            {
                                "name": "Facility 1",
                                "color": "YELLOW",
                                "num_green_assets": 0,
                                "num_yellow_assets": 1,
                                "num_red_assets": 0,
                                "percentages": {
                                    "green_assets": 0,
                                    "yellow_assets": 100,
                                    "red_assets": 0
                                }
                            }
                        ],
                        "yellow_assets": [
                            {
                                "facility_name": "Facility 1",
                                "id": 1,
                                "name": "6602",
                                "color": "YELLOW"
                            }
                        ],
                        "red_assets": []
                    }
                }
            ]
        }
        """

        args = CustomerValidator(request.query).validate()

        customer = args['customer']
        customer_groups = self.authorizer.token.get_customer_groups(customer)
        schema_names = [group.replace('-', '_') for group in customer_groups]

        customer_sites = self._get_customer_sites(customer, schema_names)
        alarm_end_data_datetime = datetime.utcnow()
        alarm_start_data_datetime = alarm_end_data_datetime - self._alarm_data_timedelta
        sites = []
        for customer_site in customer_sites:
            site = (
                customer_site,
                self._get_facility_areas(customer, customer_site.schema_name),
                self._get_component_colors(
                    customer,
                    customer_site.schema_name,
                    alarm_start_data_datetime,
                    alarm_end_data_datetime
                )
            )
            sites.append(site)

        serialized_sites = serialize_site(sites)

        for overview_setter in self._overview_setters:
            overview_setter().set_overview(serialized_sites)

        return JSONResponse(serialized_sites)


TEST_EVENT = {
    'httpMethod': 'GET',
    'path': 'left-menu',
    'queryStringParameters': {
        'customer': 'mhs'
    }
}
