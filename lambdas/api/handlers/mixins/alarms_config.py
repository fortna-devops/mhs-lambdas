from datetime import timedelta


__all__ = ['AlarmsConfigMixin']


class AlarmsConfigMixin:

    _alarm_data_timedelta = timedelta(hours=24)

    _count_alarm_data_timedelta = timedelta(days=30)
