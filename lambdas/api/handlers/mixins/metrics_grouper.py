from typing import Dict, Tuple, List, Sequence, Any
from collections import defaultdict

from mhs_rdb.models import MetricsModel


__all__ = ['MetricsGrouper']


class MetricsGrouper:

    @staticmethod
    def group_metric_dicts(metric_dicts: List[Dict[str, Any]]) -> List[Dict[str, Any]]:
        grouped_metrics: Dict[Tuple[str, int], List[int]] = defaultdict(list)
        for metric in metric_dicts:
            grouped_metrics[(metric['display_name'], metric['units'])].append(metric['id'])

        result = []
        for (display_name, units), metric_ids in grouped_metrics.items():
            result.append({
                'display_name': display_name,
                'units': units,
                'metric_ids': metric_ids
            })

        return result

    @staticmethod
    def group_metrics(metrics: Sequence[MetricsModel]) -> List[Dict[str, Any]]:
        return MetricsGrouper.group_metric_dicts([m.to_dict(include_properties=True) for m in metrics])
