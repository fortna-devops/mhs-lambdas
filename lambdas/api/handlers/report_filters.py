import logging

from mhs_rdb import Metrics

from mhs_web_utils.request import Request
from mhs_web_utils.response import Response, JSONResponse
from mhs_web_utils.handler import Handler
from mhs_web_utils.handler.mixins import DBMixin, MethodMixin
from mhs_web_utils.validation import SiteValidator


__all__ = ['ReportFiltersHandler']


logger = logging.getLogger(__file__.split('.')[0].upper())


class ReportFiltersHandler(DBMixin, MethodMixin, Handler):
    allowed_methods = ('GET',)
    args_validator_cls = SiteValidator

    def get_handler(self, request: Request) -> Response:
        """
        Get list of metrics groped by display name
        """

        args = self.args_validator_cls(request.query).validate()
        with self.connector.get_session(args.pop('customer'), args.pop('site')) as session:
            metrics = Metrics(session).get_for_reports()

        return JSONResponse({'metrics': metrics})


TEST_EVENT = {
    'httpMethod': 'GET',
    'path': 'report-filters',
    'queryStringParameters': {
        'site': 'mhs_emulated'
    }
}
