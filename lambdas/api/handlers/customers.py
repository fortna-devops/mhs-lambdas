import os
import json

from typing import List
from datetime import datetime

from pytz import timezone

from mhs_rdb import CustomerSites
from mhs_rdb.models import CustomerSitesModel

from mhs_web_utils.authorizer import CognitoAuthorizer
from mhs_web_utils.request import Request
from mhs_web_utils.response import Response, JSONResponse
from mhs_web_utils.handler import Handler
from mhs_web_utils.handler.mixins import DBMixin, MethodMixin


class CustomersHandler(DBMixin, MethodMixin, Handler):
    authorizer_cls = CognitoAuthorizer

    allowed_methods = ('GET',)

    _customer_schema_name = 'customer'

    @staticmethod
    def _get_tz_offset(tz: str) -> float:
        """
        Get offset in hours for specified time zone
        :param tz: Time zone string e.g. 'US/Eastern'
        :return: Offset from UTC in hours
        """
        offset = timezone(tz).utcoffset(datetime.utcnow())
        if offset is None:
            return 0

        return offset.total_seconds() / 3600

    def _get_customer_sites(self, database: str) -> List[CustomerSitesModel]:
        """
        Returns customer sites.
        """

        with self.connector.get_session(database, self._customer_schema_name) as session:
            return CustomerSites(session).get_all()

    def get_handler(self, _request: Request) -> Response:
        """
        Response example:
        {
            "customers": [
                {
                    "id": "mhs",
                    "name": "MHS",
                    "sites": [
                        {
                            "id": "mhs-emulated",
                            "timezone_offset_hours": -7.0
                        }
                    ]
                }
            ]
        }
        """

        customer_ids = self.authorizer.token.customers
        groups = self.authorizer.token.groups

        customers = {}

        for customer in json.loads(os.environ['customers']):
            if customer['id'] not in customer_ids:
                continue
            customers[customer['id']] = customer

        for customer in customers.values():
            customer['sites'] = []
            for customer_site in self._get_customer_sites(customer['id']):
                if customer_site.name not in groups:
                    continue
                customer['sites'].append({
                    'id': customer_site.name,
                    'timezone_offset_hours': self._get_tz_offset(customer_site.timezone)
                })

        return JSONResponse({'customers': list(customers.values())})


TEST_EVENT = {
    'path': 'customers',
    'httpMethod': 'GET'
}
