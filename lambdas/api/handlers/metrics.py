import logging
from datetime import datetime, timedelta
from typing import Dict, Any, List

from mhs_rdb import Metrics
from mhs_rdb.models import ColorsEnum
from mhs_rdb.serializers import BaseSerializer

from mhs_web_utils.request import Request
from mhs_web_utils.response import Response, JSONResponse
from mhs_web_utils.handler import Handler
from mhs_web_utils.handler.mixins import DBMixin, MethodMixin
from mhs_web_utils.validation import SiteValidator, StrField, IntField, BoolField, ListField, TimedeltaField


__all__ = ['MetricsHandler']


logger = logging.getLogger(__file__.split('.')[0].upper())


class ArgsValidator(SiteValidator):

    lookup_days = TimedeltaField(units='days', default=timedelta(days=7))
    display_names = ListField(StrField(), required=False)
    alarm_type_names = ListField(StrField(), required=False)
    alarm_colors = ListField(StrField(choices=(ColorsEnum.YELLOW.name, ColorsEnum.RED.name)), required=False)
    offset = IntField(required=False)
    limit = IntField(required=False)
    count = BoolField(required=False)


class MetricsSerializer(BaseSerializer):

    def serialize(self, metric_dicts: List[Dict[str, Any]]) -> List[Dict[str, Any]]:
        serialized_metrics = []
        for metric_dict in metric_dicts:

            serialized_metric = {
                'id': metric_dict['id'],
                'display_name': metric_dict['display_name'],
                'units': metric_dict['units'],
                'num_alarms': metric_dict['num_alarms'],
                'data_source': {
                    'id': metric_dict['data_source_id'],
                    'type_name': metric_dict['data_source_type_name']
                },
                'component': {
                    'id': metric_dict['component_id'],
                    'name': metric_dict['component_name'],
                    'type_name': metric_dict['component_type_name']
                },
                'asset': {
                    'id': metric_dict['asset_id'],
                    'name': metric_dict['asset_name']
                }
            }
            serialized_metrics.append(serialized_metric)

        return serialized_metrics


class MetricsHandler(DBMixin, MethodMixin, Handler):
    allowed_methods = ('GET',)
    args_validator_cls = ArgsValidator

    def get_handler(self, request: Request) -> Response:
        """
        Get list of metrics groped by display name
        """

        args = self.args_validator_cls(request.query).validate()
        args['end'] = datetime.utcnow()
        args['start'] = args['end'] - args.pop('lookup_days')
        with self.connector.get_session(args.pop('customer'), args.pop('site')) as session:
            metrics = Metrics(session).get_alarming(**args)

        if isinstance(metrics, int):
            return JSONResponse({'count': metrics})

        return JSONResponse({'metrics': MetricsSerializer().serialize(metrics)})


TEST_EVENT = {
    'httpMethod': 'GET',
    'path': 'metrics',
    'queryStringParameters': {
        'site': 'mhs_emulated',
        'display_names': 'Temperature',
        'alarm_type_names': 'iso_persistent',
        'alarm_colors': 'YELLOW',
        'lookup_days': '7'
    }
}
