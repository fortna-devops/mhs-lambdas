import base64
import json
import logging
import os
from typing import Dict, Any, Sequence, Optional, List
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication

from boto3.session import Session as BotoSession
from botocore.exceptions import ClientError

from mhs_web_utils.authorizer import CognitoAuthorizer
from mhs_web_utils.request import Request
from mhs_web_utils.response import Response, JSONResponse
from mhs_web_utils.handler import Handler
from mhs_web_utils.handler.mixins import MethodMixin
from mhs_web_utils.validation import Validator, StrField, ListField, Base64EncodedField, NestedDataField


__all__ = ['SendEmailHandler']


logger = logging.getLogger(__file__.split('.')[0].upper())


class AttachmentsValidator(Validator):
    file_name = StrField()
    file_content = Base64EncodedField()


class BodyValidator(Validator):
    subject = StrField()
    recipients = ListField(StrField())
    text = StrField(required=False)
    html = StrField(required=False)
    attachments = ListField(NestedDataField(AttachmentsValidator))

    @staticmethod
    def clean_attachments(value: List[Dict[str, Any]]) -> Dict[str, bytes]:
        return {attachment['file_name']: attachment['file_content'] for attachment in value}


class SendEmailHandler(MethodMixin, Handler):
    """
    Interface to send an email
    """
    authorizer_cls = CognitoAuthorizer

    allowed_methods = ('POST',)
    body_validator_cls = BodyValidator
    default_sender: str = 'no-reply@mhsinsights.com'

    boto_session = BotoSession(profile_name=os.getenv('profile'))

    @staticmethod
    def _build_multipart_message(sender: str, recipients: Sequence[str], subject: str, text: Optional[str] = None,
                                 html: Optional[str] = None, attachments: Optional[Dict[str, bytes]] = None
                                 ) -> MIMEMultipart:
        """
        Creates a MIME multipart message object.
        Uses only the Python `email` standard library.
        Emails, both sender and recipients, can be just the email string
        or have the format 'The Name <the_email@host.com>'.

        :param sender: The sender.
        :param recipients: List of recipients. Needs to be a list, even if only one recipient.
        :param subject: The subject of the email.
        :param text: The text version of the email body (optional).
        :param html: The html version of the email body (optional).
        :param attachments: List of files to attach in the email.
        :return: A `MIMEMultipart` to be used to send the email.
        """
        multipart_content_subtype = 'alternative' if text and html else 'mixed'
        msg = MIMEMultipart(multipart_content_subtype)
        msg['Subject'] = subject
        msg['From'] = sender
        msg['To'] = ', '.join(recipients)

        # Record the MIME types of both parts - text/plain and text/html.
        # According to RFC 2046, the last part of a multipart message,
        # in this case the HTML message, is best and preferred.
        if text:
            part = MIMEText(text, 'plain')
            msg.attach(part)
        if html:
            part = MIMEText(html, 'html')
            msg.attach(part)

        # Add attachments
        if attachments:
            for name, attachment in attachments.items():
                a_part = MIMEApplication(attachment)
                a_part.add_header('Content-Disposition', 'attachment', filename=name)
                msg.attach(a_part)

        return msg

    def _send_email(self, recipient: str, subject: str, text: Optional[str] = None, html: Optional[str] = None,
                    attachments: Optional[Dict[str, bytes]] = None) -> Dict[str, Any]:
        """
        Send email to the recipient.

        :param recipient: Recipient email, can be just the email string
            or have the format 'The Name <the_email@host.com>'.
        :param subject: The subject of the email.
        :param text: The text version of the email body (optional).
        :param html: The html version of the email body (optional).
        :param attachments: List of files to attach in the email.
        :return: Status dict with an error message in case of any.
        """
        client = self.boto_session.client('ses')

        try:
            recipients = [recipient]
            msg = self._build_multipart_message(sender=self.default_sender, recipients=recipients, subject=subject,
                                                text=text, html=html, attachments=attachments)
            ses_response = client.send_raw_email(
                Source=self.default_sender,
                Destinations=recipients,
                RawMessage={'Data': msg.as_string()}
            )
            logger.debug(ses_response)
        except ClientError as client_error:
            msg = client_error.response['Error']['Message']
            logger.error(msg)
            return {
                'recipient': recipient,
                'success': False,
                'message': msg
            }

        return {
            'recipient': recipient,
            'success': True
        }

    def post_handler(self, request: Request) -> Response:
        """
        Send e-mail message to recipients

        Request:
        {
            "subject": "string",
            "recipients": ["list of strings"],
            "text": "optional string",
            "html": "optional string",
            "attachments": [
                {
                    "file_name": "string",
                    "file_content": "base64 encoded data"
                }
            ]
        }

        Response:
        [
            {
                "recipient": "e-mail address of recipient"
                "success": true,
                "message": "message in case of error occurred"
            }
        ]
        """
        body = self.body_validator_cls(json.loads(request.body)).validate()

        results = []
        for recipient in body.pop('recipients'):
            result = self._send_email(recipient=recipient, **body)
            results.append(result)

        return JSONResponse(results)


TEST_EVENT = {
    'httpMethod': 'POST',
    'path': 'send-email',
    'body': json.dumps(
        {
            'subject': 'MHS test email',
            'recipients': ['igor.panteleyev@dataart.com'],
            'text': 'MHS test email',
            'html': '',
            'attachments': [
                {
                    'file_name': 'test_file.txt',
                    'file_content': base64.b64encode(b'test file content').decode()
                }
            ],
        }
    )
}
