"""
Alarm counts handler.
"""

from datetime import datetime

from typing import List, Dict, Any, Optional

from mhs_web_utils.request import Request
from mhs_web_utils.response import Response, JSONResponse
from mhs_web_utils.handler import Handler
from mhs_web_utils.handler.mixins import DBMixin, MethodMixin
from mhs_web_utils.validation import SiteValidator, StrField, IntField, DateTimeField, ListField

from mhs_rdb import Alarms
from mhs_rdb.models import FacilityAreasModel, ComponentsModel, MetricsModel, AlarmsModel

from .mixins import AlarmsConfigMixin


__all__ = ['AlarmCountsHandler']


class ArgsValidator(SiteValidator, AlarmsConfigMixin):
    """
    Args validator.
    """

    include_counts = 'counts'
    include_date_counts = 'date_counts'

    _include_choices = (include_counts, include_date_counts)

    component_ids = ListField(IntField(), required=False)
    start = DateTimeField(required=False)
    end = DateTimeField(required=False)
    timezone = IntField(required=False, default=0)
    include = ListField(StrField(choices=_include_choices), required=False, default=_include_choices)

    def validate(self) -> Dict[str, Any]:
        """
        Returns validated args.
        """

        args = super().validate()

        if args['start'] and args['end']:
            return args

        args['end'] = datetime.utcnow()
        args['start'] = args['end'] - self._count_alarm_data_timedelta

        return args


class AlarmCountsGrouper:
    """
    Alarm counts grouper.
    """

    def __init__(self, group_attr_name: str):
        self._group_attr_name = group_attr_name

    def _group_alarm_counts(self, alarm_counts: List[Dict[str, Any]]) -> List[Dict[str, Any]]:
        """
        Returns grouped alarm counts.
        """

        grouped_alarm_counts: Dict[Any, Any] = {}
        for alarm_count in alarm_counts:

            group_attr_value = alarm_count[self._group_attr_name]

            if group_attr_value not in grouped_alarm_counts:
                grouped_alarm_counts[group_attr_value] = {
                    self._group_attr_name: group_attr_value
                }

                for attr_name, attr_value in alarm_count.items():
                    if attr_name in ('metric_id', 'metric_display_name', 'alarm_type_name', 'num_alarms'):
                        continue
                    grouped_alarm_counts[group_attr_value][attr_name] = attr_value

                grouped_alarm_counts[group_attr_value]['metrics'] = {}

            metrics = grouped_alarm_counts[group_attr_value]['metrics']
            if alarm_count['metric_id'] not in metrics:
                metrics[alarm_count['metric_id']] = {
                    'metric_id': alarm_count['metric_id'],
                    'metric_display_name': alarm_count['metric_display_name'],
                    'counts': []
                }

            metrics[alarm_count['metric_id']]['counts'].append({
                'alarm_type_name': alarm_count['alarm_type_name'],
                'num_alarms': alarm_count['num_alarms']
            })

        for grouped_alarm_count in grouped_alarm_counts.values():
            grouped_alarm_count['metrics'] = list(grouped_alarm_count['metrics'].values())

        return list(grouped_alarm_counts.values())

    def _group_alarm_date_counts(self, alarm_date_counts: List[Dict[str, Any]]) -> List[Dict[str, Any]]:
        """
        Returns grouped alarm date counts.
        """

        grouped_alarm_date_counts: Dict[Any, Any] = {}

        for alarm_date_count in alarm_date_counts:

            group_attr_value = alarm_date_count[self._group_attr_name]

            if group_attr_value not in grouped_alarm_date_counts:
                grouped_alarm_date_counts[group_attr_value] = {
                    self._group_attr_name: group_attr_value
                }

                for attr_name, attr_value in alarm_date_count.items():
                    if attr_name in ('date', 'num_alarms'):
                        continue
                    grouped_alarm_date_counts[group_attr_value][attr_name] = attr_value

                grouped_alarm_date_counts[group_attr_value]['counts'] = []

            grouped_alarm_date_counts[group_attr_value]['counts'].append({
                'date': alarm_date_count['date'],
                'num_alarms': alarm_date_count['num_alarms']
            })

        return list(grouped_alarm_date_counts.values())

    def group(self,
              alarm_counts: Optional[List[Dict[str, Any]]] = None,
              alarm_date_counts: Optional[List[Dict[str, Any]]] = None) -> Dict[str, List[Dict[str, Any]]]:
        """
        Returns grouped counts.
        """

        counts = {}
        if alarm_counts:
            counts['counts'] = self._group_alarm_counts(alarm_counts)

        if alarm_date_counts:
            counts['date_counts'] = self._group_alarm_date_counts(alarm_date_counts)

        return counts


class AlarmCountsHandler(DBMixin, MethodMixin, Handler):
    """
    Handles alarm counts.
    """

    allowed_methods = ('GET',)

    def get_handler(self, request: Request) -> Response:
        """
        Request handler.

        Response example:

        {
            "counts": [
                {
                    "component_id": 1,
                    "component_name": "Motor",
                    "metrics": [
                        {
                            "metric_id": 3,
                            "metric_display_name": "temperature",
                            "counts": [
                                {
                                    "alarm_type_name": "ISO_PERSISTENT",
                                    "num_alarms": 2
                                }
                            ]
                        }
                    ]
                }
            ],
            "date_counts": [
                {
                    "component_id": 1,
                    "component_name": "Motor",
                    "counts": [
                        {
                            "date": "2020-09-20",
                            "num_alarms": 1
                        }
                    ]
                }
            ]
        }
        """

        args = ArgsValidator(request.query).validate()

        customer = args.pop('customer')
        site = args.pop('site')
        timezone = str(args.pop('timezone'))
        include = args.pop('include')

        group_attr_name = 'facility_area_id'
        group_by = [FacilityAreasModel.id.label(group_attr_name), FacilityAreasModel.name.label('facility_area_name')]

        if args['component_ids']:
            group_attr_name = 'component_id'
            group_by = [ComponentsModel.id.label(group_attr_name), ComponentsModel.name.label('component_name')]

        group_kwargs = {}
        if ArgsValidator.include_counts in include:
            args['group_by'] = group_by + [MetricsModel.id.label('metric_id'),
                                           MetricsModel.display_name.label('metric_display_name'),
                                           AlarmsModel.type_name.label('alarm_type_name')]
            with self.connector.get_session(customer, site) as session:
                group_kwargs['alarm_counts'] = Alarms(session).count(**args)

        if ArgsValidator.include_date_counts in include:
            with self.connector.get_session(customer, site) as session:
                args['group_by'] = group_by
                group_kwargs['alarm_date_counts'] = Alarms(session).count_by_date(timezone=timezone, **args)

        return JSONResponse(AlarmCountsGrouper(group_attr_name).group(**group_kwargs))


TEST_EVENT = {
    'httpMethod': 'GET',
    'path': 'alarm-counts',
    'queryStringParameters': {
        'site': 'mhs-emulated',
        'component_ids': '13',
        # 'start': '2019-10-01T00:00:00.0Z',
        # 'end': '2020-05-01T00:00:00.0Z',
        # 'include': 'date_counts'
    }
}
