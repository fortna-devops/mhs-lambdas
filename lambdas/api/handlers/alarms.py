"""
Alarms handler.
"""

from datetime import datetime
from typing import List, Dict, Any

from mhs_web_utils.request import Request
from mhs_web_utils.response import Response, JSONResponse
from mhs_web_utils.handler import Handler
from mhs_web_utils.handler.mixins import DBMixin, MethodMixin
from mhs_web_utils.validation import SiteValidator, IntField, BoolField, StrField, DateTimeField, ListField

from mhs_rdb import Alarms
from mhs_rdb.models import AlarmsModel, ColorsEnum
from mhs_rdb.serializers import ModelDictSerializer

from .mixins import AlarmsConfigMixin


__all__ = ['AlarmsHandler']


class ArgsValidator(SiteValidator, AlarmsConfigMixin):
    """
    Args validator.
    """

    asset_ids = ListField(IntField(), required=False)
    component_ids = ListField(IntField(), required=False)
    data_source_ids = ListField(IntField(), required=False)
    metric_ids = ListField(IntField(), required=False)
    start = DateTimeField(required=False)
    end = DateTimeField(required=False)
    colors = ListField(StrField(choices=(ColorsEnum.YELLOW.name, ColorsEnum.RED.name)), required=False)
    type_names = ListField(StrField(), required=False)
    group_by_metric = BoolField(required=False)
    offset = IntField(required=False)
    limit = IntField(required=False)
    count = BoolField(required=False)

    def validate(self) -> Dict[str, Any]:
        """
        Returns validated args.
        """

        args = super().validate()

        if args['start'] and args['end']:
            return args

        args['end'] = datetime.utcnow()
        args['start'] = args['end'] - self._alarm_data_timedelta

        return args


class MetricSerializer(ModelDictSerializer):
    """
    Metric serializer.
    """

    included_attrs = ('id', 'name', 'display_name', 'units')


class DataSourceSerializer(ModelDictSerializer):
    """
    Data source serializer.
    """

    included_attrs = ('id',)


class ComponentSerializer(ModelDictSerializer):
    """
    Component serializer.
    """

    included_attrs = ('id', 'name')


class AssetSerializer(ModelDictSerializer):
    """
    Asset serializer.
    """

    included_attrs = ('id', 'name')


class AlarmsSerializer(ModelDictSerializer):
    """
    Alarms serializer.
    """

    included_attrs = (
        'id',
        'started_at',
        'ended_at',
        'color',
        'type_name',
        'trigger_value',
        'trigger_criteria'
    )

    # pylint: disable=W0221
    def _serialize_model(self, alarm: AlarmsModel) -> Dict[str, Any]:
        """
        Returns serialized alarm.
        """

        serialized_alarm = super()._serialize_model(alarm)
        serialized_alarm['description'] = alarm.description

        serialized_alarm['metric'] = MetricSerializer().serialize(alarm.metric)
        serialized_alarm['data_source'] = DataSourceSerializer().serialize(alarm.metric.data_source)
        serialized_alarm['data_source'].update({'type_name': alarm.metric.data_source.type.name})
        serialized_alarm['component'] = ComponentSerializer().serialize(alarm.metric.data_source.component)
        serialized_alarm['component'].update({'type_name': alarm.metric.data_source.component.type.name})
        serialized_alarm['asset'] = AssetSerializer().serialize(alarm.metric.data_source.component.asset)

        return serialized_alarm


class AlarmsHandler(DBMixin, MethodMixin, Handler):
    """
    Handles alarms.
    """

    allowed_methods = ('GET',)

    def get_handler(self, request: Request) -> Response:
        """
        Handles request.

        Response example:
        {
            "alarms": [
                {
                    "id": 1,
                    "started_at": "2020-01-01 00.00.00.000",
                    "ended_at": "2020-01-02 00.00.00.000",
                    "color": 'RED',
                    "type_name": "ISO_PERSISTENT",
                    "trigger_value": 2.2,
                    "trigger_criteria": 2.1,
                    "description": "Description",
                    "metric": {
                        "id": 155,
                        "name": "temperature",
                        "display_name": "Temperature",
                        "units": "C"
                    },
                    "data_source": {
                        "id": 12,
                        "type_name": "qm42vt2"
                    },
                    "component": {
                        "id": 9,
                        "type_name": "gearbox",
                        "name": "gearbox"
                    }
                    "asset": {
                        "id": 1,
                        "name": "sorter"
                    }
                }
            ]
        }
        """
        kwargs = ArgsValidator(request.query).validate()
        group_by_metric = kwargs.pop('group_by_metric')
        with self.connector.get_session(kwargs.pop('customer'), kwargs.pop('site')) as session:
            alarms = Alarms(session).filter(**kwargs)

        if isinstance(alarms, int):
            return JSONResponse({'count': alarms})

        serialized_alarms: List[Dict[str, Any]] = AlarmsSerializer().serialize(alarms)
        if not group_by_metric:
            return JSONResponse({'alarms': serialized_alarms})

        keys = ('metric', 'data_source', 'component', 'asset')
        metrics: Dict[int, Any] = {}
        for serialized_alarm in serialized_alarms:

            metric_id = serialized_alarm['metric']['id']
            if metric_id not in metrics:
                metrics[metric_id] = {}
                for key in keys:
                    metrics[metric_id][key] = serialized_alarm[key]

            for key in keys:
                del serialized_alarm[key]

            if serialized_alarm['type_name'] not in metrics[metric_id]:
                metrics[metric_id][serialized_alarm['type_name']] = []

            metrics[metric_id][serialized_alarm['type_name']].append(serialized_alarm)

        return JSONResponse({'alarms': list(metrics.values())})


TEST_EVENT = {
    'httpMethod': 'GET',
    'path': 'alarms',
    'queryStringParameters': {
        'site': 'mhs-emulated',
        'colors': ['RED'],
        'group_by_metric': 'True'
        # 'asset_ids': '1',
        # 'component_ids': '1,2',
        # 'data_source_ids': '201',
        # 'metric_ids': '30366, 29881',
        # 'start': '2020-08-21T00:00:00.843125Z',
        # 'end': '2020-08-22T00:00:00.843125Z',
        # 'severities': '2',
        # 'type_names': 'ISO_PERSISTENT',
        # 'offset': '0',
        # 'limit': '0',
        # 'count': 'True'
    }
}
