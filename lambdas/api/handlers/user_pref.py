import os
import json

from boto3.session import Session as BotoSession

from mhs_web_utils.authorizer import CognitoAuthorizer
from mhs_web_utils.request import Request
from mhs_web_utils.response import Response, JSONResponse
from mhs_web_utils.handler import Handler
from mhs_web_utils.handler.mixins import MethodMixin


__all__ = ['UserPreferencesHandler']


class UserPreferencesHandler(MethodMixin, Handler):
    """
    Interface to store and retrieve user preferences
    """

    authorizer_cls = CognitoAuthorizer

    allowed_methods = ('GET', 'POST')
    cognito_preference_key = 'custom:preferences'

    boto_session = BotoSession(profile_name=os.getenv('profile'))

    @staticmethod
    def get_user_pool_id() -> str:
        """
        Returns user pool id
        """
        return json.loads(os.environ['cognito_user_pool'])['id']

    def get_handler(self, _request: Request) -> Response:
        """
        Get user preferences from cognito
        """
        user = self.boto_session.client('cognito-idp').admin_get_user(
            UserPoolId=self.get_user_pool_id(),
            Username=self.authorizer.token.user_name
        )

        for attr in user['UserAttributes']:
            if attr['Name'] == self.cognito_preference_key:
                preferences = json.loads(attr['Value'])
                break
        else:
            preferences = {}

        return JSONResponse(preferences)

    def post_handler(self, request: Request) -> Response:
        """
        Store user preferences in cognito
        """
        try:
            num_set_preferences = len(json.loads(request.body))
        except ValueError:
            return JSONResponse({'validation_error': 'body is not JSON'}, 400)

        self.boto_session.client('cognito-idp').admin_update_user_attributes(
            UserPoolId=self.get_user_pool_id(),
            Username=self.authorizer.token.user_name,
            UserAttributes=[
                {
                    'Name': self.cognito_preference_key,
                    'Value': request.body
                }
            ]
        )

        return JSONResponse({'num_set_preferences': num_set_preferences})


TEST_EVENT = {
    'httpMethod': 'GET',
    'path': 'user-pref',
    'body': json.dumps({
        'key_1': 'val_1',
        'key_2': 'val_2',
        'key_3': 'val_3'
    })
}
