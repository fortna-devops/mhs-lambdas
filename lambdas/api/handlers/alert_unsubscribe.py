import logging

from mhs_rdb import DataSources, AlertBlackList
from mhs_web_utils.request import Request
from mhs_web_utils.response import Response
from mhs_web_utils.handler import Handler
from mhs_web_utils.handler.mixins import DBMixin, MethodMixin
from mhs_web_utils.validation import SiteValidator, StrField, IntField
from mhs_web_utils.authorizer import SignatureAuthorizer


__all__ = ['AlertUnsubscribeHandler']


logger = logging.getLogger(__file__.split('.')[0].upper())


class ArgsValidator(SiteValidator):
    email = StrField()
    timestamp = StrField()
    data_source_id = IntField(required=False)
    signature = StrField()


class AlertUnsubscribeHandler(DBMixin, MethodMixin, Handler):

    allowed_methods = ('GET',)
    authorizer_cls = SignatureAuthorizer

    def _add_data_source_to_black_list(self, customer: str, site: str, email: str, data_source_id: str):
        """
        Add data source to black list.
        """

        with self.connector.get_session(customer, site) as session:
            black_list = AlertBlackList.model(alert_user_email=email, data_source_id=data_source_id)
            AlertBlackList(session).insert(black_list)

    def _add_site_to_black_list(self, customer: str, site: str, email: str):
        """
        Add all data sources on site to black list.
        """

        black_lists = []
        with self.connector.get_session(customer, site) as session:
            for data_source_id in DataSources(session).get_ids():
                black_lists.append(AlertBlackList.model(alert_user_email=email, data_source_id=data_source_id))
            AlertBlackList(session).insert_all(black_lists)

    def get_handler(self, request: Request) -> Response:
        """
        Unsubscribe user from alerts
        """

        kwargs = ArgsValidator(request.query).validate()
        customer = kwargs['customer']
        site = kwargs['site']
        email = kwargs['email']
        data_source_id = kwargs.get('data_source_id')

        if data_source_id:
            self._add_data_source_to_black_list(customer, site, email, data_source_id)
            return Response(f'You have been unsubscribed from data source {data_source_id} alerts')

        self._add_site_to_black_list(customer, site, email)
        return Response(f'You have been unsubscribed from site {site} alerts')


TEST_EVENT = {
    'httpMethod': 'GET',
    'path': 'alert-unsubscribe',
    'queryStringParameters': {
        'customer': 'mhs',
        'site': 'mhs_emulated',
        'email': 'Dmitry.Platon@dataart.com',
        'timestamp': '1575465243569',
        'data_source_id': '5',
        'signature': 'f197e2824702ca2c4b4b69f769fcac4156abdf0ec4c6553976b615bee927607a'
    }
}
