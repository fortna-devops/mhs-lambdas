from typing import List, Dict, Any
from datetime import datetime

from mhs_web_utils.authorizer import CognitoCustomerAuthorizer
from mhs_web_utils.request import Request
from mhs_web_utils.response import Response, JSONResponse
from mhs_web_utils.handler import Handler
from mhs_web_utils.handler.mixins import DBMixin, MethodMixin
from mhs_web_utils.validation import CustomerValidator, DateTimeField

from mhs_rdb import CustomerSites, Metrics
from mhs_rdb.models import ColorsEnum
from mhs_rdb.utils.color_calculators import DefaultColorCalculator as ColorCalculator
from mhs_rdb.utils.color_calculators import Colors

from .mixins import AlarmsConfigMixin


__all__ = ['CustomerEventsHandler']


def serialize_site(site: str, metrics: List[Dict[str, Any]], colors: Colors) -> Dict[str, Any]:
    """
    Serializes site and returns it with lists of assets, components and metrics.
    """

    serialized_site: Dict[str, Any] = {
        'site': site,
        'color': ColorsEnum.GREEN,
        'num_alarms': 0,
        'assets': {}
    }

    for metric in metrics:

        assets = serialized_site['assets']
        if metric['asset_id'] not in assets:
            assets[metric['asset_id']] = {
                'id': metric['asset_id'],
                'name': metric['asset_name'],
                'color': ColorsEnum.GREEN,
                'num_alarms': 0,
                'components': {}
            }

        components = assets[metric['asset_id']]['components']
        if metric['component_id'] not in components:
            components[metric['component_id']] = {
                'id': metric['component_id'],
                'name': metric['component_name'],
                'type_name': metric['component_type_name'],
                'color': colors.get_color(metric['component_id']),
                'num_alarms': 0,
                'metrics': []
            }

        components[metric['component_id']]['metrics'].append({
            'id': metric['id'],
            'display_name': metric['display_name'],
            'units': metric['units'],
            'num_alarms': metric['num_alarms']
        })

        if serialized_site['color'].value < components[metric['component_id']]['color'].value:
            serialized_site['color'] = components[metric['component_id']]['color']

        if assets[metric['asset_id']]['color'].value < components[metric['component_id']]['color'].value:
            assets[metric['asset_id']]['color'] = components[metric['component_id']]['color']

        serialized_site['num_alarms'] += metric['num_alarms']
        assets[metric['asset_id']]['num_alarms'] += metric['num_alarms']
        components[metric['component_id']]['num_alarms'] += metric['num_alarms']

    serialized_site['assets'] = list(serialized_site['assets'].values())
    for asset in serialized_site['assets']:
        asset['components'] = list(asset['components'].values())

    return serialized_site


class ArgsValidator(CustomerValidator, AlarmsConfigMixin):
    """
    Args validator.
    """

    start = DateTimeField(required=False)
    end = DateTimeField(required=False)

    def validate(self) -> Dict[str, Any]:
        """
        Returns validated args.
        """

        args = super().validate()

        if args['start'] and args['end']:
            return args

        args['end'] = datetime.utcnow()
        args['start'] = args['end'] - self._alarm_data_timedelta

        return args


class CustomerEventsHandler(DBMixin, MethodMixin, Handler):

    authorizer_cls = CognitoCustomerAuthorizer

    allowed_methods = ('GET',)

    _customer_schema_name = 'customer'

    def get_handler(self, request: Request) -> Response:
        """
        Response example:

        {
            "num_alarms": 1,
            "sites": [
                {
                    "site": "mhs_emulated",
                    "num_alarms": 1,
                    "assets": [
                        {
                            "id": 1,
                            "name": "Sorter",
                            "color": "YELLOW",
                            "num_alarms": 1,
                            "components": [
                                {
                                    "id": 1,
                                    "name": "Motor",
                                    "type_name": "motor",
                                    "color": "YELLOW",
                                    "num_alarms": 1,
                                    "metrics": [
                                        {
                                            "id": 1793,
                                            "display_name": "RMS Vibrational Velocity X",
                                            "units": "in/sec",
                                            "num_alarms": 1
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        }

        """

        args = ArgsValidator(request.query).validate()
        customer = args['customer']
        start, end = args['start'], args['end']

        with self.connector.get_session(customer, self._customer_schema_name) as session:
            sites = [customer_site.schema_name for customer_site in CustomerSites(session).get_all()]

        events: Dict[str, Any] = {
            'num_alarms': 0,
            'sites': []
        }
        for site in sites:
            with self.connector.get_session(customer, site) as session:
                metrics = Metrics(session).get_alarming(start, end)

            component_ids = list({metric['component_id'] for metric in metrics})
            with self.connector.get_session(customer, site) as session:
                colors = ColorCalculator(session).calculate(
                    alarm_start=start,
                    alarm_end=end,
                    component_ids=component_ids
                )

            serialized_site = serialize_site(site, metrics, colors)
            events['num_alarms'] += serialized_site['num_alarms']
            events['sites'].append(serialized_site)

        return JSONResponse(events)


TEST_EVENT = {
    'httpMethod': 'GET',
    'path': 'customer-events',
    'queryStringParameters': {
        'customer': 'mhs'
    }
}
