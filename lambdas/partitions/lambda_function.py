#!/usr/bin/env python3

"""

Partitions data on the second minute of every day and updates the tables.

.. sectionauthor:: Julia Varzari

"""
import os
import time
import logging
import datetime
from botocore.exceptions import ClientError
from analytics_sdk_core.data_init.data_pull import DataQueryEngineLite as DQE
from mhs_rdb import ConfigManager

logger = logging.getLogger()
logger.setLevel(logging.INFO)



TABLES = [
    'sensor',
    'sensor_minute',
    'sensor_hour',
    'plc',
    'error',
    'ambient_env',
]


def add_partition(bucket_name, table, datestr):

    dqe = DQE(bucket_name)

    partition_query = f"ALTER TABLE `{bucket_name}`.`{table}` ADD IF NOT EXISTS PARTITION (date='{datestr}');"

    dqe.execute_generic_query(partition_query, pull_result=False)

    logging.info(f"Athena adding partition for {datestr} on table: {table}")


def lambda_handler(event, context):
    # update tables, run it on second minute of every day
    timestamp = time.time()
    datestr = datetime.datetime.fromtimestamp(int(timestamp)).strftime('%Y-%m-%d')
    active_env = os.environ['env']
    cm = ConfigManager(env=active_env)
    customer_sites = cm.customer_sites

    for customer in customer_sites:
        for site in customer_sites[customer]:
            bucket = f"mhspredict-site-{site.replace('_', '-')}"
            for table in TABLES:
                try:
                    add_partition(bucket, table, datestr)
                except ClientError as e:
                    if 'InvalidBucketName' in e.response['Error']['Message']:
                        logger.warning(f'(InvalidBucketName) Check that {bucket} bucket exists and it has a {table} table.')
                    logger.warning(e.response)

    return 'Success'

if __name__ == '__main__':
    os.environ['env'] = 'develop'
    os.environ['AWS_RDB_USERNAME'] = 'developer'
    lambda_handler({}, {})
