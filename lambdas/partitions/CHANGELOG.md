# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [1.2.0] - 2020-02-12

### Changed

- Replace `ConfigManager`.`site_bucket_names` toolbox property with `ConfigManager`.`get_config_vars` mhs-rdb property to get site names from RDS instead of Athena.
