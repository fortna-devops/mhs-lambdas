# Partitions
----------------------------

#### Background

A lambda for creating daily paritions for the tables that have timeseries type data inside of them.

----------------------------
#### Methodology

The lambda works by utilizing the toolbox to run an `ALTER TABLE` query for the following tables:
    `sensor`
    `sensor_minute`
    `sensor_hour`
    `plc`
    `error`

----------------------------
#### Configurable Parameters/Event Information:

The `event` and `context` parameters are empty.


1. **bucket**: List of bucket names for which partitions should be created

	For example: ['mhspredict-site-dhl-miami']

----------------------------

#### Lambda Triggers:

The lambda is invoked by a Cloudwatch Trigger for once per day @ 12 AM UTC time.