'use strict';
exports.handler = (event, context, callback) => {
    //Get contents of response
    const response = event.Records[0].cf.response;
    const headers = response.headers;

    //Set new headers
    headers['strict-transport-security'] = [
        {
            key: 'Strict-Transport-Security',
            value: 'max-age=2592000; includeSubdomains; preload'
        }
    ];
    headers['x-content-type-options'] = [
        {
            key: 'X-Content-Type-Options',
            value: 'nosniff'
        }
    ];
    headers['x-frame-options'] = [
        {
            key: 'X-Frame-Options',
            value: 'DENY'
        }
    ];
    headers['content-security-policy'] = [
        {
            key: 'Content-Security-Policy',
            value: "default-src 'self'; " +
                   "img-src 'self' data: www.google-analytics.com; " +
                   "frame-src 'self' data:; " +
                   "style-src 'self' 'unsafe-inline'; " +
                   "font-src 'self'; " +
                   "script-src 'self' 'unsafe-inline' " +
                       "www.googletagmanager.com www.google-analytics.com; " +
                   "connect-src 'self' " +
                       "*.execute-api.us-east-1.amazonaws.com " +
                       "www.google-analytics.com " +
                       "cognito-idp.us-east-1.amazonaws.com " +
                       "data:; "
        }
    ];
    headers['x-permitted-cross-domain-policies'] = [
        {
            key: 'X-Permitted-Cross-Domain-Policies',
            value: 'master-only'
        }
    ];

    //Return modified response
    callback(null, response);
};
