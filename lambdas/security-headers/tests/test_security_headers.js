var expect = require('chai').expect;
var security_headers = require('../index');

test_event = {
  "Records": [
    {
      "cf": {
        "config": {
          "distributionId": "EXAMPLE"
        },
        "response": {
          "status": "200",
          "statusDescription": "OK",
          "headers": {
            "vary": [
              {
                "key": "Vary",
                "value": "*"
              }
            ],
            "last-modified": [
              {
                "key": "Last-Modified",
                "value": "2016-11-25"
              }
            ],
            "x-amz-meta-last-modified": [
              {
                "key": "X-Amz-Meta-Last-Modified",
                "value": "2016-01-01"
              }
            ]
          }
        }
      }
    }
  ]
}

describe('test_security_headers', function () {
  it('check presence of required headers', function () {
    security_headers.handler(test_event, null, function(ctx, result) {
        expect(result).to.have.property('headers');
        expect(result['headers']).to.have.property('strict-transport-security');
        expect(result['headers']).to.have.property('x-content-type-options');
        expect(result['headers']).to.have.property('x-frame-options');
        expect(result['headers']).to.have.property('content-security-policy');
        expect(result['headers']).to.have.property('x-permitted-cross-domain-policies');
    })
  });

  it('check other headers unmodified', function () {
    security_headers.handler(test_event, null, function(ctx, result) {
        expect(result).to.include(test_event.Records[0].cf.response);
    })
  });
});
