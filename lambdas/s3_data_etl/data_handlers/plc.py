import logging
from typing import ClassVar

from .parquet import BaseParquetDataHandler


__all__ = ['PLCDataHandler']


logger = logging.getLogger()


class PLCDataHandler(BaseParquetDataHandler):
    do_take_sample: ClassVar[bool] = False
