import logging
from datetime import datetime

from mhs_rdb import Gateways

from .base import BaseDataHandler


__all__ = ['HeartbeatHandler']


logger = logging.getLogger()


class HeartbeatHandler(BaseDataHandler):

    def handle(self):
        with self._s3fs.open(self._event.file_path, 'rb') as f:
            data = f.read()

        try:
            timestamp = datetime.utcfromtimestamp(float(data) / 1000)
        except ValueError:
            logger.exception(f'Can\'t convert "{data}" to python datetime')
            return

        with self._config_manager.connector.get_session(self._event.customer, self._event.site) as session:
            Gateways(session).update(Gateways.model.name == self._event.gateway_name,
                                     values={'heartbeat_timestamp': timestamp})

        logger.info(f'New heartbeat timestamp for "{self._event.gateway_name}" gateway is {timestamp}')
