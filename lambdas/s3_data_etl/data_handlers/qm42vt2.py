import logging
from collections import defaultdict
from typing import Dict, Any, ClassVar, Sequence, Tuple, List

import pandas as pd

from mhs_rdb.models import DataStatusesEnum, AssetStatusesEnum
from mhs_rdb import AssetStatusTransitions

from .parquet import BaseParquetDataHandler


__all__ = ['QM42VT2DataHandler']


logger = logging.getLogger()


class QM42VT2DataHandler(BaseParquetDataHandler):
    """
    Process data from VT sensors

    min_rms_acceleration_x - threshold to calculate asset status
    component_types_priority - priority of component types that will be used in asset status transition calculation
    """
    min_rms_acceleration_x: ClassVar[float] = 0.02
    component_types_priority: Tuple[str, ...] = ('motor', 'gearbox', 'bearing')

    @classmethod
    def _is_asset_on(cls, data: Dict[str, Any]) -> bool:
        """
        Check asset status based rms_acceleration_x value
        :param data: Data dict to be checked
        :return: True - if asset is ON
        """
        return data['rms_acceleration_x'] > cls.min_rms_acceleration_x

    @classmethod
    def _get_data_flags(cls, data: Dict[str, Any]) -> int:
        """
        Calculate data flags based on data
        :param data: Data dict to be checked
        :return: Integer representation of binary flags
        """
        if not cls._is_asset_on(data):
            return DataStatusesEnum.asset_off.value
        return 0

    @classmethod
    def _get_asset_status(cls, data: Dict[str, Any]) -> AssetStatusesEnum:
        """
        Calculate asset status based on data
        :param data: Data dict to be checked
        :return: Status of the asset
        """
        if not cls._is_asset_on(data):
            return AssetStatusesEnum.OFF
        return AssetStatusesEnum.ON

    def _get_gateway_port_sensor_to_asset_id_map(self, gateway_port_sensor_combinations: Sequence[Tuple[str, str, str]]
                                                 ) -> Dict[int, Dict[str, Tuple[str, str, str]]]:
        """
        Build a dict to map gateway data structure to cloud data structure
        :param gateway_port_sensor_combinations: Sequence of combinations of gateway names, ports, and sensors
        that should be checked for metrics
        :return: Nested dict with asset id as a key and value is another dict
        with component type as a key and (gateway, port, address) as a value
        """
        assets_map: Dict[int, Dict[str, Tuple[str, str, str]]] = defaultdict(dict)
        for metric in self._get_cloud_structure(gateway_port_sensor_combinations):
            if not metric.data_source.gateway_associations:
                logger.warning(f'Can\'t find gateway association for data_source with id {metric.data_source_id} '
                               f'Will be skipped')
                continue

            if len(metric.data_source.gateway_associations) > 1:
                logger.warning(f'More than one gateway association found '
                               f'for data_source with id {metric.data_source_id} '
                               f'Will be skipped')
                continue

            association = metric.data_source.gateway_associations[0]
            gateway_key = (association.gateway.name, association.address, metric.data_source.address)

            assets_map[metric.data_source.component.asset_id][metric.data_source.component.type.name] = gateway_key

        return assets_map

    @classmethod
    def _order_by_component_priority_key(cls, item: Tuple[str, Tuple[str, str, str]]) -> int:
        """
        Calculate a sorting key based on `component_types_priority`
        :param item: item to be sorted
        :return: key for sorting based on `component_types_priority`
        """
        component_type, _ = item
        if component_type not in cls.component_types_priority:
            logger.warning(f'"{component_type}" is not in {cls.component_types_priority}')
            return -1
        return cls.component_types_priority.index(component_type)

    def _process_asset_statuses(self, data: pd.DataFrame):
        """
        Calculate asset status based on timeseries data and insert it into RDB
        :param data: `pd.DataFrame` with data
        """
        asset_map = self._get_gateway_port_sensor_to_asset_id_map(data.index.tolist())

        transitions: List[AssetStatusTransitions.model] = []
        for asset_id, components_map in asset_map.items():
            _, key = sorted(components_map.items(), key=self._order_by_component_priority_key)[0]
            row = data.loc[key].to_dict()

            status = self._get_asset_status(row)
            transition = AssetStatusTransitions.model(asset_id=asset_id, timestamp=row['timestamp'], status=status)
            transitions.append(transition)

        with self._config_manager.connector.get_session(self._event.customer, self._event.site) as session:
            AssetStatusTransitions(session).insert_all(transitions)

    def handle(self):
        """
        Process S3 event for qm42vt2 sensors
        """
        logger.info(f'Attempt to read data rows for {self._event.site}. Path: {self._event.file_path}')
        data = self._get_data()

        if data.empty:
            logger.warning(f'{self._event.file_path} is empty. Will be skipped.')
            return

        self._process_timeseries_data(data=data)
        self._process_asset_statuses(data=data)
