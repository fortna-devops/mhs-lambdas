import logging
from abc import ABC, abstractmethod

import s3fs
from boto3.session import Session as BotoSession

from mhs_rdb import ConfigManager


from event import Event


__all__ = ['BaseDataHandler']


logger = logging.getLogger()


class BaseDataHandler(ABC):
    """
    Base class for data handlers
    """

    def __init__(self, event: Event, config_manager: ConfigManager, boto_session: BotoSession):
        self._event = event
        self._config_manager = config_manager
        self._boto_session = boto_session
        self._s3fs = s3fs.S3FileSystem(session=self._boto_session)

    @abstractmethod
    def handle(self):
        """
        Process S3 event
        """
