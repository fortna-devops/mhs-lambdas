import logging
from typing import Dict, ClassVar, Tuple, Sequence, Any, List

import pandas as pd
from sqlalchemy import tuple_
from sqlalchemy.orm import contains_eager

from mhs_rdb.models import MetricsModel, DataSourcesModel, DataSourcesGatewaysAssociationModel, \
    GatewaysModel, ComponentsModel
from mhs_rdb.utils.data import DataProxy, DataPoints

from .base import BaseDataHandler


__all__ = ['BaseParquetDataHandler']


logger = logging.getLogger()


class BaseParquetDataHandler(BaseDataHandler):
    """
    Base class for parquet data handlers
    """

    index_fields: ClassVar[Tuple[str, ...]] = ('gateway', 'port', 'sensor')
    do_take_sample: ClassVar[bool] = True

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__cloud_structure_map: Dict[Tuple, Any] = {}

    @classmethod
    # pylint: disable=W0613
    def _get_data_flags(cls, data: Dict[str, Any]) -> int:
        return 0

    def _get_data(self) -> pd.DataFrame:
        """
        Read data from file
        :return: `pd.DataFrame` with data
        """
        with self._s3fs.open(self._event.file_path, 'rb') as f:
            data = pd.read_parquet(f, engine='fastparquet', index=self.index_fields).sort_index()

        if self.do_take_sample:
            data = self._take_data_sample(data)

        return data

    def _take_data_sample(self, data: pd.DataFrame) -> pd.DataFrame:
        """
        Apply mean sampling to data
        :param data: `pd.DataFrame` with data
        :return: `pd.DataFrame` with averaged data
        """
        data['timestamp'] = data['timestamp'].astype(int)
        data = data.mean(level=self.index_fields)
        data['timestamp'] = pd.to_datetime(data['timestamp'])
        return data

    def _get_cloud_structure(self, gateway_port_sensor_combinations: Sequence[Tuple[str, str, str]]
                             ) -> List[MetricsModel]:
        cache_key = tuple(gateway_port_sensor_combinations)

        if cache_key not in self.__cloud_structure_map:
            with self._config_manager.connector.get_session(self._event.customer, self._event.site) as session:
                cloud_structure = session.query(MetricsModel)\
                    .options(
                        contains_eager(
                            MetricsModel.data_source,
                            DataSourcesModel.component,
                            ComponentsModel.asset
                        ),
                        contains_eager(
                            MetricsModel.data_source,
                            DataSourcesModel.gateway_associations,
                            DataSourcesGatewaysAssociationModel.gateway
                        ),
                        contains_eager(
                            MetricsModel.data_source,
                            DataSourcesModel.component,
                            ComponentsModel.type
                        )
                    )\
                    .join(MetricsModel.data_source)\
                    .join(DataSourcesModel.component)\
                    .join(ComponentsModel.asset)\
                    .join(DataSourcesModel.gateway_associations)\
                    .join(DataSourcesGatewaysAssociationModel.gateway)\
                    .join(ComponentsModel.type)\
                    .filter(
                        tuple_(
                            GatewaysModel.name,
                            DataSourcesGatewaysAssociationModel.address,
                            DataSourcesModel.address
                        ).in_(gateway_port_sensor_combinations)
                    ).all()
            self.__cloud_structure_map[cache_key] = cloud_structure

        return self.__cloud_structure_map[cache_key]

    def _get_gateway_port_sensor_to_metric_map(self, gateway_port_sensor_combinations: Sequence[Tuple[str, str, str]]
                                              ) -> Dict[Tuple[str, str, str, str], MetricsModel]:
        """
        Build a dict to map gateway data structure to cloud data structure
        :param gateway_port_sensor_combinations: Sequence of combinations of gateway names, ports, and sensors
        that should be checked for metrics
        :return: Dict object with (gateway, port, address, metric_name) as a key and metric instance as a value
        """
        metrics_map: Dict[Tuple[str, str, str, str], MetricsModel] = {}
        for metric in self._get_cloud_structure(gateway_port_sensor_combinations):
            for association in metric.data_source.gateway_associations:
                key = (association.gateway.name, association.address, metric.data_source.address, metric.name)
                if key in metrics_map:
                    logger.warning(f'More than one metric found for '
                                   f'gateway={association.gateway.name}, port={association.address}, '
                                   f'address={metric.data_source.address}, metric_name={metric.name}. '
                                   f'First one will be used.')
                    continue

                metrics_map[key] = metric

        return metrics_map

    def _get_data_points(self, data: pd.DataFrame, gateway_cloud_map: Dict[Tuple[str, str, str, str], MetricsModel]
                         ) -> DataPoints:
        """
        Populate `DataPoints` object with data
        :param data: `pd.DataFrame` with data
        :param gateway_cloud_map: Dict to map gateway data to cloud structure
        :return: `DataPoints` object
        """
        data_points = DataPoints(DataProxy.raw_data_kind)
        skipped_metrics = set()
        data = data.set_index('timestamp', append=True).sort_index()
        for (gateway, port, address, timestamp), data_dict in data.to_dict('index').items():
            flags = self._get_data_flags(data_dict)
            for metric_name, value in data_dict.items():
                key = (gateway, port, address, metric_name)
                if key not in gateway_cloud_map:
                    skipped_metrics.add((gateway, port, address, metric_name))
                    continue
                data_points.add_data(metric=gateway_cloud_map[key], timestamp=timestamp, value=value, flags=flags)

        if skipped_metrics:
            logger.warning(f'Can\'t find metric id for next (gateway, port, address, metric_name) combinations: '
                           f'{skipped_metrics}')
        return data_points

    def _insert_timeseries_data(self, data_points: DataPoints):
        """
        Insert data into RDS
        :param data_points: data points to be inserted
        :return:
        """
        with self._config_manager.connector.get_session(self._event.customer, self._event.site) as session:
            DataProxy(session).insert(data_points=data_points)

    def _process_timeseries_data(self, data: pd.DataFrame):
        """
        Process timeseries data and insert it into RDB
        :param data: `pd.DataFrame` with data
        """
        gateway_cloud_map = self._get_gateway_port_sensor_to_metric_map(data.index.tolist())

        if not gateway_cloud_map:
            logger.warning(f'No metrics found for file {self._event.file_path}. Will be skipped.')
            return

        data_points = self._get_data_points(data=data, gateway_cloud_map=gateway_cloud_map)
        self._insert_timeseries_data(data_points)
        logger.info(f'{len(data_points)} data points handled for {self._event.site}. Path: {self._event.file_path}.')

    def handle(self):
        """
        Process S3 event for parquet files
        """
        logger.info(f'Attempt to read data rows for {self._event.site}. Path: {self._event.file_path}')
        data = self._get_data()

        if data.empty:
            logger.warning(f'{self._event.file_path} is empty. Will be skipped.')
            return

        self._process_timeseries_data(data=data)
