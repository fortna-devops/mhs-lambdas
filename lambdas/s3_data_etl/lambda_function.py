import logging
import os
from typing import Dict, Any, Type

from boto3.session import Session as BotoSession
from mhs_rdb import ConfigManager

from event import Event
from data_handlers import BaseDataHandler, QM42VT2DataHandler, M12FTH3QDataHandler, PLCDataHandler, HeartbeatHandler


logger = logging.getLogger()

if logger.handlers:
    logger.setLevel(logging.INFO)
else:
    logging.basicConfig(level=logging.INFO)


BOTO_SESSION = BotoSession(profile_name=os.getenv('profile'))


CONFIG_MANAGER = ConfigManager(
    env=os.environ['env'],
    boto_session=BOTO_SESSION,
    debug=os.environ.get('debug') == 'TRUE'
)


HANDLER_MAP: Dict[str, Type[BaseDataHandler]] = {
    'sensor': QM42VT2DataHandler,
    'ambient_env': M12FTH3QDataHandler,
    'plc': PLCDataHandler,
    'heartbeat.unixtimestamp': HeartbeatHandler,
}


def lambda_handler(e: Dict[str, Any], _context: Dict[str, Any]):
    """
    Entry point for S3 put event.

    Edge gateways sends files with every 60 seconds.
    Each file contains approximately 1 minute of data points.
    This lambda calculate average value for each metric per sensor per file.

    As a result we will get data sampled close to one minute.
    """
    event = Event(e)

    handler_cls = HANDLER_MAP.get(event.data_key)
    if not handler_cls:
        logger.warning(f'No handler found for {event}')
        return

    handler = handler_cls(event, CONFIG_MANAGER, BOTO_SESSION)
    handler.handle()


if __name__ == '__main__':
    TEST_EVENT = {
        "Records": [{
            "s3": {
                "bucket": {
                    "name": "mhspredict-site-mhs-emulated-dev-new"
                },
                "object": {
                    "key": "mhs-emulated/sensor/date=2020-09-23/0625c073ff35408e849ed1cf17097e25.parquet",
                    # "key": "mhs-emulated/ambient_env/date=2020-09-23/0661ab6ba2114b3783019c120e706213.parquet"
                    # "key": "mhs-emulated/heartbeat.unixtimestamp"
                }
            }
        }]
    }
    lambda_handler(TEST_EVENT, {})
