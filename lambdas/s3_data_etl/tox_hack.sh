#!/usr/bin/env bash

set -e

python -m pip install numpy==1.19.5
python -m pip install "$@"
