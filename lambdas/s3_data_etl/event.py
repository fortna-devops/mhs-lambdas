import urllib.parse
from typing import Dict, Any


__all__ = ['Event']


class Event:
    """
    AWS Event.
    """

    def __init__(self, event: Dict[str, Any]):
        self._event = event

    def __repr__(self) -> str:
        return f'bucket_name: {self.bucket_name}, object_key: {self._object_key}'

    @property
    def bucket_name(self) -> str:
        """
        Returns bucket name.
        """
        return self._event['Records'][0]['s3']['bucket']['name']

    @property
    def _object_key(self) -> str:
        """
        Returns object key.
        """
        return urllib.parse.unquote(self._event['Records'][0]['s3']['object']['key'])

    @property
    def file_path(self) -> str:
        """
        Returns file path.
        """
        return f's3://{self.bucket_name}/{self._object_key}'

    @property
    def data_key(self) -> str:
        """
        Returns data key for event key.
        e.g. for key "mhs-dataart/plc/date=2019-10-04/example.parquet" returns "plc"
        """
        return self._object_key.split('/')[1]

    @property
    def gateway_name(self) -> str:
        """
        Returns gateway name

        WARNING: our S3 structure doesn't support multiple gateways per site
        """
        return self._object_key.split('/')[0]

    @property
    def site(self) -> str:
        """
        Returns site name.
        """
        return self.gateway_name.replace('-', '_')

    @property
    def customer(self) -> str:
        """
        Returns customer name.
        """
        return self.site.split('_')[0]
