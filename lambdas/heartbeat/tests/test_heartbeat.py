#!/usr/bin/env python3

"""
Module for testing new-heartbeat lambda
"""

from datetime import datetime
import pytest

from handler import RawConfigParser, determine_bad_message, process_heartbeats, OFFLINE, ONLINE


@pytest.fixture
def mock_threshold():
    """mock heartbeat_threshold and email_threshold values"""
    return {
        'heartbeat_threshold': 300,
        'email_threshold': 43200
    }

def test_determine_bad_message_many_hours():
    """
    test that message determine returns message with issue
    """
    seconds_since = 1000*1000
    gateway = 'mhspredict-site-test-site'
    last_hb_str = 'July 01, 2019 09:30:32 PM'
    msg = determine_bad_message(seconds_since,gateway,last_hb_str)
    assert 'Check connectivity' in msg
    assert 'color:red' in msg
    assert last_hb_str in msg
    assert gateway in msg
    assert 'hours' in msg


def test_determine_bad_message_less_than_an_hour():
    """
    test that message determine returns message with issue
    """
    seconds_since = 900
    gateway = 'mhspredict-site-test-site'
    last_hb_str = 'July 01, 2019 09:30:32 PM'
    msg = determine_bad_message(seconds_since,gateway,last_hb_str)
    assert 'Check connectivity' in msg
    assert 'color:red' in msg
    assert last_hb_str in msg
    assert gateway in msg
    assert 'minutes' in msg

def test_process_heartbeats_heartbeat(mock_threshold):
    """
    make sure message does not contains an indication of an issue
    """

    email_threshold = mock_threshold['email_threshold']
    heartbeat_threshold = mock_threshold['heartbeat_threshold']

    site1 = "test-site"
    gateway1 = "test-site"
    heartbeat_time1 = datetime(2020, 9, 24, 23, 12, 41, 401840)
    last_message_status1 = "online"
    last_message_time1= datetime(2020, 9, 24, 23, 15,40, 922055)
    time_utc1 = datetime(2020, 9, 24, 23, 16, 48, 66248)

    items_list = [{'site':site1,'gateway':gateway1, 'heartbeat_time':heartbeat_time1,'last_message_status':last_message_status1,'last_message_time':last_message_time1,'time_utc':time_utc1}]

    message, write_and_send,status_by_gateway = process_heartbeats(items_list, heartbeat_threshold, email_threshold)

    assert gateway1 in message, f"expected {gateway1} in message"
    assert 'problem' not in message.lower(), f"Did not expect to see 'problem' in message: {message.lower()}"



def test_process_heartbeats_state_change_on_off(mock_threshold):
    """
    make sure the state_change variable is true when heartbeat goes from
    online to offline
    this would normally trigger emails to send
    """

    email_threshold = mock_threshold['email_threshold']
    heartbeat_threshold = mock_threshold['heartbeat_threshold']

    site1 = "test-site"
    gateway1 = "test-site"
    heartbeat_time1 = datetime(2020, 9, 24, 23, 12, 41, 401840)
    last_message_status1 = "online"
    last_message_time1= datetime(2020, 9, 24, 23, 15,40, 922055)
    time_utc1 = datetime(2020, 9, 25, 8, 15, 48, 66248)

    site2 = "other-site"
    gateway2 = "other-test-site"
    heartbeat_time2 = datetime(2020, 9, 24, 23, 12, 58, 518540)
    last_message_status2 = "online"
    last_message_time2= datetime(2020, 9, 24, 19, 52, 41, 401840)
    time_utc2 = datetime(2020, 9, 24, 23, 13, 48, 166687)

    items_list = [{'site':site1,'gateway':gateway1, 'heartbeat_time':heartbeat_time1,'last_message_status':last_message_status1,'last_message_time':last_message_time1,'time_utc':time_utc1}]
    items_list.append({'site':site2,'gateway':gateway2, 'heartbeat_time':heartbeat_time2,'last_message_status':last_message_status2,'last_message_time':last_message_time2,'time_utc':time_utc2})

    message, write_and_send,status_by_gateway = process_heartbeats(items_list, heartbeat_threshold, email_threshold)

    assert write_and_send, "a state change should have been detected, so write_and_send should be true"
    assert 'problem' in message.lower(), f"Expected to see 'problem' in message: {message.lower()}"


def test_process_heartbeats_stayed_off(mock_threshold):
    """
    make sure the state_change variable is false when heartbeat/ state does not change
    """

    email_threshold = mock_threshold['email_threshold']
    heartbeat_threshold = mock_threshold['heartbeat_threshold']

    site1 = "test-site"
    gateway1 = "test-site"
    heartbeat_time1 = datetime(2020, 9, 24, 23, 12, 41, 401840)
    last_message_status1 = "offline"
    last_message_time1= datetime(2020, 9, 24, 23, 15,40, 922055)
    time_utc1 = datetime(2020, 9, 25, 8, 15, 48, 66248)

    site2 = "other-site"
    gateway2 = "other-test-site"
    heartbeat_time2 = datetime(2020, 9, 24, 23, 12, 58, 518540)
    last_message_status2 = "online"
    last_message_time2= datetime(2020, 9, 24, 19, 52, 41, 401840)
    time_utc2 = datetime(2020, 9, 24, 23, 13, 48, 166687)

    items_list = [{'site':site1,'gateway':gateway1, 'heartbeat_time':heartbeat_time1,'last_message_status':last_message_status1,'last_message_time':last_message_time1,'time_utc':time_utc1}]
    items_list.append({'site':site2,'gateway':gateway2, 'heartbeat_time':heartbeat_time2,'last_message_status':last_message_status2,'last_message_time':last_message_time2,'time_utc':time_utc2})

    message, write_and_send,status_by_gateway = process_heartbeats(items_list, heartbeat_threshold, email_threshold)

    assert not write_and_send, "a state change should not have been detected, so write_and_send should be false"
    assert 'problem' in message.lower(), f"Expected to see 'problem' in message: {message.lower()}"


def test_process_heartbeats_state_change_off_on(mock_threshold):
    """
    make sure the state_change variable is true when heartbeat goes from
    offline to online
    this would normally trigger emails to send
    """

    email_threshold = mock_threshold['email_threshold']
    heartbeat_threshold = mock_threshold['heartbeat_threshold']

    site1 = "test-site"
    gateway1 = "test-site"
    heartbeat_time1 = datetime(2020, 9, 24, 23, 12, 41, 401840)
    last_message_status1 = "offline"
    last_message_time1= datetime(2020, 9, 24, 19, 52,40, 922055)
    time_utc1 = datetime(2020, 9, 24, 23, 13, 48, 66248)

    site2 = "other-site"
    gateway2 = "other-test-site"
    heartbeat_time2 = datetime(2020, 9, 24, 23, 12, 58, 518540)
    last_message_status2 = "online"
    last_message_time2= datetime(2020, 9, 24, 19, 52, 41, 401840)
    time_utc2 = datetime(2020, 9, 24, 23, 13, 48, 166687)

    items_list = [{'site':site1,'gateway':gateway1, 'heartbeat_time':heartbeat_time1,'last_message_status':last_message_status1,'last_message_time':last_message_time1,'time_utc':time_utc1}]
    items_list.append({'site':site2,'gateway':gateway2, 'heartbeat_time':heartbeat_time2,'last_message_status':last_message_status2,'last_message_time':last_message_time2,'time_utc':time_utc2})

    message, write_and_send,status_by_gateway = process_heartbeats(items_list, heartbeat_threshold, email_threshold)


    assert write_and_send, "a state change should have been detected, so write_and_send should be true"
    assert 'problem' not in message.lower(), f"Did not expect to see 'problem' in message: {message.lower()}"


def test_process_heartbeats_stayed_on(mock_threshold):
    """
    make sure the time difference is less than 43200 seconds (12 hours)
    this would not normally trigger emails to send
    """

    email_threshold = mock_threshold['email_threshold']
    heartbeat_threshold = mock_threshold['heartbeat_threshold']

    site1 = "test-site"
    gateway1 = "test-site"
    heartbeat_time1 = datetime(2020, 9, 24, 23, 12, 41, 401840)
    last_message_status1 = "online"
    last_message_time1= datetime(2020, 9, 24, 19, 52,40, 922055)
    time_utc1 = datetime(2020, 9, 24, 23, 13, 48, 66248)

    site2 = "other-site"
    gateway2 = "other-test-site"
    heartbeat_time2 = datetime(2020, 9, 24, 23, 12, 58, 518540)
    last_message_status2 = "online"
    last_message_time2= datetime(2020, 9, 24, 19, 52, 41, 401840)
    time_utc2 = datetime(2020, 9, 24, 23, 13, 48, 166687)

    items_list = [{'site':site1,'gateway':gateway1, 'heartbeat_time':heartbeat_time1,'last_message_status':last_message_status1,'last_message_time':last_message_time1,'time_utc':time_utc1}]
    items_list.append({'site':site2,'gateway':gateway2, 'heartbeat_time':heartbeat_time2,'last_message_status':last_message_status2,'last_message_time':last_message_time2,'time_utc':time_utc2})

    message, write_and_send,status_by_gateway = process_heartbeats(items_list, heartbeat_threshold, email_threshold)
    assert not write_and_send, "a state change should not have been detected and the time should not have exceeded the threshold meaning write_and_send should be false"
    assert 'problem' not in message.lower(), f"Did not expect to see 'problem' in message: {message.lower()}"


def test_process_heartbeats_delayed_emails(mock_threshold):
    """
    make sure the time difference is more than 43200 seconds (12 hours)
    this would normally trigger emails to send
    """
    
    email_threshold = mock_threshold['email_threshold']
    heartbeat_threshold = mock_threshold['heartbeat_threshold']

    site1 = "test-site"
    gateway1 = "test-site"
    heartbeat_time1 = datetime(2020, 9, 24, 23, 12, 41, 401840)
    last_message_status1 = "online"
    # last message sent was more than 12 hours previously
    last_message_time1= datetime(2020, 9, 23, 19, 52,40, 922055)
    time_utc1 = datetime(2020, 9, 24, 23, 13, 48, 66248)

    site2 = "other-site"
    gateway2 = "other-test-site"
    heartbeat_time2 = datetime(2020, 9, 24, 23, 12, 58, 518540)
    last_message_status2 = "online"
    last_message_time2= datetime(2020, 9, 24, 19, 52, 41, 401840)
    time_utc2 = datetime(2020, 9, 24, 23, 13, 48, 166687)

    items_list = [{'site':site1,'gateway':gateway1, 'heartbeat_time':heartbeat_time1,'last_message_status':last_message_status1,'last_message_time':last_message_time1,'time_utc':time_utc1}]
    items_list.append({'site':site2,'gateway':gateway2, 'heartbeat_time':heartbeat_time2,'last_message_status':last_message_status2,'last_message_time':last_message_time2,'time_utc':time_utc2})

    message, write_and_send,status_by_gateway = process_heartbeats(items_list, heartbeat_threshold, email_threshold)
    
    assert write_and_send, "a state change should not have been detected, but the time should have exceeded the threshold meaning write_and_send should be true"
    assert 'problem' not in message.lower(), f"Did not expect to see 'problem' in message: {message.lower()}"

def test_multiple_sites(mock_threshold):
    '''
    test output when there are multiple sites and only one requires an email to be sent
    '''

    email_threshold = mock_threshold['email_threshold']
    heartbeat_threshold = mock_threshold['heartbeat_threshold']

    site1 = "test-site"
    gateway1 = "test-site"
    heartbeat_time1 = datetime(2020, 9, 24, 23, 12, 41, 401840)
    last_message_status1 = "online"
    # last message sent was more than 12 hours previously
    last_message_time1= datetime(2020, 9, 23, 19, 52,40, 922055)
    time_utc1 = datetime(2020, 9, 24, 23, 13, 48, 66248)

    site2 = "other-site"
    gateway2 = "other-test-site"
    heartbeat_time2 = datetime(2020, 9, 24, 23, 12, 58, 518540)
    last_message_status2 = "online"
    last_message_time2= datetime(2020, 9, 24, 19, 52, 41, 401840)
    time_utc2 = datetime(2020, 9, 24, 23, 13, 48, 166687)

    items_list = [{'site':site1,'gateway':gateway1, 'heartbeat_time':heartbeat_time1,'last_message_status':last_message_status1,'last_message_time':last_message_time1,'time_utc':time_utc1}]
    items_list.append({'site':site2,'gateway':gateway2, 'heartbeat_time':heartbeat_time2,'last_message_status':last_message_status2,'last_message_time':last_message_time2,'time_utc':time_utc2})

    message, write_and_send,status_by_gateway = process_heartbeats(items_list, heartbeat_threshold, email_threshold)

    assert gateway1 in message, f"expected {gateway1} in message"
    assert gateway2 in message, f"expected {gateway2} in message"
    assert write_and_send, "an email should be sent due to the delay"
    assert 'problem' not in message.lower(), f"Did not expect to see 'problem' in message: {message.lower()}"


def test_multiple_gateways(mock_threshold):
    '''
    test output when there are multiple gateways in one bucket
    '''

    email_threshold = mock_threshold['email_threshold']
    heartbeat_threshold = mock_threshold['heartbeat_threshold']

    site1 = "test-site"
    gateway1 = "test-site"
    heartbeat_time1 = datetime(2020, 9, 24, 23, 12, 41, 401840)
    last_message_status1 = "online"
    last_message_time1= datetime(2020, 9, 23, 3, 52,40, 922055)
    time_utc1 = datetime(2020, 9, 24, 23, 15, 48, 66248)

    gateway2 = "other-test-site"
    heartbeat_time2 = datetime(2020, 9, 24, 23, 12, 58, 518540)
    last_message_status2 = "online"
    last_message_time2= datetime(2020, 9, 24, 23, 13, 41, 401840)
    time_utc2 = datetime(2020, 9, 24, 23, 13, 48, 166687)

    items_list = [{'site':site1,'gateway':gateway1, 'heartbeat_time':heartbeat_time1,'last_message_status':last_message_status1,'last_message_time':last_message_time1,'time_utc':time_utc1}]
    items_list.append({'site':site1,'gateway':gateway2, 'heartbeat_time':heartbeat_time2,'last_message_status':last_message_status2,'last_message_time':last_message_time2,'time_utc':time_utc2})

    message, write_and_send,status_by_gateway = process_heartbeats(items_list, heartbeat_threshold, email_threshold)

    assert gateway1 in message, f"expected {gateway1} in message"
    assert gateway2 in message, f"expected {gateway2} in message"
    assert write_and_send, "an email should be sent due to the delay"
    assert 'problem' not in message.lower(), f"Did not expect to see 'problem' in message: {message.lower()}"


def test_multiple_sites_one_broke(mock_threshold):
    '''
    test output when there are multiple sites and only one requires an email to be sent
    '''

    email_threshold = mock_threshold['email_threshold']
    heartbeat_threshold = mock_threshold['heartbeat_threshold']

    site1 = "test-site"
    gateway1 = "test-site"
    #make it look like the last hb was two days ago
    heartbeat_time1 = datetime(2020, 9, 21, 23, 12, 41, 401840)
    last_message_status1 = "online"
    last_message_time1= datetime(2020, 9, 24, 19, 52,40, 922055)
    time_utc1 = datetime(2020, 9, 24, 23, 13, 48, 66248)

    site2 = "test-site2"
    gateway2 = "other-test-site"
    heartbeat_time2 = datetime(2020, 9, 24, 23, 12, 58, 518540)
    last_message_status2 = "online"
    last_message_time2= datetime(2020, 9, 24, 19, 52, 41, 401840)
    time_utc2 = datetime(2020, 9, 24, 23, 13, 48, 166687)

    items_list = [{'site':site1,'gateway':gateway1, 'heartbeat_time':heartbeat_time1,'last_message_status':last_message_status1,'last_message_time':last_message_time1,'time_utc':time_utc1}]
    items_list.append({'site':site2,'gateway':gateway2, 'heartbeat_time':heartbeat_time2,'last_message_status':last_message_status2,'last_message_time':last_message_time2,'time_utc':time_utc2})

    message, write_and_send,status_by_gateway = process_heartbeats(items_list, heartbeat_threshold, email_threshold)

    assert gateway1 in message, f"expected {gateway1} in message"
    assert gateway2 in message, f"expected {gateway2} in message"
    assert write_and_send, "an email should be sent due to the delay"
    assert 'problem' in message.lower(), f"Expected to see 'problem' in message: {message.lower()}"


def test_multiple_sites_no_emails(mock_threshold):
    '''
    test output when there are multiple buckets and only one requires an email to be sent
    '''

    email_threshold = mock_threshold['email_threshold']
    heartbeat_threshold = mock_threshold['heartbeat_threshold']

    site1 = "test-site"
    gateway1 = "test-site"
    heartbeat_time1 = datetime(2020, 9, 24, 23, 12, 41, 401840)
    last_message_status1 = "online"
    last_message_time1= datetime(2020, 9, 24, 19, 52,40, 922055)
    time_utc1 = datetime(2020, 9, 24, 23, 13, 48, 66248)

    site2 = "test-site2"
    gateway2 = "other-test-site"
    heartbeat_time2 = datetime(2020, 9, 24, 23, 12, 58, 518540)
    last_message_status2 = "online"
    last_message_time2= datetime(2020, 9, 24, 19, 52, 41, 401840)
    time_utc2 = datetime(2020, 9, 24, 23, 13, 48, 166687)

    items_list = [{'site':site1,'gateway':gateway1, 'heartbeat_time':heartbeat_time1,'last_message_status':last_message_status1,'last_message_time':last_message_time1,'time_utc':time_utc1}]
    items_list.append({'site':site2,'gateway':gateway2, 'heartbeat_time':heartbeat_time2,'last_message_status':last_message_status2,'last_message_time':last_message_time2,'time_utc':time_utc2})

    message, write_and_send,status_by_gateway = process_heartbeats(items_list, heartbeat_threshold, email_threshold)

    assert gateway1 in message, f"expected {gateway1} in message"
    assert gateway2 in message, f"expected {gateway2} in message"
    assert not write_and_send, "an email should be not sent"
    assert 'problem' not in message.lower(), f"Did not expect to see 'problem' in message: {message.lower()}"
