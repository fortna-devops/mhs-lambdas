# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.3.0] - 2020-09-28

### Changed

- The heartbeat would be read from RDB ( instead of S3), so changes to that effect. Gateway information is stored in RDB too, like "gateway status" and "last message sent timestamp".
- Information can be stored on per gateway basis and can address sites with multiple gateways.

## [1.2.0] - 2020-02-12

### Changed

- Replace `ConfigManager`.`site_bucket_names` toolbox property with `ConfigManager`.`get_config_vars` mhs-rdb property to get site names from RDS instead of Athena.


## [1.1.0] - 2019-08-30

### Changed

- Integrate new `ConfigManager` functionality added to toolbox in versions greater than 0.92.8 (not inclusive) to retrieve configurable variables and emails from S3.
