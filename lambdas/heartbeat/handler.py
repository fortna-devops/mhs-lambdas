#!/usr/bin/env python3

"""
AWS Lambda that alerts developers
of any heartbeat issues.
"""
# pylint: disable=locally-disabled, too-many-ancestors, line-too-long, logging-fstring-interpolation, too-many-locals

import configparser
from datetime import datetime
import logging
import os
from typing import List, Dict, Any

from analytics_sdk_core.email_tools.email_tools import EmailSender

from mhs_rdb import ConfigManager as RDBConfigManager
from mhs_rdb import Gateways
from mhs_rdb.models import GatewaysModel

logger = logging.getLogger()
logger.setLevel(logging.INFO)

TZ_OFFSET = -4
OFFLINE = 'offline'
ONLINE = 'online'
PROBLEM_SUBJECT = '[MHS] Summary Heartbeat - Problem Detected.'
NORMAL_SUBJECT = '[MHS] Summary Heartbeat'

def determine_bad_message(seconds_since_heartbeat: float, gateway: str, lastheartbeat_str: str) -> str:
    """
    determine if the heartbeat condition should send a message
    in the emails. This function determines what that messge should be
    """

    if lastheartbeat_str:
        units = 'minutes' if seconds_since_heartbeat < 3600. else 'hours'
        t_since = 0.0
        # manually convert units until units manager can be imported from analytics toolbox
        if units == 'minutes':
            t_since = seconds_since_heartbeat / 60.
        elif units == 'hours':
            t_since = seconds_since_heartbeat / 3600.

        # trim to 2 decimal places
        short_time = f'{t_since:.2f}'

        individual_message = f'Last heartbeat for site {gateway} was <b>{lastheartbeat_str}</b>. <br><p style="color:red;">Check connectivity for {gateway}, it has been {short_time} {units} since the last heartbeat.</p>'
    else:
        individual_message = f'No heartbeat received for site {gateway}</b>. <br><p style="color:red;">Check connectivity for {gateway}.</p>'

    return individual_message


def process_heartbeats(items_list: List[Dict[str, Any]], heartbeat_threshold: int, email_threshold: int) -> Any:
    """
    determine the message that should be created
    based on the heartbeat last detected
    """
    write_and_send = False
    last_message_status= ""
    status_by_gateway = []
    message = ""

    # iterate the list of gateways information and process for state, heartbeat changes
    for item in items_list:
        site = item['site']
        gateway = item['gateway']
        lastheartbeat_time = item['heartbeat_time']
        last_message_time = item['last_message_time']
        last_message_status = item['last_message_status']
        time_utc = item['time_utc']

        if lastheartbeat_time is None:
            seconds_since_hb = heartbeat_threshold + 1
            lastheartbeat_str = ""
        else:
            seconds_since_hb = int((time_utc - lastheartbeat_time).total_seconds())
            lastheartbeat_str = lastheartbeat_time.strftime("%B %d, %Y at %I:%M:%S %p UTC")

        if last_message_time is None:
            time_since_last_email = email_threshold
        else:
            time_since_last_email = int((time_utc - last_message_time).total_seconds())

        write_and_send_gw, gateway_status, message_gw = check_hb_status(gateway, seconds_since_hb, last_message_status, time_since_last_email, lastheartbeat_str, heartbeat_threshold, email_threshold)
        gateway_status['site'] = site
        message += message_gw
        status_by_gateway.append(gateway_status)
        write_and_send = write_and_send or write_and_send_gw

    return message, write_and_send, status_by_gateway

def update_gateway_status(status_by_gateway: List[Dict[str, Any]]) -> None:
    """
    update the RDB database with status and last_message_timestamp
    """
    active_env = os.environ['env']
    rdb_cm = RDBConfigManager(active_env)
    # if the system has gone from online to offline, offline to online,
    # or it's been 12+ hrs since the last email
    for file in status_by_gateway:
        time_utc = datetime.utcnow()
        customer = file['site'].split('-')[0]
        site_name = file['site'].replace('-', '_')
        with rdb_cm.connector.get_session(customer, site_name) as db_session:
            Gateways(db_session).update(GatewaysModel.name == file['gateway'], values = {'last_message_status': file['status'], 'last_message_timestamp': time_utc})

def check_hb_status(gateway: str, seconds_since_hb: float, last_message_status: str, time_since_last_email: int, lastheartbeat_str: str, heartbeat_threshold: int, email_threshold: int) -> Any:
    """
    use the times and strings about the heartbeats of a gateway to
    determine the current status of the gateway and build a message for it
    """
    message = ''

    # cincinnati is not using regular gateway,
    # heartbeat is updated in the sensonix-data-engineering lambda,
    # heartbeats will thus be on a different schedule even during regular operation

    if gateway == 'dhl-cincinnati':
        # 7200 seconds is 2 hours
        heartbeat_threshold = 7200

    if seconds_since_hb > heartbeat_threshold:
        # if the heartbeat is older than the threshold, something is wrong
        message += f'A problem has been detected for the <b>{gateway} gateway</b>. <br>'
        message += determine_bad_message(seconds_since_hb, gateway, lastheartbeat_str)

        if (last_message_status == ONLINE) or (time_since_last_email >= email_threshold) or not last_message_status:
            # if the system was online at last run and is now offline
            # if the system was offline at last run and is still offline and it's been 12 hours
            write_and_send_gw = True
            logger.info(f'write_and_send is true for {gateway}, time since email {time_since_last_email} is either greater or equal to {email_threshold} or the last status ({last_message_status}) was "online"')
            gateway_status = {'gateway': gateway, 'status': OFFLINE}
        else:
            write_and_send_gw = False
            gateway_status = {'gateway': gateway, 'status': last_message_status}

    else:
        # the heartbeat is running as expected, no problems
        message += f"Last hearbeat for site {gateway} was <b>{lastheartbeat_str}</b>. <br>"
        if (last_message_status == OFFLINE) or (time_since_last_email >= email_threshold):
            # if the system was offline at last run and is now back online
            # if the system was online at last run and is still online and it's been 12 hours
            write_and_send_gw = True
            logger.info(f'write_and_send is true for {gateway}, time since email {time_since_last_email} is either greater or equal to {email_threshold} or the last status ({last_message_status}) was "offline"')
            gateway_status = {'gateway': gateway, 'status': ONLINE}
        else:
            write_and_send_gw = False
            gateway_status = {'gateway': gateway, 'status': last_message_status}

    return write_and_send_gw, gateway_status, message

def lambda_handler(_event: Dict, _context: Dict)->None:
    """
    entry point for AWS lambda invocation into heartbeat logic
    """

    message = ''
    write_and_send = False
    gateway_heartbeats = []

    active_env = os.environ['env']
    rdb_cm = RDBConfigManager(active_env)

    heartbeat_threshold = int(os.environ['heartbeat_threshold'])
    email_threshold = int(os.environ['email_threshold'])
    emails_str = os.environ['internal_diagnostics_group']
    emails = emails_str.split(",")

    es = EmailSender()

    site_names = [] # type: List[str]
    for sites in rdb_cm.customer_sites.values():
        site_names += [site.replace('_', '-') for site in sites]

    for site in site_names:
        customer = site.split('-')[0]
        site_name = site.replace('-', '_')
        with rdb_cm.connector.get_session(customer, site_name) as db_session:
            new_gateways_data = {(new_gateway.name, new_gateway.heartbeat_timestamp, new_gateway.last_message_status, new_gateway.last_message_timestamp): new_gateway for new_gateway in Gateways(db_session).get_all()}
            for data in new_gateways_data:
                gateway_name = data[0]
                heartbeat_timestamp = data[1]
                last_message_status = data[2]
                last_message_time = data[3]
                gateway_heartbeats.append({'site': site, 'gateway': gateway_name, 'heartbeat_time': heartbeat_timestamp, 'last_message_status': last_message_status, 'last_message_time': last_message_time, 'time_utc': datetime.utcnow()})

    if not gateway_heartbeats:
        logger.error(f'No gateways found in database for {site_names}')
    else:
        message, write_and_send, status_by_gateway = process_heartbeats(gateway_heartbeats, heartbeat_threshold, email_threshold)
        if write_and_send:
            update_gateway_status(status_by_gateway)

            if 'problem' in message:
                # if the system has gone from online to offline, offline to online,
                # or it's been 12+ hrs since the last email
                subject = PROBLEM_SUBJECT
            else:
                subject = NORMAL_SUBJECT

            logger.debug(f"message to send: {message}")
            es.send_emails(message, subject, emails)

class RawConfigParser(configparser.ConfigParser):
    """inherit config parser"""
    # https://stackoverflow.com/questions/19359556/configparser-reads-capital-keys-and-make-them-lower-case"""
    def optionxform(self, optionstr):
        """overrird optionxform for case sensitivity"""
        return optionstr

if __name__ == '__main__':
    # load in config.ini variables for running locally
    config = RawConfigParser()
    config.read('.config.ini')
    for section in config.sections():
        for key, value in config.items(section):
            os.environ[key] = value
    lambda_handler({}, {})
