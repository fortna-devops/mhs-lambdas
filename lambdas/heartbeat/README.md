# Heartbeat
----------------------------
#### Background
This lambda exists to send email notifications regarding heartbeat summaries. If the last time a heartbeat has been recieved exceeds the heartbeat threshold, it is marked as a problem. If the system has changed from offline to online, online to offline, or it has been more than 12 hours since an email was sent, a notification email will be sent to let the users know the status of each site.

----------------------------
#### Methodology
A list is created which contains the last heartbeat as an integer (the number of seconds since 1-1-1970 as is standard epoch or unix timestamp format). This timestamp is always in UTC

If there is nothing in the list, a message is created indicating that it has been over 24 hours since the last heartbeat.

If the last heartbeat time has been recieved but it exceeds 300 seconds(5 minutes), the system is marked as offline. If the system has changed from offline to online, online to offline, or it has been more than 12 hours since an email was sent, a notification email will be sent to let the users know the status of each site.

----------------------------
#### Configurable Parameters
1. **env**: name of current running environment, used to get site names and email lists
2. **heartbeat_threshold**: The heartbeat threshold. The maximum time (in seconds) between heartbeats before the system is marked as offline. For example, if `heartbeat_threshold = 300` and no heartbeat has been recieved within 300 seconds (or 5 minutes) then it would be assumed that the system had gone offline for some reason
3. **email_threshold**: the email threshold. The maximum time (in seconds) between emails before another is sent. For example, if `email_threshold = 43200` and an email has not been sent with heartbeat status updates within 43200 seconds (or 12 hours) then another email will be sent to remind users of the system's status

----------------------------
#### Lambda Triggers:

The lambda is triggerred to run once every hour, via a scheduled CloudWatch Trigger.


