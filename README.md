# AWS lambda management system

This repo is a storage for AWS lambdas

## Before you start
### Structure
* Lambda should be placed in [lambdas](lambdas) dir
* Lambda should be represented by dir
* Lambda should have `.config.ini` file

### Initial configuration
You need to install git hooks:
```
./install_hooks.sh
```


## How to remove directory with lambda
* run ```./uninstall_hooks.sh```
* delete unneeded directory
* commit and push your changes
* run ```./install_hooks.sh```

## How to use
After you push code to this repo `pipeline` will be run according to the [bitbucket-pipelines](bitbucket-pipelines.yml)

## Additional information
To get more details about CI please read the readme for `mhs-ci` repository.
