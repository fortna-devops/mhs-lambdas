#!/usr/bin/env bash

BASE_DIR=./lambdas/*


for DIR in $BASE_DIR
do
    echo $DIR
    VERSION_FILE=${DIR}/.version
    DUMMY_HASH='12345'
    echo $DUMMY_HASH
    echo $DUMMY_HASH > ${VERSION_FILE}
done